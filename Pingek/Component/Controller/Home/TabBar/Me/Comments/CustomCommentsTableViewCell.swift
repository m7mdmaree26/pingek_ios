//
//  CustomCommentsTableViewCell.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/26/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit
import Cosmos

class CustomCommentsTableViewCell: UITableViewCell {

    @IBOutlet weak var userRate: CosmosView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var userName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.shadowOffset.height = 1
        self.shadowRadius = 5
        self.shadowColor = UIColor.lightGray.withAlphaComponent(30)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(comment:CommentsModel){
        self.userName.text = comment.name
        self.comment.text = comment.comment
        self.userRate.rating = comment.rate
        self.userImage.setImageWith(comment.avatar)
//        if (comment.rate  >= 3.5){
//            self.userImage.image = #imageLiteral(resourceName: "happy")
//        }else if( comment.rate >= 2.5 && comment.rate < 4){
//            self.userImage.image = #imageLiteral(resourceName: "confused")
//        }else {
//            self.userImage.image = #imageLiteral(resourceName: "sad")
//
//        }
      //  self.userRate.value = comment.rate
    }

    
}
