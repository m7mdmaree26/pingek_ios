//
//  MakeOrderSecondPageVc.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/25/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import ActionSheetPicker_3_0
import EzPopup

class MakeOrderSecondPageVc: UIViewController {

    @IBOutlet weak var storeName: UILabel!
    @IBOutlet weak var recivingPointAddress: UILabel!
    @IBOutlet weak var orderDurationText: UITextField!
    @IBOutlet weak var delivaryPointText: UITextField!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var favPlaceButton: UIButton!
    @IBOutlet weak var storeImage: UIImageView!
    @IBOutlet weak var sendOrderBtn: UIButton!

    var store = NearStoreModel()
    var imagesKeys = [String]()
    var imagesData = [Data]()
    var orderDetails = ""
    let storeMarker = GMSMarker()
    let selectedUserMarker = GMSMarker()
    let path = GMSMutablePath()

    var lat:Double?
    var lng:Double?
    var minExpictedPrice:Double!
    var MaxExpictedPrice:Double!
    let paragraphStyle = NSMutableParagraphStyle()
    var HourArray = [String]()
    var selectedValue = ""
    
    //==============
    var savedPlaces = [SavedPlace]()


    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        getSavedPlaces()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        

    }
    
    func setupView(){
        storeName.text = store.name
        recivingPointAddress.text = store.vicinity
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)
        self.storeImage.setImageWith(store.icon)
        orderDurationText.setLeftPaddingPoints(30)
        delivaryPointText.setLeftPaddingPoints(30)
        orderDurationText.setRightPaddingPoints(30)
        delivaryPointText.setRightPaddingPoints(30)
        paragraphStyle.alignment = .center

        //======================mapViewInit
        mapView.delegate = self
        storeMarker.appearAnimation = .pop
        selectedUserMarker.appearAnimation = .pop
        let camera = GMSCameraPosition.camera(withLatitude:  Double(store.lat), longitude: Double(store.long), zoom: 15)
        self.mapView.camera = camera
        storeMarker.position = CLLocationCoordinate2D(latitude: Double(store.lat), longitude: Double(store.long))
        storeMarker.icon = #imageLiteral(resourceName: "store")
        storeMarker.map = mapView
        storeMarker.title = store.vicinity
        path.add(CLLocationCoordinate2DMake(Double(store.lat), Double(store.long)))



    }
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func addAddressToFavourite(_ sender: Any) {
        
        if self.delivaryPointText.text?.count != 0 {
            if favPlaceButton.imageView?.image != #imageLiteral(resourceName: "star"){
                favPlaceButton.setImage(#imageLiteral(resourceName: "star"), for: .normal)
                let vc = Storyboard.Home.viewController(SavePlaceVC.self)
                vc.store = self.store
                vc.vc = self
                vc.lat = self.lat
                vc.lng = self.lng
                vc.address = self.recivingPointAddress.text!
                
                let popupVC = PopupViewController(contentController: vc, popupWidth: UIScreen.main.bounds.width-60, popupHeight: 150)
                
                popupVC.backgroundAlpha = 0.5
                popupVC.backgroundColor = .black
                popupVC.canTapOutsideToDismiss = false
                popupVC.cornerRadius = 20
                popupVC.shadowEnabled = true
                
                
                self.present(popupVC, animated: true)
            }
        }
    }

    @IBAction func gotoMapAction(_ sender: Any) {
        if savedPlaces.count == 0 {
            print("myTargetFunction")
            let vc = Storyboard.Authentication.viewController(GetLocationViewController.self)
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = Storyboard.Home.viewController(SavedPlacesVC.self)
            vc.vc = self
            vc.store = self.store
            vc.delegate = self
            vc.savedPlaces = self.savedPlaces
            
            let popupVC = PopupViewController(contentController: vc, popupWidth: UIScreen.main.bounds.width-60, popupHeight: 300)
            
            popupVC.backgroundAlpha = 0.5
            popupVC.backgroundColor = .black
            popupVC.canTapOutsideToDismiss = true
            popupVC.cornerRadius = 20
            popupVC.shadowEnabled = true
            
            
            self.present(popupVC, animated: true)
        }
     
    }
    @IBAction func chosseOrderDuration(_ sender: Any) {

        self.HourArray = ["1","2","3","4","5","6","7","8"]
        let vc = Storyboard.Home.viewController(SelectDurationVC.self)
        vc.hours = self.HourArray
        vc.vc2 = self
        let popupVC = PopupViewController(contentController: vc, popupWidth: UIScreen.main.bounds.width-100, popupHeight: 250)
           
        popupVC.backgroundAlpha = 0.5
        popupVC.backgroundColor = .black
        popupVC.canTapOutsideToDismiss = true
        popupVC.cornerRadius = 10
        popupVC.shadowEnabled = true
           
           
        self.present(popupVC, animated: true)
    }
   
    @IBAction func addOrder(_ sender: Any) {
           if(delivaryPointText.text?.isEmpty == true || orderDurationText.text?.isEmpty == true){
               self.sendOrderBtn.shake(duration: 0.5, values: [-12.0, 12.0, -12.0, 12.0, -6.0, 6.0, -3.0, 3.0, 0.0])
               ShowErrorMassge(massge: "Please complete your Data".localized, title: "Error".localized)
           }else{

            self.AddNewProduct(place_name: self.store.name, icon: self.store.icon, receive_address: self.store.vicinity, receive_lat: String(describing: self.store.lat), receive_long: String(describing:self.store.long), deliver_address: self.delivaryPointText.text!, deliver_lat: self.lat!, deliver_long: self.lng!, deliver_time: self.selectedValue, description: self.orderDetails)
           }
       }
       
       func AddNewProduct(place_name:String,icon:String,receive_address:String,receive_lat:String,receive_long:String,deliver_address:String,deliver_lat:Double,deliver_long:Double,deliver_time:String,description:String){
           var parmas:Parameters = [
               "place_name":place_name,
               "icon":icon,
               "receive_address":receive_address,
               "receive_lat":receive_lat,
               "receive_long":receive_long,
               "deliver_address":deliver_address,
               "deliver_lat":deliver_lat,
               "deliver_long":deliver_long,
               "deliver_time":deliver_time,
               "description":description,

           ]
        if self.store.reference == "" {
            parmas.updateValue(self.store.place_id, forKey: "store_id")
        }else{
             parmas.updateValue(self.store.place_id, forKey: "place_id")
             parmas.updateValue(self.store.reference, forKey: "place_ref")
        }
        
        self.showProgress()
        API.POSTImage(url: CreateOrder_URL, Images: imagesData, Keys: imagesKeys, header: ["Authorization":"Bearer" + getTokenId(),"lang": getServerLang()], parameters: parmas) { (succss, value) in
               if succss{
                   let msg = value["msg"] as! String
                   self.stopAnimating()
                   print("🌸\(value)")
                   ShowTrueMassge(massge: msg, title: "Done".localized())
                   DispatchQueue.main.asyncAfter(deadline: .now() + 1) { // change 2 to desired number of
                       let vc = Storyboard.Home.viewController(ThanksForOrderViewController.self)
                       self.navigationController?.pushViewController(vc, animated: true)
                       
                   }
               }else{
                   self.stopAnimating()
                   ShowErrorMassge(massge: "Something went wrong and tried again".localized(), title: "Error".localized())
               }
           }
       }
}

extension MakeOrderSecondPageVc{

    func getSavedPlaces(){
        self.savedPlaces.removeAll()
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        
        API.POST(url: savedPlaces_URL, parameters: [:], headers: header) { (succss, value) in
            if succss{
                let key = value["value"] as! String
                if key == "1" {
                    let data = value["data"] as! [[String:Any]]
                    for item in data {
                        self.savedPlaces.append(SavedPlace().getObject(dicc:item))
                    }
                    
                }
            }
        }
    }
    
}
extension MakeOrderSecondPageVc : choseSavedPlaceProtocole ,sendDataBackDelegate , GMSMapViewDelegate {
    
    func finishPassing(location: String, lat: Double, lng: Double) {
        favPlaceButton.setImage(#imageLiteral(resourceName: "favorite"), for: .normal)
        self.delivaryPointText.text = location
        self.lat = lat
        self.lng = lng
        selectedUserMarker.position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        selectedUserMarker.icon = #imageLiteral(resourceName: "vectorperson")
        selectedUserMarker.map = mapView
        selectedUserMarker.title = location
        path.add(CLLocationCoordinate2DMake(lat,lng))
        let bounds = GMSCoordinateBounds(path: path)
        self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
    }
    
    func sentData(savedPlaces: SavedPlace, isRecLocation: Bool) {
        
        self.delivaryPointText.text = savedPlaces.name
        self.lat = Double(savedPlaces.lat)!
        self.lng = Double(savedPlaces.long)!
        favPlaceButton.setImage(#imageLiteral(resourceName: "star"), for: .normal)
        selectedUserMarker.position = CLLocationCoordinate2D(latitude: Double(savedPlaces.lat)!, longitude: Double(savedPlaces.long)!)
        selectedUserMarker.icon = #imageLiteral(resourceName: "vectorperson")
        selectedUserMarker.map = mapView
        selectedUserMarker.title = savedPlaces.address
        path.add(CLLocationCoordinate2DMake(Double(savedPlaces.lat)!, Double(savedPlaces.long)!))
        let bounds = GMSCoordinateBounds(path: path)
        self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))

    }
    
    
    
}
