//
//  DeliveryTripViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 2/27/19.
//  Copyright © 2019 Sara Ashraf. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import MapKit
class DeliveryTripViewController: UIViewController, GMSMapViewDelegate {
    @IBOutlet weak var mapView: MKMapView!
    let locationManager = CLLocationManager()
    let GoogleAPI = "AIzaSyDEJd62-3jzmktJR565uvRV6qtedNwwk3c"
    var userTrackId = ""
    var ResiverLat:Double?
    var ResiverLong:Double?
    
    var latitude:Double!
    var longitude:Double!
    
    var DeliverLat = 0.0
    var DeliverLng = 0.0
    
    var Userelat = 0.0
    var UserLng = 0.0
    
    var Addressstore = ""
    var ClintAddress = ""
    var RestruntName = ""
    @IBOutlet weak var addressClint: UILabel!
    @IBOutlet weak var clintImage: UIImageView!
    @IBOutlet weak var storImafe: UIImageView!
    @IBOutlet weak var storeAddress: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var googleMap: GMSMapView!
    @IBAction func goback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func storeLocationAction(_ sender: Any) {
        
        let coordinate = CLLocationCoordinate2DMake(self.ResiverLat!,self.ResiverLong!)
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
        mapItem.name = self.RestruntName
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
        
    }
    
    @IBAction func ClintActiionLocation(_ sender: Any) {
        
        let coordinate = CLLocationCoordinate2DMake(self.DeliverLat,self.DeliverLng)
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
        mapItem.name = "Customer location".localized
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
    }
    
    @IBOutlet weak var storeLocationView: UIView!
    @IBOutlet weak var clientLocationView: UIView!
    @IBOutlet weak var gpsicon2: UIButton!
    @IBOutlet weak var gpsIcon: UIButton!
    
    let Delgateannotation = ImageAnnotation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView?.delegate = self
        self.storImafe.image = #imageLiteral(resourceName: "vectorstore")
        self.clintImage.image = #imageLiteral(resourceName: "vector")
        self.gpsIcon.setImage(#imageLiteral(resourceName: "location"), for: .normal)
        self.gpsicon2.setImage(#imageLiteral(resourceName: "location"), for: .normal)
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        self.storeAddress.text = Addressstore
        self.addressClint.text = ClintAddress
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(),for: .default)
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)
        self.googleMap.delegate = self
        self.googleMap.animate(toZoom: 10)
        
        let camera = GMSCameraPosition.camera(withLatitude:  Double(getUserLat())!, longitude: Double(getUserLong())!, zoom: 15)
               self.googleMap.camera = camera

        
        DrawAnnotion(lat: self.DeliverLat, long: self.DeliverLng)
        self.addResiveLocation(lat: self.ResiverLat!, Long: self.ResiverLong!)
        self.addDelgate(lat: self.Userelat,Long: self.UserLng)
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
        } else {
            locationManager.requestWhenInUseAuthorization()
            locationManager.requestAlwaysAuthorization()
        }
        let openstoreLocation = UITapGestureRecognizer(target: self, action: #selector(openstoreLocationAction))
        self.storeLocationView.addGestureRecognizer(openstoreLocation)
        
        let openClientLocation = UITapGestureRecognizer(target: self, action: #selector(openClientLocationAction))
        self.clientLocationView.addGestureRecognizer(openClientLocation)
        
    }
    @objc func openstoreLocationAction(sender: UITapGestureRecognizer){
        let coordinate = CLLocationCoordinate2DMake(self.ResiverLat!,self.ResiverLong!)
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
        mapItem.name = self.RestruntName
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
    }
    @objc func openClientLocationAction(sender: UITapGestureRecognizer){
        let coordinate = CLLocationCoordinate2DMake(self.DeliverLat,self.DeliverLng)
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
        mapItem.name = "Customer location".localized
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
    }
    let regionRadius: CLLocationDistance = 1500
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView?.setRegion(coordinateRegion, animated: true)
    }
    
    func DrawAnnotion(lat:Double,long:Double){
        
//        let coordinateRegion = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: lat, longitude: long),
//                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
//        mapView?.setRegion(coordinateRegion, animated: true)
//        let annotation = ImageAnnotation()
//        annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
//        annotation.annotationImage = #imageLiteral(resourceName: "vector")
//        annotation.isClicked = false
//        mapView?.addAnnotation(annotation)
//        print("🚘\(annotation.coordinate.latitude),\(annotation.coordinate.longitude)")
//        //    annotation.viewTag = self.projects[i].id!
        
        //====================google
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: Double(lat), longitude: Double(long))
        marker.icon = #imageLiteral(resourceName: "vector")
        marker.map = googleMap
    }
    func addResiveLocation(lat:Double,Long:Double){
//        let annotation = ImageAnnotation()
//        annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: Long)
//        annotation.annotationImage = #imageLiteral(resourceName: "vectorstore")
//        annotation.isClicked = false
//        mapView?.addAnnotation(annotation)
//        //    annotation.viewTag = self.projects[i].id!
        //====================google
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: Double(lat), longitude: Double(Long))
        marker.icon = #imageLiteral(resourceName: "vectorstore")
        marker.map = googleMap
    }
    func addDelgate(lat:Double,Long:Double){
//        self.mapView?.removeAnnotations((self.mapView?.annotations)!)
//        self.Delgateannotation.coordinate = CLLocationCoordinate2D(latitude: Double(lat), longitude: Double(Long))
//        self.Delgateannotation.annotationImage = #imageLiteral(resourceName: "Capa")
//        self.Delgateannotation.isClicked = false
//        mapView?.addAnnotation(  self.Delgateannotation)
//        DrawAnnotion(lat: self.DeliverLat, long: self.DeliverLng)
//        self.addResiveLocation(lat: self.ResiverLat!, Long: self.ResiverLong!)
//        print("delgatee")
        //====================google
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: Double(lat), longitude: Double(Long))
        marker.icon = #imageLiteral(resourceName: "bus")
        marker.map = googleMap
    }
    
    
    
}
extension DeliveryTripViewController : CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        Alert.showAlertOnVC(target: self, title: "تأكد من فتح ال GPS", message: "")
    }
}
extension DeliveryTripViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        
        if annotation.isKind(of: ImageAnnotation.self) {
            
            var view: ImageAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: "imageAnnotation") as? ImageAnnotationView
            if view == nil {
                view = ImageAnnotationView(annotation: annotation, reuseIdentifier: "imageAnnotation")
                
            }
            
            let annotation = annotation as! ImageAnnotation
            view?.annotationImgView.image = annotation.annotationImage
            view?.annotaionLbl.setTitle(annotation.title, for: .normal)
            view?.annotation = annotation
            
            return view
        }else{
            
        }
        return nil
    }
}

