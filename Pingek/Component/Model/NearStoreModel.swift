//
//  NearStoreModel.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 2/27/19.
//  Copyright © 2019 Sara Ashraf. All rights reserved.
//

import Foundation

struct  NearStoreModel{
    
    var name :String = ""
    var icon:String = ""
    var vicinity:String = ""
    var lat:Double = 0.0
    var long:Double = 0.0
    var place_id:String = ""
    var reference:String = ""
    var opening_hours:[String] = []
    var distance:String = ""
    var storeID :String = ""
    
}
