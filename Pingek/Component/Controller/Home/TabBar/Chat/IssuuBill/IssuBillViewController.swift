//
//  IssuBillViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/29/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Photos
class IssuBillViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    @IBOutlet weak var billImage: UIImageView!
    @IBOutlet weak var goodsCost: BorderBaddedTextField!
    @IBOutlet weak var delivryCost: BorderBaddedTextField!
    @IBOutlet weak var total: BorderBaddedTextField!
    @IBOutlet weak var addimageBtn: UIButton!
    @IBOutlet weak var issuuBtn: RoundedButton!
    @IBOutlet weak var addimage: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var plusIcone: UIButton!
    
    @IBAction func addImage(_ sender: Any) {
        print("😃🔍hiiiiii")
        var myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(myPickerController, animated: false, completion: nil)
    }
    
    var imagesData = [Data]()
    var imagesKeys = [String]()
    var flagImage = false
    var imageStr = ""
    var imageData:Data!
    var orderPrice = ""
    var OrderId = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        EditTextDesign(text: goodsCost)
        EditTextDesign(text: delivryCost)
        EditTextDesign(text: total)
        self.hideKeyboardWhenTappedAround()
        self.delivryCost.text = self.orderPrice
        self.goodsCost.keyboardType  = .asciiCapableNumberPad
        self.delivryCost.keyboardType  = .asciiCapableNumberPad
        self.goodsCost.addTarget(self, action: #selector(addTotlal), for:.editingChanged)
        self.navigationController?.isNavigationBarHidden = false
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)
        self.billImage.clipsToBounds = true

    }
    @objc func addTotlal(){
        if self.goodsCost.text?.count != 0  {
            if  self.delivryCost.text?.count != 0{
                let total = Double(self.goodsCost.text!)! + Double(self.orderPrice)!
                self.total.text = String(describing:total)
            }
        }
       
    }
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func IssuBillAction(_ sender: Any) {
        if(self.goodsCost.text?.isEmpty == true || self.flagImage == false){
            self.issuuBtn.shake(duration: 0.5, values: [-12.0, 12.0, -12.0, 12.0, -6.0, 6.0, -3.0, 3.0, 0.0])
            ShowErrorMassge(massge: "Please complete your Data".localized, title: "Error".localized)
        }else{
            imagesData.append(self.imageData!)
            imagesKeys.append("image")
            self.showProgress()
            self.IssuBill(delivery_cost: self.delivryCost.text!, price: self.goodsCost.text!, order_id: OrderId)
        }
    }
    
    
    func imagePickerController(_ picker:UIImagePickerController,didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]){
        if let imageURL = info[.imageURL] as? URL {
            let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
            let asset = result.firstObject
        
        }
        let image_data = info[.originalImage] as! UIImage
        self.imageData = (image_data).pngData()! as Data
        self.billImage.image = image_data
        imageStr = imageData.base64EncodedString()
        billImage.backgroundColor = UIColor.clear
        self.dismiss(animated: true, completion: nil)
        self.addimageBtn.setImage( nil, for:.normal)
        self.addimage.setImage( nil, for:.normal)
        self.plusIcone.isHidden = true

        flagImage = true
        
    }
    
    func IssuBill(delivery_cost:String,price:String,order_id:String){
        let parmas:Parameters = [
            "user_id":getUserID(),
            "delivery_cost":delivery_cost,
            "price":price,
             "order_id":order_id
        ]
        API.POSTImage(url: IssuBill_URL, Images: imagesData, Keys: imagesKeys, header: ["Authorization":"Bearer" + getTokenId(),"lang":getServerLang()], parameters: parmas) { (succss, value) in
            if succss{
                self.stopAnimating()
                print("🌸\(value)")
                ShowTrueMassge(massge:"The invoice was successfully issued".localized, title: "Done".localized)
                
                self.navigationController?.popViewController(animated: true)
                NotificationCenter.default.post(name: .IssuBill, object:nil)
            }else{
                self.stopAnimating()
                ShowErrorMassge(massge: "Something went wrong and tried again".localized, title: "Error".localized)
            }
        }
    }


}
