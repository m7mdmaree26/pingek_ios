//
//  SpecialOrderCollectionCell.swift
//  Pingek
//
//  Created by Engy Bakr on 2/10/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit

class SpecialOrderCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemTitle: UILabel!
    
    
}
