//
//  SavedPlacesVC.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/25/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import Alamofire

protocol choseSavedPlaceProtocole {
    func sentData(savedPlaces: SavedPlace,isRecLocation : Bool)
}

class SavedPlacesVC: UIViewController {

    
    @IBOutlet weak var placesTable: UITableView!
    var savedPlaces = [SavedPlace]()
    var vc = MakeOrderSecondPageVc()
    var specialOrdervc : SpecialOrderVC?
    var store = NearStoreModel()
    var delegate : choseSavedPlaceProtocole?
    var isRecLocation = true
    override func viewDidLoad() {
        super.viewDidLoad()
        placesTable.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func addFromMap(_ sender: Any) {
        self.dismiss(animated: true)
        let mapVC = Storyboard.Authentication.viewController(GetLocationViewController.self)
        if specialOrdervc == nil {
             mapVC.delegate = self.vc
            self.vc.navigationController?.pushViewController(mapVC, animated: true)
        }else{
            if isRecLocation {
                mapVC.delegate = self.specialOrdervc
            }else{
                mapVC.delegate2 = self.specialOrdervc

            }
             self.specialOrdervc?.navigationController?.pushViewController(mapVC, animated: true)
        }
       
    }
    
    func resetPlaceFromFav(id:String,name: String,address: String,lat: String,long: String){
           self.showProgress()
           let params:Parameters = [
               "name":name,
               "id":id,
               "address":address,
               "lat":lat,
               "long":long
           ]
           let header:HTTPHeaders = [
               "Authorization":"Bearer" + getTokenId(),
               "lang":getServerLang()
           ]
               
           API.POST(url: savePlace_URL, parameters: params, headers: header) { (succss, value) in
               if succss{
                   self.stopAnimating()
                   let key = value["value"] as! String
                   if key == "1" {
                       ShowTrueMassge(massge: value["msg"] as! String, title: "Success".localized())
                        let data = value["data"] as! [[String:Any]]
                        self.vc.savedPlaces.removeAll()
                        for item in data {
                            self.vc.savedPlaces.append(SavedPlace().getObject(dicc:item))
                        }
                       self.dismiss(animated: true)
                   }else{
                       ShowErrorMassge(massge: value["msg"] as! String, title: "Error".localized)

                   }
               }else{
                   
                   self.stopAnimating()
                   ShowErrorMassge(massge: "Something went wrong and tried again".localized, title: "Error".localized)
               }
           }
       }


}
extension SavedPlacesVC : UITableViewDataSource ,UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.savedPlaces.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceTableCell", for: indexPath) as! PlaceTableCell
        cell.placeTitle.text = self.savedPlaces[indexPath.row].name
        cell.placeDesc.text = self.savedPlaces[indexPath.row].address
        cell.removePlace = {
            self.resetPlaceFromFav(id: self.savedPlaces[indexPath.row].id,name: self.savedPlaces[indexPath.row].name,address: self.savedPlaces[indexPath.row].address,lat: self.savedPlaces[indexPath.row].lat,long: self.savedPlaces[indexPath.row].long)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.sentData(savedPlaces: self.savedPlaces[indexPath.row],isRecLocation : isRecLocation)
        self.dismiss(animated: true)
    }
        
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }

}
