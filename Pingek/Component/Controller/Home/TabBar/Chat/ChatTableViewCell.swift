//
//  ChatTableViewCell.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/12/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {
    @IBOutlet weak var massge: UILabel!
    @IBOutlet weak var massgeBody: UIView!
    @IBOutlet weak var senderWidth: NSLayoutConstraint!
    @IBOutlet weak var reseverWidth: NSLayoutConstraint!
    @IBOutlet weak var UsersenderImage: UIImageView!
    @IBOutlet weak var UserResveImage: UIImageView!
   
    var flageSender:Bool!
    
    override func awakeFromNib() {
        super.awakeFromNib()
   }

    override func prepareForReuse() {
        self.UsersenderImage.image = nil
        self.UserResveImage.image = nil
        self.massgeBody.backgroundColor = nil
        self.massge.textColor = nil
    }

    

}
