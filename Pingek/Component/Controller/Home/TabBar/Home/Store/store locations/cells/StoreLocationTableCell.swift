//
//  StoreLocationTableCell.swift
//  Sandkom
//
//  Created by Engy Bakr on 2/1/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit

class StoreLocationTableCell: UITableViewCell {

    @IBOutlet weak var storeImage: UIImageView!
    @IBOutlet weak var storeNAme: UILabel!
    @IBOutlet weak var storeDistance: UILabel!
    @IBOutlet weak var checkImage: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected{
            checkImage.image = #imageLiteral(resourceName: "checked")
        }else{
           checkImage.image = nil
        }

        // Configure the view for the selected state
    }

}
