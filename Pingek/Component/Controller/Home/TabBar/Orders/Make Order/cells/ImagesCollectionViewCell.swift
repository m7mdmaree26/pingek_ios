//
//  ImagesCollectionViewCell.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/5/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit

class ImagesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var CurrentImage: UIImageView!
    @IBOutlet weak var deleteButton: UIButton!

}
