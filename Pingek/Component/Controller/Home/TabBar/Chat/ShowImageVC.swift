//
//  ShowImageVC.swift
//  Sandkom
//
//  Created by Engy Bakr on 2/5/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit

class ShowImageVC: UIViewController , UIScrollViewDelegate{
    
    var storeWeb = ""
    var storePho = ""
    
    @IBOutlet weak var storePhone: UILabel!
    @IBOutlet weak var storeWebsite: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var messgImage: UIImageView!
    
    var image = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 6.0
        messgImage.setImageWith(image)
        if storeWeb != "" {
            storeWebsite.text = "Website".localized + " : " + storeWeb

        }
        if storePho != "" {
            storePhone.text = "Phone number".localized + " : " + storePho

        }
    }
    

    @IBAction func cancelView(_ sender: Any) {
        self.dismiss(animated: true)
    }
     func viewForZooming(in scrollView: UIScrollView) -> UIView? {

         return messgImage
     }

}
