//
//  HomeModel.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/27/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import Foundation
class HomeModel: NSObject {
    var next_page_token = ""
    var status = ""
    var results = [PlaceModel]()
    var isNextTokenAvailable = false
    
    func getObject(dicc: [String: Any]) -> HomeModel {
        let dic = HandleJSON.getObject().handle(dicc: dicc)
        let model = HomeModel()
//        model.next_page_token = dic["next_page_token"] as! String!
        if let nextToken = dic["next_page_token"] as? String{
            model.isNextTokenAvailable = true
            model.next_page_token = nextToken
        }else{
            model.isNextTokenAvailable = false
            model.next_page_token = ""
        }
        model.status = dic["status"] as! String
  
        
        let results = dic["results"] as! [[String: Any]]
        model.results.removeAll()
        for item in results {
            model.results.append(PlaceModel().getObject(dicc: item))
       }
        print("💄\(self.results.count)")
        
        return model
    }
    
}






