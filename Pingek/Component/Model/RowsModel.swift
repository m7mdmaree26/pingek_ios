//
//  RowsModel.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/28/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import Foundation
class RowsModel: NSObject {
    var elements = [ElemntsModel]()
    
    func getObject(dicc: [String: Any]) -> RowsModel {
        let dic = HandleJSON.getObject().handle(dicc: dicc)
        let model = RowsModel()
        let elements = dic["elements"] as! [[String: Any]]
        model.elements.removeAll()
        for item in elements {
            model.elements.append(ElemntsModel().getObject(dicc: item))
        }
        print("💄\(self.elements.count)")
        
        
        
        return model
    }
}
