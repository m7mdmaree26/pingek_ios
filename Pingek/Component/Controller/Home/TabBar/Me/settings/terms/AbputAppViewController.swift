//
//  AbputAppViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/3/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class AbputAppViewController: UIViewController {

    @IBOutlet weak var termsTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.topItem?.title = ""

    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "About the application".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name:  "JFFlat-Medium", size: 15)!]
        self.showProgress()
        getTerms()
    }
    
    
    func getTerms(){
        API.POST(url: AboutApp_URL, parameters: [:], headers: ["lang":getServerLang()]) { (succss, value) in
            if succss{
                self.stopAnimating()
                let data = value["data"] as! [String:Any]
                let about_app = data["about_app"] as! String
                self.termsTextView.text = about_app
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
}
