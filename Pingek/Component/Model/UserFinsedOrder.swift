//
//  UserFinsedOrder.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/16/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import Foundation
struct UserFinsedOrder{
    var id:Int
    var client_name:String
    var place_id:String
    var place_name:String
    var place_icon:String
    var details:String
    var deliver_time:String
    var status:String
    var date:String
}
