//
//  EditProfileViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/26/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Photos
import EzPopup
import GoogleMaps

class EditProfileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    var imagesData = [Data]()
    var imagesKeys = [String]()
    var flagImage = false
    var imageStr = ""
    var imageData:Data!
    //location
    var lat = getUserLat()
    var lng = getUserLong()
    var userEmail = ""
    
   
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var changePassword: UIButton!
    @IBOutlet weak var addImageBtn: UIButton!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var backButton: UIButton!

    @IBAction func goBack(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addImageAction(_ sender: Any) {
        print("😃🔍hiiiiii")
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(myPickerController, animated: false, completion: nil)
    }

    @IBAction func ChangePassword(_ sender: Any) {
        let forgetPass = Storyboard.Home.viewController(ChangePasswordViewController.self)
        let popupVC = PopupViewController(contentController: forgetPass, popupWidth: UIScreen.main.bounds.width-60, popupHeight: 400)
        popupVC.backgroundAlpha = 0.5
        popupVC.backgroundColor = .black
        popupVC.canTapOutsideToDismiss = true
        popupVC.cornerRadius = 10
        popupVC.shadowEnabled = true
        
                        
                        
        self.present(popupVC, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.hideKeyboardWhenTappedAround()
        self.userName.isUserInteractionEnabled = true
        self.userName.isUserInteractionEnabled = true
        self.userName.isUserInteractionEnabled = true
//        self.locationNumber.text = getUserAddress()
        userName.addTarget(self, action: #selector(FoucseUIEditText), for: UIControl.Event.editingDidBegin)
        userName.addTarget(self, action: #selector(UnFoucseUIEditText), for: UIControl.Event.editingDidEnd)
        email.addTarget(self, action: #selector(FoucseUIEditText), for: UIControl.Event.editingDidBegin)
        email.addTarget(self, action: #selector(UnFoucseUIEditText), for: UIControl.Event.editingDidEnd)
        self.changePassword.setTitle("Change Password".localized, for: .normal)
        self.save.setTitle("Save".localized, for: .normal)
        self.userImage.setImageWith(getUserAvatar())
        self.userName.text = getUserName()
        self.email.text = getUSerEmail()
        self.userImage.borderColor = UIColor.lightGray.withAlphaComponent(30)
        self.userImage.borderWidth = 1.5
        self.userImage.clipsToBounds = true
        self.userImage.cornerRadius = 50
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)
        
        self.userName.setLeftPaddingPoints(10)
        self.userName.setRightPaddingPoints(10)
        self.email.setLeftPaddingPoints(10)
        self.email.setRightPaddingPoints(10)

    }
    
    override func viewWillAppear(_ animated: Bool) {
         print("🌸\(getUserAddress())")
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "JFFlat-Medium", size: 16)!,NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)]
       

    }
    @objc func FoucseUIEditText(text:UITextField){
        text.layer.borderColor = UIColor.BasicColor.cgColor
        text.layer.borderWidth = 2
    }
    
    @objc func UnFoucseUIEditText(text:UITextField){
        text.layer.borderColor = UIColor.clear.cgColor
        text.layer.borderWidth = 1
    }
    @IBAction func saveImageAction(_ sender: Any) {
        self.startAnimating()
        if(flagImage == false){
            imagesData = []
            imagesKeys = []
        }else{
            imagesData.append(self.imageData!)
            imagesKeys.append("avatar")
        }
        self.EditProfile(name: self.userName.text!)
    }
    
    internal func imagePickerController(_ picker:UIImagePickerController,didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]){
        if let imageURL = info[.imageURL]  as? URL {
            let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
            let asset = result.firstObject
        
        }
        let image_data = info[.originalImage] as! UIImage
        self.imageData = (image_data).pngData()! as Data
        self.userImage.image = image_data
        imageStr = imageData.base64EncodedString()
        userImage.backgroundColor = UIColor.clear
        self.dismiss(animated: true, completion: nil)
        self.addImageBtn.setImage( nil, for:.normal)
        flagImage = true
        
    }
    func EditProfile(name:String){
        let parmas:Parameters = [
            "name":name,
            "user_id":getUserID(),
            "email":self.email.text!
        ]
        API.POSTImage(url: EditProfile_URL, Images: imagesData, Keys: imagesKeys, header: ["Authorization":"Bearer" + getTokenId(),"lang":getServerLang()], parameters: parmas) { (succss, value) in
            if succss{
                self.stopAnimating()
                print("🌸\(value)")
                ShowTrueMassge(massge: "Modified successfully".localized, title: "Done".localized)
                let data = value["data"] as! [String:Any]
                let address =  data["address"] as! String
                let name = data["name"] as! String
                let email = data["email"] as! String
                let avatar = data["avatar"] as! String
                let googlekey = data["googlekey"] as! String
                GMSServices.provideAPIKey(googlekey)
                defaults.set(googlekey, forKey: GOOGLE_KEY)
                defaults.set(avatar, forKey: User_Avatar)
                defaults.set(name, forKey: User_Name)
                defaults.set(address, forKey: User_Address)
                defaults.set(email, forKey: User_Email)
                print("🌸\(address)")
            }else{
                self.stopAnimating()
                ShowErrorMassge(massge: "Something went wrong and tried again".localized, title: "Error".localized)
            }
        }
    }
    
    
    
}
//extension EditProfileViewController : sendDataBackDelegate{
//    func finishPassing(location: String, lat: Double, lng: Double) {
//        self.locationNumber.text = location
//        self.lat = String(lat)
//        self.lng = String(lng)
//        print("🔓\(self.lat)")
//    }
//}
