//
//  PlaceTableCell.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/25/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit

class PlaceTableCell: UITableViewCell {
    @IBOutlet weak var placeTitle: UILabel!
    @IBOutlet weak var placeDesc: UILabel!

    var removePlace : (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func deletePlace(_ sender: Any) {
        removePlace?()
    }
    
}
