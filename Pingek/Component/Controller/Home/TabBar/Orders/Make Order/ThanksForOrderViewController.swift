//
//  ThanksForOrderViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/29/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit

class ThanksForOrderViewController: UIViewController {
    
    @IBOutlet weak var thankImage: UIImageView!
    @IBOutlet weak var backGround: UIImageView!
    
    
    override func viewDidLoad() {
           super.viewDidLoad()
           self.navigationController?.isNavigationBarHidden = true
           self.tabBarController?.tabBar.isHidden = true
    }
    @IBAction func backToHome(_ sender: Any) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        guard let window = UIApplication.shared.keyWindow else { return }
        let vc = Storyboard.Home.viewController(TabBarViewController.self)
        window.rootViewController = vc
    }
    
   

   

}
