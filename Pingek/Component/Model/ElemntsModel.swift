//
//  ElemntsModel.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/28/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import Foundation
class ElemntsModel: NSObject {
    var duration = KelomtrModel()
    var distance = KelomtrModel()
    
    func getObject(dicc: [String: Any]) -> ElemntsModel {
        let dic = HandleJSON.getObject().handle(dicc: dicc)
        let model = ElemntsModel()
        model.distance = KelomtrModel().getObject(dicc: dic["distance"] as! [String:Any])
        model.duration = KelomtrModel().getObject(dicc: dic["duration"] as! [String:Any])

        
        return model
    }
}
