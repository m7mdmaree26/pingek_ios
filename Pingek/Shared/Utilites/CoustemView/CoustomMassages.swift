//
//  CoustomMassages.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/20/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import Foundation
import Localize_Swift
import UIKit
import SwiftMessages
import NotificationBannerSwift
import SwiftEntryKit


func editTextError(editText:UITextField) {
    editText.layer.cornerRadius = editText.frame.height/2
    editText.layer.borderWidth = 1
    editText.layer.borderColor = UIColor.red.cgColor
}

func editTextValid(editText:UITextField) {
    editText.layer.cornerRadius = editText.frame.height/2
    editText.layer.borderWidth = 0
    editText.layer.borderColor = UIColor.lightGray.cgColor
    
}


func ShowErrorMassge(massge:String , title:String){
    
 
    var attributes = EKAttributes.topFloat
    attributes.entryBackground = .gradient(gradient: .init(colors: [EKColor(UIColorFromRGB(rgbValue: 0xB13232)), EKColor(UIColorFromRGB(rgbValue: 0xB13232))], startPoint: .zero, endPoint: CGPoint(x: 1, y: 1)))
    attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3), scale: .init(from: 1, to: 0.7, duration: 0.7)))
    attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 10, offset: .zero))
    attributes.statusBar = .dark
    attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
    attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.bounds.width - 20), height: .intrinsic)
    
    var title = EKProperty.LabelContent(text:  "  " + title, style: .init(font: UIFont(name: "JFFlat-Medium", size: 17)!, color: EKColor(UIColor.white)))
    var description = EKProperty.LabelContent(text:  "  " + massge, style: .init(font: UIFont(name: "JFFlat-Medium", size: 15)!, color: EKColor(UIColor.white)))
   if AppLanguage.getLang().contains("en"){
        title.style.alignment = .left
        description.style.alignment = .left
    }else{
        title.style.alignment = .right
        description.style.alignment = .right
    }
    let simpleMessage = EKSimpleMessage(title: title, description: description)
    let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)
    
    let contentView = EKNotificationMessageView(with: notificationMessage)
    SwiftEntryKit.display(entry: contentView, using: attributes)
    
}

func ShowTrueMassge(massge:String , title:String){
    
     var attributes = EKAttributes.topFloat
    attributes.entryBackground = .gradient(gradient: .init(colors: [EKColor(UIColor.BasicColor), EKColor(UIColor.BasicColor)], startPoint: .zero, endPoint: CGPoint(x: 1, y: 1)))
           attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3), scale: .init(from: 1, to: 0.7, duration: 0.7)))
           attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 10, offset: .zero))
           attributes.statusBar = .dark
           attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
           attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.bounds.width - 20), height: .intrinsic)
           
           var title = EKProperty.LabelContent(text: "  " + title, style: .init(font: UIFont(name: "JFFlat-Medium", size: 17)!, color: EKColor(UIColor.white)))
           var description = EKProperty.LabelContent(text:  "  " + massge, style: .init(font: UIFont(name: "JFFlat-Medium", size: 15)!, color: EKColor(UIColor.white)))
            if AppLanguage.getLang().contains("en"){
                title.style.alignment = .left
                description.style.alignment = .left
            }else{
                title.style.alignment = .right
                description.style.alignment = .right
            }
           
           let simpleMessage = EKSimpleMessage(title: title, description: description)
           let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)
           
           let contentView = EKNotificationMessageView(with: notificationMessage)
           SwiftEntryKit.display(entry: contentView, using: attributes)
    
}


func ShowInformationMassge(massge:String , title:String){
    let view = MessageView.viewFromNib(layout: .statusLine)
    view.configureTheme(.warning)
    
    view.configureDropShadow()

    SwiftMessages.defaultConfig.duration = .seconds(seconds: 2)
    view.configureContent(title:title.localized(), body: massge.localized(), iconImage:#imageLiteral(resourceName: "logo"), iconText:nil, buttonImage:nil, buttonTitle: "OK") { _ in
        SwiftMessages.hide()}
    view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    (view.backgroundView as? CornerRoundingView)?.layer.cornerRadius = 10
    SwiftMessages.show(view: view)
    
}


func NoInterNetMassge(){
    
    let banner = StatusBarNotificationBanner(title:"No Internet..Waiting to reconnect".localized())
    //banner.subtitleLabel?.textColor = AppUtils.getObject().chatgradientColor()
    //banner.subtitleLabel?.font = UIFont(name: "JFFlat-Regular", size: 14)
    // banner.titleLabel?.textColor = AppUtils.getObject().chatgradientColor()
    banner.backgroundColor = #colorLiteral(red: 0.9667869607, green: 0.8070821271, blue: 0.2714624504, alpha: 1)
    
    banner.dismissOnSwipeUp = true
    banner.show()
    
}
func PrayNote(massge:String){
    
    let view = MessageView.viewFromNib(layout: .cardView)
    var config = SwiftMessages.Config()
    config.presentationStyle = .center
    config.duration = .forever
    
    view.configureTheme(.info)
    view.configureDropShadow()
    view.configureBackgroundView(width: 250)
    view.configureContent(title: "", body: massge , iconText:"🕌")
    view.button?.isHidden = true
    
    (view.backgroundView as? CornerRoundingView)?.layer.cornerRadius = 10
    
    // Show the message.
    SwiftMessages.show(config: config, view: view)
    
}



func ShowNoConnectionMassge(massge:String , title:String){
    let view = MessageView.viewFromNib(layout:.statusLine)
    view.backgroundColor = UIColor.yellow
    
    var config = SwiftMessages.defaultConfig
    config.presentationStyle = .center
    config.duration = .forever
    
    
    (view.backgroundView as? CornerRoundingView)?.layer.cornerRadius = 10
    SwiftMessages.show(view: view)
    
}
