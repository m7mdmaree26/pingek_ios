//
//  LocationMapModel.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/23/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import Foundation
struct LocationMapModel {
    var lat:Double
    var addresse:String
    var lang:Double
    
    func toDic()->[String:Any]{
        let dic = [
            "addresse":addresse,
            "lat":lat,
            "lang":lang
            ] as [String : Any]
        return dic
    }
}
