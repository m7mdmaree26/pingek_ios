//
//  StoreTimesVC.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/23/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit

class StoreTimesVC: UIViewController {
    
    var times = [String]()
    
    @IBOutlet weak var timesTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timesTable.tableFooterView = UIView()
    }

    @IBAction func closeTimes(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    
}

extension StoreTimesVC : UITableViewDataSource ,UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.times.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TimesTableCell", for: indexPath) as! TimesTableCell
        cell.timeLbl.text = self.times[indexPath.row]
            
        return cell
    }
        
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }

}
