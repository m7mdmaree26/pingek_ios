//
//  OrderDetilsModel.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/10/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import Foundation
class OrderDetilsModel{
    var id = ""
    var username = ""
    var avatar = ""
    var rating = ""
    var store_id = ""
    var store_name = ""
    var store_icon = ""
    var deliver_time = ""
    var receive_lat = ""
    var receive_long = ""
    var receive_address = ""
    var deliver_lat = ""
    var deliver_long = ""
    var deliver_address = ""
    var app_percentage = ""
    var price = ""
    var min_expected_price = ""
    var max_expected_price = ""
    var expected_price_msg = ""
    var details = [wattingOrdersDetails]()
    var status = ""
    var conversation_id = ""
    var have_invoice = ""
    var distance_to_store = ""
    var distance_to_client = ""

    func getObject(dicc: [String: Any]) ->  OrderDetilsModel {
        let dic = HandleJSON.getObject().handle(dicc: dicc)
        let model =  OrderDetilsModel()
        model.id = dic["id"] as! String
        model.username = dic["username"] as! String
        model.avatar = dic["avatar"] as! String
        model.rating = dic["rating"] as! String
        model.store_id = dic["store_id"] as! String
        model.store_name = dic["store_name"] as! String
        model.store_icon = dic["store_icon"] as! String
        model.deliver_time = dic["deliver_time"] as! String
        model.receive_lat = dic["receive_lat"] as! String
        model.receive_long = dic["receive_long"] as! String
        model.receive_address = dic["receive_address"] as! String
        model.deliver_lat = dic["deliver_lat"] as! String
        model.deliver_long = dic["deliver_long"] as! String
        model.deliver_address = dic["deliver_address"] as! String
        model.app_percentage = dic["app_percentage"] as! String
        model.price = dic["price"] as! String
        model.min_expected_price = dic["min_expected_price"] as! String
        model.max_expected_price = dic["max_expected_price"] as! String
        model.expected_price_msg = dic["expected_price_msg"] as! String
        for item in dic["details"] as! [[String: Any]] {
            model.details.append(wattingOrdersDetails().getObject(dicc: item))
        }
        model.status = dic["status"] as! String
        model.conversation_id = dic["conversation_id"] as! String
        model.have_invoice = dic["have_invoice"] as! String
        model.distance_to_store = dic["distance_to_store"] as! String
        model.distance_to_client = dic["distance_to_client"] as! String
        return model
              
    }
}
