
//
//  ComplimentList.swift
//  Sandkom
//
//  Created by Engy Bakr on 2/1/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import Foundation
class  ComplimentList{
    var id = ""
    var subject = ""
    var username = ""
    var user_id = ""
    var order_id = ""
    var avatar = ""
    var store_name = ""
    var store_icon = ""
    var text = ""
    var answer = ""
    var status = ""
    var date = ""
    func getObject(dicc: [String: Any]) ->  ComplimentList {
        let dic = HandleJSON.getObject().handle(dicc: dicc)
        let model =  ComplimentList()
        model.id = dic["id"] as! String
        model.subject = dic["subject"] as! String
        model.username = dic["username"] as! String
        model.user_id = dic["user_id"] as! String
        model.order_id = dic["order_id"] as! String
        model.avatar = dic["avatar"] as! String
        model.store_name = dic["store_name"] as! String
        model.store_icon = dic["store_icon"] as! String
        model.text = dic["text"] as! String
        model.answer = dic["answer"] as! String
        model.status = dic["status"] as! String
        model.answer = dic["date"] as! String

        return model
        
    }
}
