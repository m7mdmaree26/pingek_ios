//
//  StoreCommentsVC.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/22/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import Alamofire

class StoreCommentsVC: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var commentsTable: UITableView!
    
    var storeID = ""
    var comments = [StoreRates]()
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController!.navigationBar.backgroundColor = UIColor.clear
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        self.showProgress()
        getComments()
        

    }
    
    func setupView(){
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)
        commentsTable.tableFooterView = UIView(frame: .zero)


    }
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func getComments(){
        self.comments.removeAll()
        let params:Parameters = [
            "store_id":self.storeID,
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId()
        ]
            API.POST(url: storeComments_URL, parameters: params, headers: header, withIndicator: false) { (succss, value) in
                if succss{
                    self.stopAnimating()
                    print("🤭\(value)")
                    let key = value["key"] as! String
                    if (key == "success"){
                    let comments = value["data"] as! [[String:Any]]
                    if comments.count != 0 {
                        for i in comments {
                            self.comments.append(StoreRates().getObject(dicc: i))
                        }
                        self.commentsTable.reloadData()
                    }else{
                        
                        //=======empty
                        
                    }
                }
                         
            }else{
                self.stopAnimating()
            }
                     
        }
    }
    
    func LikeOrDislikeComment(comment_id: String,action: String){
        let params:Parameters = [
            "comment_id":comment_id,
            "action":action,
            
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
            
        API.POST(url: likeStoreComment_URL , parameters: params, headers: header) { (succss, value) in
            if succss{
                let key = value["value"] as! String
                if key == "1" {
                    self.getComments()
                }
            }else{                
                ShowErrorMassge(massge: "Something went wrong and tried again".localized, title: "Error".localized)
            }
        }
    }

        
}



extension StoreCommentsVC : UITableViewDataSource ,UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.comments.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentsTableCell", for: indexPath) as! CommentsTableCell
        cell.userName.text = self.comments[indexPath.row].name
        cell.comment.text = self.comments[indexPath.row].comment
        cell.userImage.setImageWith(self.comments[indexPath.row].avatar)
        cell.time.text = self.comments[indexPath.row].date
        cell.rateView.rating = Double(self.comments[indexPath.row].rate)!
        cell.LikeButton.setTitle(" " + self.comments[indexPath.row].num_like, for: .normal)
        cell.disLikeButton.setTitle(" " + self.comments[indexPath.row].num_dislike, for: .normal)
        
        if self.comments[indexPath.row].is_liked == "1" {
            if self.comments[indexPath.row].action == "dislike"{
                cell.disLikeButton.setImage(#imageLiteral(resourceName: "dislikeactive"), for: .normal)
                cell.LikeButton.setImage(#imageLiteral(resourceName: "like"), for: .normal)


            }else{
                cell.LikeButton.setImage(#imageLiteral(resourceName: "likeactive"), for: .normal)
                cell.disLikeButton.setImage(#imageLiteral(resourceName: "dislike"), for: .normal)

            }
        }else{
            cell.disLikeButton.setImage(#imageLiteral(resourceName: "dislike"), for: .normal)
            cell.LikeButton.setImage(#imageLiteral(resourceName: "like"), for: .normal)
        }
        cell.likeAction = {
            self.LikeOrDislikeComment(comment_id: self.comments[indexPath.row].id,action: "like")
        }
        cell.disLikeAction = {
            self.LikeOrDislikeComment(comment_id: self.comments[indexPath.row].id,action: "dislike")
        }
        return cell
    }
        
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 140
    }

}
