//
//  ChatViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/12/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Photos
import SocketIO
import SwiftyJSON
import Alamofire
import Cosmos
import ActionSheetPicker_3_0
import AVFoundation
import KMPlaceholderTextView
import Photos
import MapKit
import AVKit
import EzPopup


class ChatViewController: UIViewController ,UITableViewDataSource ,UITableViewDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AMChoiceDelegate,UIGestureRecognizerDelegate{
    
    var selectedId = ""
    
   func didSelectRowAt(indexPath: IndexPath) {
      print("item at index \(indexPath.row) selected")
     selectedId = self.myItems[indexPath.row].oilId
    print("🌸\(self.myItems[indexPath.row].oilId)")
    }

    var player: AVAudioPlayer!
    var player2:AVAudioPlayer = AVAudioPlayer()
    var soundFileURL: URL!
    var recorder: AVAudioRecorder!
    var OrderID:Int?
    var chatMassges = [String]()
    var currentInputView: UIView?
    var Massges = [MassgeModel]()
    var CancelResons = [ResonCancelModel]()
    let paragraphStyle = NSMutableParagraphStyle()
    var ConvrstionId :String?
    var reciever_id:String?
    let date = Date()
    let formatter = DateFormatter()
    var SecoundUserImage = ""
    var imagesData = [Data]()
    var imagesKeys = [String]()
    var flagImage = false
    var imageStr = ""
    var imageData:Data!
    var imageIndex = ""
    var SoundIndex = ""
    var lat:Double?
    var lng:Double?
    var isProvider:Bool?
    var LocationMap = [LocationMapModel]()
    var DelvryPrice = ""
    var mobileNumber = ""
    var userName = ""
    var UserRate :Double?
    var secondUSerImage = ""
    var ResiverLat : Double?
    var ResiverLong : Double?
    var DeliverLat : Double?
    var DeliverLng : Double?
    var UserLat:Double?
    var UserLng:Double?
    var storeAddress = ""
    var clintAddress = ""
    var placeName = ""
    var storeId = ""
    var storeIcon = ""
    
    static var InChat:Bool!
    static var FinshInChat:Bool!
    static var IssuChat:Bool!
    
    @IBOutlet weak var placeHolder: UIView!
    @IBOutlet weak var indicator: InstagramActivityIndicator!
    @IBOutlet weak var OrderNumber: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak var ratingBar: CosmosView!
    @IBOutlet weak var SecondImage: UIImageView!
    @IBOutlet weak var secondUserName: UILabel!
    @IBOutlet weak var secondUserDistance: UILabel!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var resonView: UIView!
    @IBOutlet weak var ChatTextView: KMPlaceholderTextView!
    @IBOutlet weak var choiseTableView: AMChoice!
    @IBOutlet weak var chatTextFeildHeight: NSLayoutConstraint!
    @IBOutlet weak var MultiAlertView: UIView!
    @IBOutlet weak var chatSentViewHeight: NSLayoutConstraint!
    

    
    @IBAction func mabButton(_ sender: Any) {
        
        let Reg =   Storyboard.Home.viewController(DeliveryTripViewController.self)
        Reg.userTrackId = self.reciever_id!
        
        Reg.userTrackId = self.reciever_id!
        
        Reg.ResiverLat = self.ResiverLat
        Reg.ResiverLong = self.ResiverLong
        
        Reg.DeliverLng = self.DeliverLng!
        Reg.DeliverLat = self.DeliverLat!
        
        Reg.Userelat = self.UserLat!
        Reg.UserLng = self.UserLng!
        
        Reg.ClintAddress = self.clintAddress
        Reg.Addressstore = self.storeAddress
        Reg.RestruntName = self.placeName
        self.MultiAlertView.isHidden = true
        self.navigationController?.pushViewController(Reg, animated: false)
        
    }
    
    @IBOutlet weak var goToMap: UIButton!
    @IBOutlet weak var startRecord: UIButton!
    @IBOutlet weak var issuBillBtn: UIBarButtonItem!
    @IBOutlet weak var cancelBtn: RoundedButton!
    @IBOutlet weak var cancelOrderButton: UIButton!
    @IBOutlet weak var finishOrderButton: UIButton!
    @IBOutlet weak var seperatoview: UIView!
    @IBOutlet weak var alertViewHeight: NSLayoutConstraint!
    
   
    @IBAction func sentAction(_ sender: Any) {
        
        if(self.ChatTextView.text.isEmpty == true){
            print("enptyy")
        }else{
            if(SocketConnection.sharedInstance.socket.status == .connected){
                formatter.dateFormat = "yyyy-MM-resultdd"
                let result = formatter.string(from: date)
                self.Massges.append(MassgeModel(user_id: Int(getUserID())!, username: getUserName(), content: self.ChatTextView.text!, type: "text", date: result, avatar: getUserAvatar(), sender: "you", lat: 0, long: 0))
                
                
                let test = ["sender_id": getUserID(), "receiver_id": reciever_id!,"conversation_id":ConvrstionId!,"content": self.ChatTextView.text!,"type":"text","time_zone":getTimeZone()] as [String : Any]
                SocketConnection.sharedInstance.socket.emit("sendmessage", test)
                self.tableView.reloadData()
                self.GetLastMassge()
                print("💕)")
                self.ChatTextView.text = ""
                self.chatTextFeildHeight.constant = self.ChatTextView.contentSize.height
                self.chatSentViewHeight.constant = self.ChatTextView.contentSize.height + 20
            }else{
                print("errror")
            }
        }
    }
    @IBOutlet weak var sorryLbl: UILabel!
    
    @IBAction func issuBillAction(_ sender: Any) {
        let Reg =   Storyboard.Home.viewController(IssuBillViewController.self)
        Reg.orderPrice = self.DelvryPrice
        Reg.OrderId = String(self.OrderID!)
        self.MultiAlertView.isHidden = true
        self.navigationController?.pushViewController(Reg, animated: false)
    }
    
    @IBAction func callAction(_ sender: Any) {
        print("💙\(self.mobileNumber)")
        guard let url = URL(string: "tel://\(self.mobileNumber)") else {
            return
        }
        
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    @IBOutlet weak var trackButton: UIButton!
    
    @IBAction func trackOrderAction(_ sender: Any) {
        self.MultiAlertView.isHidden = true
          if(isProvider == false){
             let Reg = Storyboard.Home.viewController(TrackViewController.self)
            Reg.userTrackId = self.reciever_id!
            
            Reg.ResiverLat = self.ResiverLat
            Reg.ResiverLong = self.ResiverLong
            
            Reg.DeliverLng = self.DeliverLng!
            Reg.DeliverLat = self.DeliverLat!
            
            Reg.Userelat = self.UserLat!
            Reg.UserLng = self.UserLng!
            self.MultiAlertView.isHidden = true
            self.navigationController?.pushViewController(Reg, animated: false)
          }else{
            let Reg = Storyboard.Authentication.viewController(GetLocationViewController.self)
            Reg.delegate = self
            self.MultiAlertView.isHidden = true
            self.navigationController?.pushViewController(Reg, animated: false)
            
        }
    }
    
    @IBAction func isDeliveredAction(_ sender: Any) {
        self.MultiAlertView.isHidden = true
       self.FinshOrder(order_id: String(self.OrderID!))
    }
    @IBAction func AddImageAction(_ sender: Any) {
        self.MultiAlertView.isHidden = true
        self.UpladImage()
    }
    @IBAction func cancelOrderAction(_ sender: Any) {
        self.MultiAlertView.isHidden = true
        self.placeHolder.isHidden = false
        self.resonView.isHidden = false
    }
    
    @IBAction func moreAction(_ sender: Any) {
        if MultiAlertView.isHidden{
            MultiAlertView.isHidden = false
        }else{
            MultiAlertView.isHidden = true
        }

    }
    @IBAction func sentComplaint(_ sender: Any) {
        let Reg = Storyboard.Home.viewController(ComplaintsViewController.self)
        Reg.orderId = String(describing: self.OrderID!)
        self.MultiAlertView.isHidden = true
        self.navigationController?.pushViewController(Reg, animated: false)
        
    }
    

    
    @IBAction func cancelAction(_ sender: Any) {
        if(isProvider == false){
       
            self.CancelOrder(order_id: String(self.OrderID!), reason_id: self.selectedId)
       
        }else{
            self.showProgress()
            self.WithdrawOrder(order_id:self.OrderID!, reason_id: self.selectedId)
            self.placeHolder.isHidden = true
            self.resonView.isHidden = true
        }
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        return true
    }
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
    func UpladImage(){
        var myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(myPickerController, animated: false, completion: nil)
    }
    
    internal func imagePickerController(_ picker:UIImagePickerController,didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]){
        if let imageURL = info[.imageURL] as? URL {
        let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
        let asset = result.firstObject
        
        }
        let image_data = info[.originalImage] as! UIImage
        self.imageData = (image_data).pngData()! as Data
        
        imagesData.append(self.imageData!)
        imagesKeys.append("file")
        self.dismiss(animated: true) {
            self.Massges.append(MassgeModel(user_id: Int(getUserID())!, username: getUserName(), content: "", type: "image", date: "4", avatar: getUserAvatar(), sender: "you", lat: 0, long: 0))
            self.imageIndex = String(describing: self.Massges.count)
           
            self.tableView.reloadData()
            self.GetLastMassge()
            self.UploadImageToSocket()
            
        }
        
    }
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.estimatedRowHeight =  UIScreen.main.bounds.height/2
        self.tableView.rowHeight = UITableView.automaticDimension
        self.ChatTextView.delegate = self
        self.ChatTextView.layer.cornerRadius = 20
        self.ChatTextView.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        navigationController?.hidesBarsWhenKeyboardAppears = false
        ChatTextView.textContainerInset = .init(top: 15.0, left: 10.0, bottom: 5.0, right: 5.0)
        ChatTextView.sizeToFit()
        self.choiseTableView.selectedImage = #imageLiteral(resourceName: "check_on")
        self.choiseTableView.unselectedImage = #imageLiteral(resourceName: "check_off")
        getChatData()
        self.goToMap.setImage(#imageLiteral(resourceName: "noun_maplocation"), for: .normal)
        self.ChatTextView.delegate = self
        self.ChatTextView.placeholder  = "Enter Your Massge here..".localized
        self.indicator.lineWidth = 3
        self.indicator.numSegments = 20
        self.indicator.strokeColor = #colorLiteral(red: 0.9529411793, green: 0.8761406154, blue: 0.242189706, alpha: 1)
        self.indicator.startAnimating()
        self.resonView.isHidden = true
        self.placeHolder.isHidden = true
        self.viewDesign()
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
        self.tableView.addGestureRecognizer(tap)
        self.placeHolder.addGestureRecognizer(tap)
        self.sorryLbl.text = "Sorry ... Select the reason for the cancellation".localized
        self.cancelBtn.setTitle("Cancel".localized, for: .normal)
        self.tableView.backgroundColor = UIColor.clear
        self.navigationController?.hidesBarsOnSwipe = false
        MultiAlertView.isHidden = true
        SecondImage.clipsToBounds = true
        
        startRecord.addTarget(self, action: #selector(startRecordAction), for: .touchDown)
        startRecord.addTarget(self, action: #selector(stopRecord), for: [.touchUpInside, .touchUpOutside])
        
        defaults.set(self.ConvrstionId, forKey: "Convrstion_Id")
        defaults.set("true", forKey: "isopenChat")
        
        NotificationCenter.default.addObserver(self, selector: #selector(getNotfy(notifcation:)), name: .FireNotfication, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reteView), name: .reteView, object: nil)


    }
    @objc func getNotfy(notifcation:Notification){
        self.getChatData()
        
    }
    @objc func reteView(notifcation:Notification){
        let vc = Storyboard.Home.viewController(RateViewController.self)
        vc.Name = self.userName
        vc.RateValu = self.UserRate!
        vc.ProviderID = self.reciever_id!
        vc.uerAvatar = self.secondUSerImage
        vc.vc = self
        let popupVC = PopupViewController(contentController: vc, popupWidth: UIScreen.main.bounds.width-60, popupHeight: 340)
                       
        popupVC.backgroundAlpha = 0.5
        popupVC.backgroundColor = .black
        popupVC.canTapOutsideToDismiss = false
        popupVC.cornerRadius = 20
        popupVC.shadowEnabled = true
                        
                           
        self.present(popupVC, animated: true)
           
    }
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        self.resonView.isHidden = true
        self.placeHolder.isHidden = true
        if !MultiAlertView.isHidden{
            MultiAlertView.isHidden = true
        }
    }
    func addLeftBarIcon(named:String,Link:String,placeName:String) {

        let BackButton = UIButton()
        BackButton.addTarget(self, action: #selector(popView), for: .touchUpInside)
        if AppLanguage.currentLanguage().contains("en") {
            BackButton.setImage(#imageLiteral(resourceName: "arrow_right"), for: .normal)
        }else{
            BackButton.setImage(#imageLiteral(resourceName: "arrow_left"), for: .normal)
        }
        let titleLable = UILabel()
        titleLable.text = placeName
        titleLable.font = UIFont(name:  "JFFlat-Medium", size: 15)!
        navigationItem.leftItemsSupplementBackButton = true
        navigationItem.leftBarButtonItems = [UIBarButtonItem.init(customView: BackButton),UIBarButtonItem.init(customView: titleLable)]
        //navigationItem.title = placeName
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.black]
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name:  "JFFlat-Medium", size: 15)!]
        
    }
    @objc func popView(){
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(),for: .default)
        defaults.set("false", forKey: "isopenChat")

        
    }
    override func viewWillAppear(_ animated: Bool) {
       // self.navigationController?.navigationBar.topItem?.title = ""
        
         self.navigationController!.navigationBar.setBackgroundImage(UIImage(color: UIColor.white, size: CGSize(width: UIScreen.main.bounds.width, height: 60)),for: .default)
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(IssuBill(notifcation:)), name: .IssuBill, object: nil)
         ConnectToSocket()
        ChatViewController.InChat = true
        ChatViewController.FinshInChat = true
        ChatViewController.IssuChat = true
        self.hideKeyboardWhenTappedAround()
    }
    @objc func IssuBill(notifcation:Notification){
        getChatData()
    }
    @objc func playSound() {
        let systemSoundID: SystemSoundID = 1007
        AudioServicesPlaySystemSound(systemSoundID)
    }
    override func viewDidDisappear(_ animated: Bool) {
        ChatViewController.InChat = false
        ChatViewController.FinshInChat = false
        ChatViewController.IssuChat = false
        
        print("🌚")
    }
    
    func ConnectToSocket() {
        
        SocketConnection.sharedInstance.manager.connect()
        
        SocketConnection.sharedInstance.socket.once(clientEvent: .connect) { (data, ack) in
            let jsonDic = ["conversation": self.ConvrstionId!, "client": getUserID()]
            SocketConnection.sharedInstance.socket.emit("adduser", jsonDic)
            print("💕💕💕AdddUssser)")
        }
        SocketConnection.sharedInstance.socket.on(clientEvent: .connect) { (data, ack) in
            print("🍋Connect")
            print("🍉\(data)")
            
            self.SockeConFigration()
           
        }
        SocketConnection.sharedInstance.socket.on(clientEvent: .error) { (data, ack) in
            print("🍋Error")
            print("🍉\(data)")
        }
        
        SocketConnection.sharedInstance.socket.on(clientEvent: .disconnect) { (data, ack) in
            print("🍋disconnect")
            print("🍉\(data)")
        }
        SocketConnection.sharedInstance.socket.on(clientEvent: .ping) { (data, ack) in
            print("🍋Ping")
            print("🍉\(data)")
        }
        
        SocketConnection.sharedInstance.socket.on(clientEvent: .reconnect) { (data, ack) in
            print("🍋reconnect")
            print("🍉\(data)")
        }
        

    }
    
    
    func SockeConFigration(){
        SocketConnection.sharedInstance.socket.on("message") { (data, ack) in
            print("🌻\(data)")
            let dict = data[0] as! [String: Any]
            print("💙\(dict["content"] as? String ?? "")")
            let type = dict["type"] as? String ?? ""
            
            switch type {
            case "text":
                self.Massges.append(MassgeModel(user_id: Int(getUserID())!, username: getUserName(), content: dict["content"] as? String ?? "", type: dict["type"] as? String ?? "", date: "" , avatar: self.SecoundUserImage, sender: "seconduser", lat: 0, long: 0))
                self.playSound()
                self.tableView.reloadData()
                self.GetLastMassge()

            case "image":
                self.Massges.append(MassgeModel(user_id: Int(getUserID())!, username: getUserName(), content: dict["content"] as? String ?? "", type: dict["type"] as? String ?? "", date: "" , avatar: self.SecoundUserImage, sender: "seconduser", lat: 0, long: 0))
                self.playSound()
                self.tableView.reloadData()
                self.GetLastMassge()

            case "map":
                let jsonString = dict["content"] as? String
                let data: Data? = jsonString?.data(using: .utf8)
                let json = (try? JSONSerialization.jsonObject(with: data!, options: [.allowFragments])) as? [String:AnyObject]
                print(json ?? "Empty Data")
                //                for i in json!{
                let lat = json!["lat"]  as? Double
                let lang = json!["lang"]  as? Double
                self.Massges.append(MassgeModel(user_id: Int(getUserID())!, username: getUserName(), content: dict["content"] as? String ?? "", type: dict["type"] as? String ?? "", date: "" , avatar: self.SecoundUserImage, sender: "seconduser", lat: lat! , long:lang! ))
                self.playSound()
                self.tableView.reloadData()
                self.GetLastMassge()
         
                
            case "sound":
                let jsonString = dict["content"] as? String
                let data: Data? = jsonString?.data(using: .utf8)
                let json = (try? JSONSerialization.jsonObject(with: data!, options: [.allowFragments])) as? [String:AnyObject]
                print(json ?? "Empty Data")
                
                
                self.Massges.append(MassgeModel(user_id: Int(getUserID())!, username: getUserName(), content: dict["content"] as? String ?? "", type: dict["type"] as? String ?? "", date: "" , avatar: self.SecoundUserImage, sender: "seconduser", lat: 0, long: 0))
                self.playSound()
                self.tableView.reloadData()
                self.GetLastMassge()
            default :
                print("")
            }
           
        }
       
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        //self.topImageView.constant = ((self.navigationController?.navigationBar.frame.height)!)
        self.chatTextFeildHeight.constant = self.ChatTextView.contentSize.height
        self.chatSentViewHeight.constant = self.ChatTextView.contentSize.height + 20

        self.view.layoutIfNeeded()
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        //  self.topImageView.constant = 20
        self.chatTextFeildHeight.constant = self.ChatTextView.contentSize.height
        self.chatSentViewHeight.constant = self.ChatTextView.contentSize.height + 20
        self.view.layoutIfNeeded()
    }
    func textViewDidChange(_ textView: UITextView) {
        self.chatTextFeildHeight.constant = self.ChatTextView.contentSize.height
        self.chatSentViewHeight.constant = self.ChatTextView.contentSize.height + 20
        ChatTextView.isScrollEnabled = true
    }
    
    
    
    ////////////////==================>>>>>>> TableView((SecondView))
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.Massges.count
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func GetLastMassge(){
        if self.Massges.count != 0 {
            let i = IndexPath(row: 0, section: self.Massges.count-1)
            self.tableView.scrollToRow(at: i , at: UITableView.ScrollPosition.middle, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatTableViewCell", for: indexPath) as! ChatTableViewCell
        let cellImage = tableView.dequeueReusableCell(withIdentifier: "ImageTableViewCell", for: indexPath) as! ImageTableViewCell
        let cellLocation = tableView.dequeueReusableCell(withIdentifier: "MapTableViewCell", for: indexPath) as! MapTableViewCell
        let Soundcell = tableView.dequeueReusableCell(withIdentifier: "SoundTableViewCell", for: indexPath) as! SoundTableViewCell
        
        switch Massges[indexPath.section].type {
        case "text":
            print("🌸Text")
            cell.selectionStyle = .none
            if(self.Massges[indexPath.section].sender == "you"){
                cell.massge.text = self.Massges[indexPath.section].content
                cell.UserResveImage.isHidden =  true
                cell.UsersenderImage.isHidden =  false
                cell.UsersenderImage.setImageWith(Massges[indexPath.section].avatar)
                cell.layoutIfNeeded()
                cell.massgeBody.backgroundColor = UIColor.BasicColor
                cell.massge.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

               
            }else{
                cell.massge.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell.massgeBody.backgroundColor = #colorLiteral(red: 0.8569236425, green: 0.8569236425, blue: 0.8569236425, alpha: 1)
                cell.massge.text = self.Massges[indexPath.section].content
                cell.UsersenderImage.isHidden =  true
                cell.UserResveImage.isHidden =  false
                cell.UserResveImage.setImageWith(Massges[indexPath.section].avatar)
                cell.layoutIfNeeded()
             
            }
            
            return cell
        case "image":
            print("🖍Image")
            cellImage.selectionStyle = .none
            if(self.Massges[indexPath.section].sender == "you"){
                cellImage.MassgeImage.setImageWith(Massges[indexPath.section].content)
                cellImage.Loder.stopAnimating()
                cellImage.UserResveImage.isHidden =  true
                cellImage.UserSenderImage.isHidden =  false
                cellImage.MassgeImage.layer.cornerRadius = 15
                cellImage.MassgeImage.clipsToBounds = true
                cellImage.MassgeImage.layer.borderColor = UIColor.BasicColor.cgColor
                cellImage.MassgeImage.layer.borderWidth = 1.5
                cellImage.UserSenderImage.setImageWith(Massges[indexPath.section].avatar)
                cellImage.layoutIfNeeded()
                cellImage.layoutSubviews()
                
            }else{
                cellImage.MassgeImage.setImageWith(Massges[indexPath.section].content)
                cellImage.Loder.stopAnimating()
                cellImage.UserResveImage.isHidden =  false
                cellImage.UserSenderImage.isHidden =  true
                cellImage.MassgeImage.layer.cornerRadius = 15
                cellImage.MassgeImage.clipsToBounds = true
                cellImage.MassgeImage.layer.borderColor = UIColor.BasicColor.cgColor
                cellImage.MassgeImage.layer.borderWidth = 1.5
                cellImage.UserResveImage.setImageWith(Massges[indexPath.section].avatar)
                cellImage.layoutIfNeeded()
                cellImage.layoutSubviews()
            }
            return cellImage
        case "map":
            print("🖍map")
            if(self.Massges[indexPath.section].sender == "you"){
                cellLocation.getSnapShotMap(lat: Massges[indexPath.section].lat, Long:  Massges[indexPath.section].long)
                cellLocation.mapImage.clipsToBounds = true
                cellLocation.UserResveImage.isHidden =  true
                cellLocation.UserSenderImage.isHidden =  false
                cellLocation.mapImage.layer.borderColor = UIColor.BasicColor.cgColor
                cellLocation.mapImage.layer.borderWidth = 1.5
                cellLocation.UserSenderImage.setImageWith(Massges[indexPath.section].avatar)
                cellLocation.layoutIfNeeded()
                cellLocation.layoutSubviews()
                cellLocation.selectionStyle = .none
                
            }else{
                cellLocation.getSnapShotMap(lat: Massges[indexPath.section].lat, Long:  Massges[indexPath.section].long)
              
                cellLocation.mapImage.clipsToBounds = true
                cellLocation.UserResveImage.isHidden =  false
                cellLocation.UserSenderImage.isHidden =  true
                cellLocation.mapImage.layer.borderColor = UIColor.BasicColor.cgColor
                cellLocation.mapImage.layer.borderWidth = 1.5
                cellLocation.UserResveImage.setImageWith(Massges[indexPath.section].avatar)
                cellLocation.layoutIfNeeded()
                cellLocation.layoutSubviews()
                cellLocation.selectionStyle = .none
            }
            
            return cellLocation
        case "sound":
            
            if(self.Massges[indexPath.section].sender == "you"){
                let videoUrl = NSURL(string: self.Massges[indexPath.section].content)
                let avPlayer = AVPlayer(url: videoUrl! as URL)
                Soundcell.playerView.playerLayer.player = avPlayer
                Soundcell.UserSenderImage.setImageWith(Massges[indexPath.section].avatar)
                Soundcell.UserResveImage.isHidden =  true
                Soundcell.UserSenderImage.isHidden =  false
                Soundcell.ResveMicro.isHidden = true
                Soundcell.layoutIfNeeded()
                Soundcell.selectionStyle = .none
                Soundcell.toogleAction = {
                    if Soundcell.playButton.imageView?.image == #imageLiteral(resourceName: "play-button"){
                        if  Soundcell.playerView.player?.status == .readyToPlay{
                            Soundcell.playerView.player?.play()
                            Soundcell.isFinished = false
                            if Soundcell.isFinished == true{
                                print("seek")
                                Soundcell.playerView.player?.seek(to: CMTime(seconds: 0.0, preferredTimescale: CMTimeScale.max))
                            }
                            //  cell.toogleButton.setTitle("Stop", for: UIControlState.normal)
                            Soundcell.playButton.setImage(#imageLiteral(resourceName: "pause"), for: UIControl.State.normal)
                            var ti = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { (t) in
                                if Soundcell.playButton.imageView?.image == #imageLiteral(resourceName: "pause"){
                                    if Soundcell.playerView.player?.status == .readyToPlay{
                                        Soundcell.updateVideoPlayerSlider()
                                    }
                                }
                                //if video finished stop timer
                                if Soundcell.isFinished{
                                    //stop timer
                                    print("stop here")
                                    //                            cell.toogleButton.setTitle("Play", for: UIControlState.normal)
                                    Soundcell.playButton.setImage(#imageLiteral(resourceName: "play-button"), for: UIControl.State.normal)
                                    Soundcell.slider.value = 0.0
                                    Soundcell.playerView.player?.seek(to: CMTime(seconds: 0.0, preferredTimescale: CMTimeScale.max))
                                    Soundcell.playerView.player?.pause()
                                    Soundcell.PlayTimeLabel.text = "00:00"
                                    Soundcell.isFinished = false
                                    t.invalidate()
                                }
                            })
                        }
                    }else if Soundcell.playButton.imageView?.image == #imageLiteral(resourceName: "pause"){
                        Soundcell.playerView.player?.pause()
                        //                cell.toogleButton.setTitle("Play", for: UIControlState.normal)
                        Soundcell.playButton.setImage(#imageLiteral(resourceName: "play-button"), for: UIControl.State.normal)
                    }
                }
            }else{
                let videoUrl = NSURL(string: self.Massges[indexPath.section].content)
                let avPlayer = AVPlayer(url: videoUrl! as URL)
                Soundcell.playerView.playerLayer.player = avPlayer
               // Soundcell.senderwidth.constant = 0
               // Soundcell.senderMicro.isHidden = true
                Soundcell.UserResveImage.setImageWith(Massges[indexPath.section].avatar)
                //Soundcell.playerView.roundCorners([.topRight,.bottomLeft,.bottomRight], radius: 10)
                Soundcell.layoutIfNeeded()
                Soundcell.senderMicro.isHidden = true
                Soundcell.UserResveImage.isHidden =  false
                Soundcell.UserSenderImage.isHidden =  true
                Soundcell.selectionStyle = .none
                Soundcell.toogleAction = {
                    if Soundcell.playButton.imageView?.image == #imageLiteral(resourceName: "play-button"){
                        if  Soundcell.playerView.player?.status == .readyToPlay{
                            Soundcell.playerView.player?.play()
                            Soundcell.isFinished = false
                            if Soundcell.isFinished == true{
                                print("seek")
                                Soundcell.playerView.player?.seek(to: CMTime(seconds: 0.0, preferredTimescale: CMTimeScale.max))
                            }
                            Soundcell.playButton.setImage(#imageLiteral(resourceName: "pause"), for: UIControl.State.normal)
                            var ti = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { (t) in
                                if Soundcell.playButton.imageView?.image == #imageLiteral(resourceName: "pause"){
                                    if Soundcell.playerView.player?.status == .readyToPlay{
                                        Soundcell.updateVideoPlayerSlider()
                                    }
                                }
                                //if video finished stop timer
                                if Soundcell.isFinished{
                                    //stop timer
                                    print("stop here")
                                    //                            cell.toogleButton.setTitle("Play", for: UIControlState.normal)
                                    Soundcell.playButton.setImage(#imageLiteral(resourceName: "play-button"), for: UIControl.State.normal)
                                    Soundcell.slider.value = 0.0
                                    Soundcell.playerView.player?.seek(to: CMTime(seconds: 0.0, preferredTimescale: CMTimeScale.max))
                                    Soundcell.playerView.player?.pause()
                                    Soundcell.PlayTimeLabel.text = "00:00"
                                    Soundcell.isFinished = false
                                    t.invalidate()
                                }
                            })
                        }
                    }else if Soundcell.playButton.imageView?.image == #imageLiteral(resourceName: "pause"){
                        Soundcell.playerView.player?.pause()
                        //                cell.toogleButton.setTitle("Play", for: UIControlState.normal)
                        Soundcell.playButton.setImage(#imageLiteral(resourceName: "play-button"), for: UIControl.State.normal)
                    }
                    
                }
            }
            return Soundcell
            
            
        default:
            print("hello")
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(self.Massges[indexPath.section].type == "map"){
            let coordinate = CLLocationCoordinate2DMake(self.Massges[indexPath.section].lat,(self.Massges[indexPath.section].long))
            let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
            //   mapItem.name = "You"
            mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
        }
        else if(self.Massges[indexPath.section].type == "image"){
//            let Reg = Storyboard.Home.viewController(ShowImageVC.self)
//            Reg.image = self.Massges[indexPath.section].content
          
            let vc = Storyboard.Home.viewController(ShowImageVC.self)
            vc.image = self.Massges[indexPath.section].content
            
            let popupVC = PopupViewController(contentController: vc, popupWidth: UIScreen.main.bounds.width, popupHeight:  UIScreen.main.bounds.height)
                        
            popupVC.backgroundAlpha = 0.5
            popupVC.backgroundColor = .black
            popupVC.canTapOutsideToDismiss = true
            popupVC.cornerRadius = 0
            popupVC.shadowEnabled = true
                            
                            
            self.present(popupVC, animated: true)
        }
    }
    func getChatData(){
        self.showProgress()
        let params:Parameters = [
            "conversation_id": self.ConvrstionId
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        API.POST(url:conversation_URL , parameters: params, headers: header) { (succss, value) in
            if succss{
                self.stopAnimating()
                let key = value["key"] as? String
                let data = value["data"] as? [String:Any]
                if (key == "success"){
                    /////////////==========>>>>>Order Data
                    let order = data!["order"] as? [String:Any]
                    let id = order!["id"] as? Int
                    let place_name = order!["place_name"] as? String
                    let store_id = order!["store_id"] as? Int
                    let place_icon = order!["place_icon"] as? String
                    let deliver_time = order!["deliver_time"] as? Int
                    let receive_lat = order!["receive_lat"] as? Double
                    let receive_long = order!["receive_long"] as? Double
                    let receive_address = order!["receive_address"] as? String
                    let deliver_lat = order!["deliver_lat"] as? Double
                    let deliver_long = order!["deliver_long"] as? Double
                    let deliver_address = order!["deliver_address"] as? String
                    let details = order!["details"] as? Double
                    let price = order!["price"] as? String
                    self.OrderID = id!
                    defaults.set(self.OrderID!, forKey: "order_id")
                    self.secondUserDistance.text = "Price :".localized + " " + price! + " " + "SR".localized()
                    self.OrderNumber.text = "Order Number :".localized() + " " + String(describing: id!)
                    self.DelvryPrice = price!
                    if place_name != nil {
                        self.addLeftBarIcon(named: "222", Link: place_icon!, placeName: place_name!)
                    }else{
                        self.addLeftBarIcon(named: "222", Link: place_icon!, placeName: "")
                    }
                  
                    self.ResiverLat = receive_lat
                    self.ResiverLong = receive_long
                    
                    self.DeliverLat = deliver_lat
                    self.DeliverLng = deliver_long
                    
                    self.storeAddress = receive_address!
                    self.clintAddress = deliver_address!
                    if place_name != nil {
                        self.placeName = place_name!
                    }
                    self.storeId = String(describing:  store_id!)
                    self.storeIcon = place_icon!
                    
                    /////////////==========>>>>>My Data
                    let yourinfo = data!["yourinfo"] as? [String:Any]
                    let idInfo = yourinfo!["id"] as? Int
                    let name = yourinfo!["name"] as? String
                    let lat = yourinfo!["lat"] as? Double
                    let long = yourinfo!["long"] as? Double
                    let is_provider = yourinfo!["is_provider"] as? Bool
                    self.isProvider = is_provider
                    if(self.isProvider == false){
                        self.trackButton.setTitle("Order tracking".localized, for: .normal)
                        self.cancelOrderButton.setTitle("Cancel order".localized, for: .normal)
                        self.finishOrderButton.isHidden = true
                        self.seperatoview.isHidden = true
                        self.alertViewHeight.constant = 163
                    }else{
                        self.trackButton.setTitle("Attach the address".localized, for: .normal)
                         self.cancelOrderButton.setTitle("Withdrawal from  Order".localized, for: .normal)
                    }
                    
                    /////////////==========>>>>>Resiver Data
                    let seconduser = data!["seconduser"] as? [String:Any]
                    let idSecond = seconduser!["id"] as? Int
                    let nameSecond = seconduser!["name"] as? String
                    let delegate = seconduser!["delegate"] as? String
                    let latSecond = seconduser!["lat"] as? Double
                    let longSecond = seconduser!["long"] as? Double
                    let rate = seconduser!["rate"] as? Double
                    let avatar = seconduser!["avatar"] as? String
                    let phone = seconduser!["phone"] as? String
                    
                    self.mobileNumber = String(describing: phone!)
                    self.userName = nameSecond!
                    self.UserRate = Double(rate!)
                    self.secondUSerImage = avatar!
                    if(is_provider == false){
                        self.issuBillBtn.tintColor = UIColor.clear
                        self.issuBillBtn.isEnabled = false
                        self.UserLat = latSecond
                        self.UserLng = longSecond
                        self.goToMap.isHidden = true
                    }else{
                        self.UserLat = lat
                        self.UserLng = long
                        self.goToMap.isHidden = false
                    }
                    
                    ////////================>>>>>>CancelResons
                    let reasons = data!["reasons"] as? [[String:Any]]
                    for i in reasons!{
                        let cancelId = i["id"] as? Int
                        let reason = i["reason"] as? String
                        self.CancelResons.append(ResonCancelModel(id: cancelId!, reason: reason!))
                    }
                    for i in self.CancelResons{
                        self.addtoVoteModel(title:i.reason , isSelected: false, isUserSelectEnable: true, id: String(describing: i.id))
                    }
                     /////////////==============>>>>>
                    self.reciever_id = String(describing: idSecond!)
                    self.SecondImage.setImageWith(avatar)
                    self.secondUserName.text = nameSecond
                    self.ratingBar.rating = rate!
                    self.SecoundUserImage = avatar!
                    self.Massges = []
                    let messages = data!["messages"] as? [[String:Any]]
                    for m in messages!{
                        let user_id = m["user_id"] as? Int
                        let username = m["username"] as? String
                        let type = m["type"] as? String ?? ""
                        let date = m["date"] as? String
                        let avatar = m["avatar"] as? String
                        let sender = m["sender"] as? String
                        let content = m["content"] as? String
                        if (type == "map"){
                            let jsonString = content
                            let data: Data? = jsonString?.data(using: .utf8)
                            let json = (try? JSONSerialization.jsonObject(with: data!, options: [.allowFragments])) as? [String:AnyObject]
                            print(json ?? "Empty Data")
                            let lat = json!["lat"]  as? Double
                            let lang = json!["lang"]  as? Double
                            self.Massges.append(MassgeModel(user_id: user_id!, username: username!, content: content!, type: type, date: date!, avatar: avatar!, sender: sender!, lat: lat!, long: lang!))
                        }else{
                            self.Massges.append(MassgeModel(user_id: user_id!, username: username!, content: content!, type: type, date: date!, avatar: avatar!, sender: sender!, lat: 0, long: 0))
                        }
                        
                    }
                    self.tableView.reloadData()
                    self.GetLastMassge()
                }else{
                    self.stopAnimating()
                    self.showCustomAlertForNetworkError()
                }
            }else{
                self.stopAnimating()
            }
        }
    }
    
    func viewDesign(){
        resonView.layer.shadowColor = UIColor.gray.cgColor
        resonView.layer.shadowOpacity = 1
        resonView.layer.shadowOffset = CGSize.zero
        resonView.layer.shadowRadius = 6
    }
    func UploadImageToSocket(){
        
        API.POSTImage(url: UploadImage_URL, Images: imagesData, Keys: imagesKeys, header: ["Authorization":"Bearer" + getTokenId(),"lang":getServerLang()], parameters: [:]) { (succss, value) in
            if succss{
                self.stopAnimating()
                print("🍉\(value)")
                let data = value["data"] as? [String:Any]
                let url = data!["url"] as? String
                let name = data!["name"] as? String
                let test = ["sender_id": getUserID(), "receiver_id": self.reciever_id!,"conversation_id":self.ConvrstionId!,"content": name,"type":"image","time_zone":getTimeZone()] as [String : Any]
                SocketConnection.sharedInstance.socket.emit("send-file", test)
                self.Massges[self.Massges.count - 1].content = url!
                self.tableView.reloadData()
                self.GetLastMassge()
                
            }else{
                self.stopAnimating()
            }
        }
    }
    
    var myItems = [Selectable]()
    
    
    func addtoVoteModel(title: String, isSelected: Bool, isUserSelectEnable: Bool,id:String){
        self.myItems.append(VoteModel(title:title , isSelected: isSelected, isUserSelectEnable: isUserSelectEnable, oilId:id ))
        print("😆\(myItems)")
        setupView()
    }
    
    func setupView(){
        
        choiseTableView.isRightToLeft = false // use it to support right to left language
        
        choiseTableView.delegate = self // the delegate used to get the selected item when pressed
        
        choiseTableView.data = myItems  // fill your item , the item may come from server or static in your code like i have done
        choiseTableView.selectionType = .single // selection type , single or multiple
        choiseTableView.cellHeight = 50 // to set cell hight
        choiseTableView.arrowImage = nil // use ot if you want to add arrow to the cell
    }
    
    func CancelOrder(order_id:String,reason_id:String){
        let parmas:Parameters = [
            "order_id":order_id,
            "reason_id":reason_id
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        API.POST(url: CancelOrder_URL, parameters: parmas, headers: header) { (succcss, value) in
            if succcss{
                self.stopAnimating()
                let key = value["key"] as? String
                if key == "success"{
                    ShowTrueMassge(massge:"Successfully Canceled".localized, title: "Done".localized)
                    self.placeHolder.isHidden = true
                    self.resonView.isHidden = true
                    self.navigationController?.popToRootViewController(animated: true)
                }else{
                    let msg = value["msg"] as? String
                    ShowErrorMassge(massge: msg!, title: "Done".localized)
                }
                                
            }else{
                self.stopAnimating()
            }
        }
    }
    
    func FinshOrder(order_id:String){
        let parmas:Parameters = [
            "order_id":order_id
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        API.POST(url: FinshlOrder_URL, parameters: parmas, headers: header) { (succcss, value) in
            if succcss{
                self.stopAnimating()
                print("🍕\(value)")
                let key = value["key"] as! String
                let msg = value["msg"] as! String
                if key == "success" {
                    ShowTrueMassge(massge: msg, title: "Done".localized)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        let vc = Storyboard.Home.viewController(RateViewController.self)
                        vc.Name = self.userName
                        vc.RateValu = self.UserRate!
                        vc.ProviderID = self.reciever_id!
                        vc.uerAvatar = self.secondUSerImage
                        vc.vc = self
                        let popupVC = PopupViewController(contentController: vc, popupWidth: UIScreen.main.bounds.width-60, popupHeight: 340)
                                    
                        popupVC.backgroundAlpha = 0.5
                        popupVC.backgroundColor = .black
                        popupVC.canTapOutsideToDismiss = false
                        popupVC.cornerRadius = 20
                        popupVC.shadowEnabled = true
                                        
                                        
                        self.present(popupVC, animated: true)
                                        
                    }
                }else{
                    ShowErrorMassge(massge: msg, title: "Error".localized)
                }
                
                
            }else{
               self.stopAnimating()
            }
        }
    }
    
    func WithdrawOrder(order_id:Int,reason_id:String){
        let parmas:Parameters = [
            "order_id":order_id,
            "reason_id":reason_id
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        API.POST(url: WithdrawOrder_URL, parameters: parmas, headers: header) { (succcss, value) in
            if succcss{
                self.stopAnimating()
                ShowTrueMassge(massge: "The withdrawal was successful".localized, title: "Done".localized)
                self.placeHolder.isHidden = true
                self.resonView.isHidden = true
                self.navigationController?.popToRootViewController(animated: true)

                 print("🍕\(value)")
            }
        }
    }
    //////////////========>>>>Record Audioooo
    
    func setupRecorder() {
        let format = DateFormatter()
        format.dateFormat="yyyy-MM-dd-HH-mm-ss"
        let currentFileName = "recording-\(format.string(from: Date())).m4a"
        print("🖍\(currentFileName)")
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        self.soundFileURL = documentsDirectory.appendingPathComponent(currentFileName)
        print("writing to soundfile url: '\(soundFileURL!)'")
        if FileManager.default.fileExists(atPath: soundFileURL.absoluteString) {
            // probably won't happen. want to do something about it?
            print("soundfile \(soundFileURL.absoluteString) exists")
        }

        let recordSettings: [String: Any] =
            
            [
                AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                AVSampleRateKey: 12000,
                AVNumberOfChannelsKey: 1,
                AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]

        do {
            recorder = try AVAudioRecorder(url: soundFileURL, settings: recordSettings)
            recorder.delegate = self as! AVAudioRecorderDelegate
            recorder.isMeteringEnabled = true
            recorder.prepareToRecord() // creates/overwrites the file at soundFileURL
        } catch {
            recorder = nil
            print(error.localizedDescription)
        }
        
    }
     @objc func startRecordAction(_ sender: UIButton) {
        self.startRecord.backgroundColor = UIColor.ErrorColor
        self.startRecord.setImage(#imageLiteral(resourceName: "microphone"), for: .normal)

        UIView.animate(withDuration: 0.6,animations: {
            self.startRecord.transform = CGAffineTransform(scaleX: 2.5, y: 2.5)
        },completion:nil)

        if player != nil && player.isPlaying {
            print("stopping")
            player.stop()
        }
        if recorder != nil && recorder.isRecording {
            print("pausing")
            recorder.pause()} else {
            print("recording")
            recordWithPermission(true)
        }
    }
    func recordWithPermission(_ setup: Bool) {
        print("\(#function)")
        
        AVAudioSession.sharedInstance().requestRecordPermission {
            [unowned self] granted in
            if granted {
                
                DispatchQueue.main.async {
                    print("Permission to record granted")
                    self.setSessionPlayAndRecord()
                    if setup {
                        self.setupRecorder()
                    }
                    
                    self.recorder.record()
                    
                }
            } else {
                print("Permission to record not granted")
            }
        }
        
        if AVAudioSession.sharedInstance().recordPermission == .denied {
            print("permission denied")
        }
    }
    @objc func stopRecord(_ sender: UIButton) {
        //        self.stopRecord.isHidden = true
        //        self.startRecord.isHidden = false
        self.startRecord.backgroundColor = UIColor.lightGray.withAlphaComponent(0.35)
        UIView.animate(withDuration: 0.6,animations: {
        self.startRecord.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        self.startRecord.setImage(#imageLiteral(resourceName: "microphone"), for: .normal)
                },completion:nil)
        recorder?.stop()
        
        player?.stop()
    }
    
    func setSessionPlayAndRecord() {
        print("\(#function)")
        
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setCategory(AVAudioSession.Category.playAndRecord)
            //try session.setCategory(AVAudioSessionCategoryPlayAndRecord, with: .defaultToSpeaker)
        } catch {
            print("could not set session category")
            print(error.localizedDescription)
        }
        
        do {
            try session.setActive(true)
        } catch {
            print("could not make session active")
            print(error.localizedDescription)
        }
    }
}



//open class SocketConnection {
//
//    open static let sharedInstance = SocketConnection()
//    let manager: SocketManager
//    public var socket: SocketIOClient
//
//    private init() {
//        manager = SocketManager(socketURL: URL(string: "http://slaty.4hoste.com:8890")!, config: [.log(true)])
//        socket = manager.defaultSocket
//    }
//}
extension ChatViewController : sendDataBackDelegate{
    func finishPassing(location: String, lat: Double, lng: Double) {
        //  self.ChatTextView.text = location
        
        self.lat = lat
        self.lng = lng
        
        self.Massges.append(MassgeModel(user_id: Int(getUserID())!, username: getUserName(), content: "", type: "map", date: "4", avatar: getUserAvatar(), sender: "you", lat: self.lat!, long: self.lng!))
        self.tableView.reloadData()
        self.GetLastMassge()
        var l = LocationMapModel(lat: lat, addresse: location, lang: lng)
        let activitesData = try? JSONSerialization.data(withJSONObject: l.toDic(), options: [])
        let activitiesDataJson = String(data: activitesData!, encoding: String.Encoding.utf8)
        
        print("💙\(activitiesDataJson!)")
        
        let test = ["sender_id": getUserID(), "receiver_id": self.reciever_id!,"conversation_id":self.ConvrstionId!,"content": activitiesDataJson! ,"type":"map","time_zone":getTimeZone()] as [String : Any]
        print("🌸🌸🌸🌸\(test)")
        SocketConnection.sharedInstance.socket.emit("sendmessage", test)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.tableView.reloadData()
            self.GetLastMassge()
        }
        
        
        self.lat = 0
        self.lng = 0
        
    }
}

extension ChatViewController: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("\(#function)")
        
        print("finished playing \(flag)")
      
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("\(#function)")
        
        if let e = error {
            print("\(e.localizedDescription)")
        }
        
    }
}
extension ChatViewController: AVAudioRecorderDelegate {
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder,
                                         successfully flag: Bool) {
       
        
        if let multiPart = try? Data(contentsOf: self.recorder.url){
            print("💙\(multiPart)")
            API.POSTSound(url: UploadImage_URL, sound: multiPart , Keys: "file", header: ["Authorization":"Bearer" + getTokenId(),"lang":getServerLang()], parameters: [:]) { (succss, value) in
                print("👏🏻\(value)")
                if value["data"] as? [String:Any] != nil {
                    let data = value["data"] as? [String:Any]
                    let url = data!["url"] as? String
                    let name = data!["name"] as? String
                   // self.Massges[self.Massges.count - 1].content = url!
                    print("💙\(url)")
                    self.Massges.append(MassgeModel(user_id: Int(getUserID())!, username: getUserName(), content: url!, type: "sound", date: "", avatar: getUserAvatar(), sender: "you", lat: 0.0, long: 0.0))
                           self.tableView.reloadData()
                    let test = ["sender_id": getUserID(), "receiver_id": self.reciever_id!,"conversation_id":self.ConvrstionId!,"content": name!,"type":"sound","time_zone":getTimeZone()] as [String : Any]
                    SocketConnection.sharedInstance.socket.emit("send-file", test)
                    self.tableView.reloadData()
                    self.GetLastMassge()
                    self.recorder = nil
                }
                
            }
        }
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder,
                                          error: Error?) {
        print("\(#function)")
        
        if let e = error {
            print("\(e.localizedDescription)")
        }
    }
    
}
extension AVPlayer {
    
    var isPlaying: Bool {
        if (self.rate != 0 && self.error == nil) {
            return true
        } else {
            return false
        }
    }
    
}



