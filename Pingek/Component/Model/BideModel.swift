
//
//  BideModel.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/28/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import Foundation


class BideModel{
    var id = ""
    var username = ""
    var avatar = ""
    var rating = ""
    var place_name = ""
    var place_icon = ""
    var deliver_time = ""
    var receive_lat = ""
    var receive_long = ""
    var receive_address = ""
    var deliver_lat = ""
    var deliver_long = ""
    var deliver_address = ""
    var price = ""
    var min_expected_price = ""
    var max_expected_price = ""
    var expected_price_msg = ""
    var details = [wattingOrdersDetails]()
    var status = ""
    var conversation_id = ""
    var have_invoice = ""
    var distance_to_store = ""
    var distance_to_client = ""
    var date = ""
    var bid = BideDetailsModel()

    func getObject(dicc: [String: Any]) ->  BideModel {
        let dic = HandleJSON.getObject().handle(dicc: dicc)
        let model =  BideModel()
        model.id = dic["id"] as! String
        model.username = dic["username"] as! String
        model.avatar = dic["avatar"] as! String
        model.rating = dic["rating"] as! String
        model.place_name = dic["place_name"] as! String
        model.place_icon = dic["place_icon"] as! String
        model.deliver_time = dic["deliver_time"] as! String
        model.receive_lat = dic["receive_lat"] as! String
        model.receive_long = dic["receive_long"] as! String
        model.receive_address = dic["receive_address"] as! String
        model.deliver_lat = dic["deliver_lat"] as! String
        model.deliver_long = dic["deliver_long"] as! String
        model.deliver_address = dic["deliver_address"] as! String
        model.price = dic["price"] as! String
        model.min_expected_price = dic["min_expected_price"] as! String
        model.max_expected_price = dic["max_expected_price"] as! String
        model.expected_price_msg = dic["expected_price_msg"] as! String
        for item in dic["details"] as! [[String: Any]] {
            model.details.append(wattingOrdersDetails().getObject(dicc: item))
        }
        model.status = dic["status"] as! String
        model.conversation_id = dic["conversation_id"] as! String
        model.have_invoice = dic["have_invoice"] as! String
        model.distance_to_store = dic["distance_to_store"] as! String
        model.distance_to_client = dic["distance_to_client"] as! String
        model.date = dic["date"] as! String
        model.bid = BideDetailsModel().getObject(dicc: dic["bid"] as! [String: Any])
        

        return model
              
    }
}



class BideDetailsModel{
    var id = ""
    var provider_name = ""
    var num_rating = ""
    var rating = ""
    var avatar = ""
    var price = ""
    var deliver_time = ""
    var best_offer = ""
    var currency = ""
    var num_bids = ""
    var distance_provider_to_store = ""
    var distance_provider_to_client = ""

        func getObject(dicc: [String: Any]) ->  BideDetailsModel {
        let dic = HandleJSON.getObject().handle(dicc: dicc)
        let model =  BideDetailsModel()
            model.id = dic["id"] as! String
            model.provider_name = dic["provider_name"] as! String
            model.num_rating = dic["num_rating"] as! String
            model.rating = dic["rating"] as! String
            model.avatar = dic["avatar"] as! String
            model.price = dic["price"] as! String
            model.num_bids = dic["num_bids"] as! String
            model.deliver_time = dic["deliver_time"] as! String
            if dic["best_offer"] != nil {
                model.best_offer = dic["best_offer"] as! String
            }
            model.currency = dic["currency"] as! String
            model.distance_provider_to_store = dic["distance_provider_to_store"] as! String
            model.distance_provider_to_client = dic["distance_provider_to_client"] as! String
        return model
              
    }
}

