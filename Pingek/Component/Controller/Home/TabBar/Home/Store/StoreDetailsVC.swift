//
//  StoreDetailsVC.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/16/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import Cosmos
import EzPopup
import GoogleMaps


class StoreDetailsVC: UIViewController , GMSMapViewDelegate{

    @IBOutlet weak var specialStoreInformation: UIScrollView!
    @IBOutlet weak var storeName: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var storeImage: UIImageView!
    @IBOutlet weak var storeInformationButton: UIButton!
    @IBOutlet weak var watingOrdersButton: UIButton!
    @IBOutlet weak var selectedViewLeading: NSLayoutConstraint!
    
    @IBOutlet weak var storeCoverImage: UIImageView!
    @IBOutlet weak var noOfSharing: UILabel!
    @IBOutlet weak var ratesView: CosmosView!
    @IBOutlet weak var sharingCount: UILabel!
    @IBOutlet weak var noOfRates: UILabel!
    @IBOutlet weak var storeLocation: UILabel!
    @IBOutlet weak var storeDistance: UILabel!
    @IBOutlet weak var storeOpeningTimes: UILabel!
    @IBOutlet weak var storeStatus: UILabel!
    @IBOutlet weak var menuCollection: UICollectionView!
    
    @IBOutlet var arrowImages: [UIImageView]!
    
    @IBOutlet weak var segmantedView: UIView!
    
    @IBOutlet weak var CommentsView: UIView!
    @IBOutlet weak var branchesView: UIView!
    @IBOutlet weak var timesView: UIView!
    
    @IBOutlet weak var semanticHeight: NSLayoutConstraint!
    
    //===============google store==================
    
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var googleStoreViewInformation: UIView!
    @IBOutlet weak var ContinerView: UIView!
    @IBOutlet weak var TableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var dropDown: UIButton!
    @IBOutlet weak var ContinerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var LineView: UIView!
    @IBOutlet weak var daystableView: UITableView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noRequstLbl: UILabel!
    @IBOutlet weak var storeNameLbl: UILabel!
    @IBOutlet weak var storeAddress: UILabel!
    
    var OrdersDataSourse = [wattingOrders]()
    var OpnningHoue = [String]()
    var branches = [BranchesModel]()




    var isGoogleStore = false
    var store = NearStoreModel()
    var notificaion : NotficationsModel?
    var storeDetails = StoreDetailsModel()
    var place_website = ""
    var place_name = ""
    var place_address = ""

    //==========location
     var UserLat = ""
     var UserLong = ""
     let userMarker = GMSMarker()
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController!.view.backgroundColor = UIColor.clear
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(),for: .default)
        self.tabBarController?.tabBar.isHidden = true
        self.noRequstLbl.isHidden = true
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupview()
        if getisDelgate() == "yes"{
            segmantedView.isHidden = false
            semanticHeight.constant = 45
        }else{
            segmantedView.isHidden = true
            semanticHeight.constant = 0

        }
        
        if self.notificaion == nil {
            if !isGoogleStore {
                getStoreDetailst()
                self.specialStoreInformation.isHidden = false
                self.googleStoreViewInformation.isHidden = true

            }else{
                self.specialStoreInformation.isHidden = true
                self.googleStoreViewInformation.isHidden = false
                if(getoIsVistor() == "yes"){
                    getNearstProviders(RestruntRef: self.store.reference, RestruntID: self.store.place_id, user_id: nil, tokenId: "")
                }else{
                    getNearstProviders(RestruntRef: self.store.reference, RestruntID: self.store.place_id, user_id: getUserID(), tokenId: getTokenId())
                }
            }
            
        }else{
            if self.notificaion?.place_ref == "" {
                getStoreDetailst()
            }else{
                if(getoIsVistor() == "yes"){
                    getNearstProviders(RestruntRef: self.notificaion!.place_ref, RestruntID: self.notificaion!.place_id, user_id: nil, tokenId: "")
                }else{
                    getNearstProviders(RestruntRef: self.notificaion!.place_ref, RestruntID: self.store.place_id, user_id: self.notificaion!.place_id, tokenId: getTokenId())
                }
            }
            
        }
    }
    
    func setupview(){
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)
        for i in self.arrowImages{
            i.image = (AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_poin_left") : #imageLiteral(resourceName: "arrow_poin_right")  )
        }
        UserLat = getUserLat()
        UserLong = getUserLong()
        self.storeName.text = store.name
        self.storeImage.setImageWith(store.icon)
        self.ContinerViewHeight.constant = 95.0
        self.TableViewHeight.constant = 0
        self.LineView.isHidden = true
        self.tableView.isHidden = true
        self.noRequstLbl.isHidden = true
        tableView.register(UINib.init(nibName: "PaddingRequstTableViewCell", bundle: nil), forCellReuseIdentifier: "PaddingRequstTableViewCell")

        
        //======================mapViewInit
        mapView.delegate = self
        userMarker.appearAnimation = .pop
        
        let camera = GMSCameraPosition.camera(withLatitude:  Double(getUserLat())!, longitude: Double(getUserLong())!, zoom: 15)
        self.mapView.camera = camera

       // marker.position = CLLocationCoordinate2D(latitude: self.lat!, longitude: self.lon!)
               //=======add user annotaion
        userMarker.position = CLLocationCoordinate2D(latitude: Double(getUserLat())!, longitude: Double(getUserLong())!)
        userMarker.icon = #imageLiteral(resourceName: "vectorperson")
        userMarker.map = mapView
        
        
        if notificaion != nil {
            
            UIView.animate(withDuration: 0.3) {
                self.watingOrdersButton.setTitleColor(UIColor.white, for: .normal)
                self.storeInformationButton.setTitleColor(UIColor.darkGray, for: .normal)
                self.selectedViewLeading.constant = 50 + self.storeInformationButton.frame.width
                self.view.updateConstraints()
                self.view.layoutIfNeeded()
            }
            self.tableView.isHidden = false
            self.specialStoreInformation.isHidden = true
            self.googleStoreViewInformation.isHidden = true
            getWattingOrders()
            
        }
        
        
        //=================tabRecognizer
        CommentsView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openComments(_:))))
        branchesView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openBranches)))
        timesView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openTimes)))
        ContinerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(someAction)))
    }
    
    @objc func openComments(_ sender: UITapGestureRecognizer){
        let vc = Storyboard.Home.viewController(StoreCommentsVC.self)
        vc.storeID = store.place_id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func openBranches(_ sender: UITapGestureRecognizer){
        if self.branches.count != 0 {
           let vc = Storyboard.Home.viewController(StoreLocationsVC.self)
            vc.delegate = self
            vc.branches = self.branches
            vc.store = self.storeDetails
           self.navigationController?.pushViewController(vc, animated: true)
        }else{
            ShowErrorMassge(massge: "this store dont have anothers branchs".localized, title: "Error".localized)
        }
    }
    @objc func openTimes(_ sender: UITapGestureRecognizer){
       let vc = Storyboard.Home.viewController(StoreTimesVC.self)
             vc.times = self.storeDetails.opening_hours
             
             let popupVC = PopupViewController(contentController: vc, popupWidth: UIScreen.main.bounds.width-60, popupHeight: 400)
             
             popupVC.backgroundAlpha = 0.5
             popupVC.backgroundColor = .black
             popupVC.canTapOutsideToDismiss = true
             popupVC.cornerRadius = 20
             popupVC.shadowEnabled = true
             
             
             self.present(popupVC, animated: true)
    }
    

    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shareStore(_ sender: Any) {
        var firstActivityItem  = ""
        var secondActivityItem  = ""
        if !isGoogleStore {
            firstActivityItem  = self.storeDetails.name
            secondActivityItem  = self.storeDetails.address
        }else{
            firstActivityItem  = self.place_name
            secondActivityItem  = self.place_address
        }
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [firstActivityItem,secondActivityItem], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = (sender as! UIButton)
        self.present(activityViewController, animated: true, completion: nil)
        
        
    }
    @IBAction func showTimes(_ sender: Any) {
        let vc = Storyboard.Home.viewController(StoreTimesVC.self)
        vc.times = self.storeDetails.opening_hours
        
        let popupVC = PopupViewController(contentController: vc, popupWidth: UIScreen.main.bounds.width-60, popupHeight: 400)
        
        popupVC.backgroundAlpha = 0.5
        popupVC.backgroundColor = .black
        popupVC.canTapOutsideToDismiss = true
        popupVC.cornerRadius = 10
        popupVC.shadowEnabled = true
        
        
        self.present(popupVC, animated: true)
    }
    @IBAction func changeStoreBranch(_ sender: Any) {
        if self.branches.count != 0 {
            let vc = Storyboard.Home.viewController(StoreLocationsVC.self)
            vc.delegate = self
            vc.branches = self.branches
            vc.store = self.storeDetails
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            ShowErrorMassge(massge: "this store dont have anothers branchs".localized, title: "Error".localized)
        }
    }
    
    @IBAction func storeInformation(_ sender: Any) {
        UIView.animate(withDuration: 0.3) {
            self.watingOrdersButton.setTitleColor(UIColor.darkGray, for: .normal)
            self.storeInformationButton.setTitleColor(UIColor.white, for: .normal)
            self.selectedViewLeading.constant = 5
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
        }
        self.tableView.isHidden = true
        if self.notificaion == nil {
            if !isGoogleStore {
                self.specialStoreInformation.isHidden = false
                self.googleStoreViewInformation.isHidden = true

            }else{
                self.specialStoreInformation.isHidden = true
                self.googleStoreViewInformation.isHidden = false
            }
            self.noRequstLbl.isHidden = true
        }else{
            if self.notificaion?.place_ref == "" {
                self.specialStoreInformation.isHidden = false
                self.googleStoreViewInformation.isHidden = true
            }else{
                self.specialStoreInformation.isHidden = true
                self.googleStoreViewInformation.isHidden = false
            }
        }

    }
    
    @IBAction func watingOrders(_ sender: Any) {
        UIView.animate(withDuration: 0.3) {
            self.watingOrdersButton.setTitleColor(UIColor.white, for: .normal)
            self.storeInformationButton.setTitleColor(UIColor.darkGray, for: .normal)
            self.selectedViewLeading.constant = 5 + self.storeInformationButton.frame.width
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
        }
        self.tableView.isHidden = false
        self.specialStoreInformation.isHidden = true
        self.googleStoreViewInformation.isHidden = true
        getWattingOrders()

    }
    
    
    @objc func someAction(_ sender:UITapGestureRecognizer){
        UIView.animate(withDuration: 0.3) {
           if(self.ContinerViewHeight.constant == 95.0){
               self.ContinerViewHeight.constant = 260.0
               self.TableViewHeight.constant = 140
               self.dropDown.setImage(#imageLiteral(resourceName: "rightarrow"), for: .normal)
               self.LineView.isHidden = false
               
           }else{
               self.ContinerViewHeight.constant = 95.0
               self.TableViewHeight.constant = 0
               self.dropDown.setImage(#imageLiteral(resourceName: "down_arrow"), for: .normal)
               self.LineView.isHidden = true
               
           }
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
        }
       }
    
    //=================google store
    
    @IBAction func addOrder(_ sender: Any) {
        if(getoIsVistor() == "yes"){
            ShowErrorMassge(massge: "You must Login first".localized, title: "Error".localized)
            let Reg = Storyboard.Authentication.viewController(LoginViewController.self)
            self.navigationController?.pushViewController(Reg, animated: true)
        }else{
           let vc = Storyboard.Home.viewController(NewOrderViewController.self)
            vc.store = self.store
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    
    
    //================refreshController
    lazy var refreshControl: UIRefreshControl = {
           let refreshControl = UIRefreshControl()
           refreshControl.addTarget(self, action:
               #selector(handleRefresh(_:)),
                                    for: UIControl.Event.valueChanged)
           refreshControl.tintColor = UIColor.BasicColor
           
           return refreshControl
       }()
       @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
           self.tableView.animateTable()
           refreshControl.endRefreshing()
       }

    func addMarkerToMap(lat: Double,lng: Double,image : UIImage,title : String?){
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        if title != nil{
            marker.title = title! + "KM".localized
        }
        
        marker.appearAnimation = .pop
        marker.icon = image
        
        marker.map = mapView
        
        self.mapView.selectedMarker = marker
    }
          
}

