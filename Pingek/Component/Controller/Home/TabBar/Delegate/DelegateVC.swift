//
//  DelegateVC.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/16/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import Alamofire

class DelegateVC: UIViewController {
    
    @IBOutlet weak var delegatesTable: UITableView!
    @IBOutlet weak var noRequstLbl: UILabel!

    
    var OrdersDataSourse = [wattingOrders]()
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        if getisDelgate() == "yes"{
             getnearOrders()
        }else{
            ShowErrorMassge(massge: "You must improve your account to delegate".localized, title: "Error".localized)
            self.tabBarController?.selectedIndex = 0
            DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                guard let url = URL(string:"http://Pingek.4hoste.com/delegation") else { return }
                UIApplication.shared.open(url)
            }
            
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.noRequstLbl.isHidden = true
        delegatesTable.tableFooterView = UIView()
        delegatesTable.register(UINib.init(nibName: "PaddingRequstTableViewCell", bundle: nil), forCellReuseIdentifier: "PaddingRequstTableViewCell")
        delegatesTable.animateTable()
        
        // Do any additional setup after loading the view.
    }
    
    func getnearOrders(){
        self.showProgress()
        self.OrdersDataSourse.removeAll()
        let parmas :Parameters = [
            "lat":  getUserLat(),
            "long": getUserLong()
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        API.POST(url: nearWaitingOrders_URL, parameters: parmas, headers: header) { (succss, value) in if succss{
            self.stopAnimating()
            let key = value["key"] as? String
            print("🌸\(value)")
            if(key == "success"){
                let data = value["data"] as! [[String:Any]]
                self.OrdersDataSourse = []
                if(data.count == 0){
                    self.noRequstLbl.isHidden = false
                }else{
                    self.noRequstLbl.isHidden = true
                    for i in data{
                        self.OrdersDataSourse.append(wattingOrders().getObject(dicc: i))

                    }
                    self.delegatesTable.animateTable()
                }
            }
                    
        }else{
            self.stopAnimating()
            ShowErrorMassge(massge: "Something went wrong and tried again".localized, title: "Error".localized)

            }
        }
    }
    
}
extension DelegateVC :UITableViewDataSource ,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.OrdersDataSourse.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "PaddingRequstTableViewCell", for: indexPath) as! PaddingRequstTableViewCell
        let item = self.OrdersDataSourse[indexPath.section]
        cell1.configureCell(order: item)
        return cell1
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let vc = Storyboard.Home.viewController(OrderDetilsViewController.self)
        vc.orderId = self.OrdersDataSourse[indexPath.section].id
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
          return 100
    
       }
       
}

