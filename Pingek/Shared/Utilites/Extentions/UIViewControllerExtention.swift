//
//  UIViewControllerExtention.swift
//  Noon
//
//  Created by Engy Bakr on 12/25/19.
//  Copyright © 2019 Engy Bakr. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
//import MIAlertController
import SwiftEntryKit
import Kingfisher


extension UIViewController {
    
    func textfeildError(txt : UITextField){
        if txt.text?.count != 0 {
            txt.layer.borderColor = UIColor.lightGray.cgColor
            
        }else{
            txt.layer.borderColor = UIColor.red.cgColor
        }
    }
    
    //================keyboard
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
    }
    
    @objc func dismissKeyboard() {
        //KeyboardAvoiding.avoidingView = view
        view.endEditing(true)
    }
    
    
    // present
    func presentDetails(viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        self.view.window?.layer.add(transition, forKey: kCATransition)
        
        present(viewControllerToPresent, animated: false, completion: nil)
    }
    
    func convertToBase64(image : UIImage) -> String {
         
         let data : Data =  image.jpegData(compressionQuality: 0)!
         return data.base64EncodedString(options: .lineLength64Characters)
         
     }
    
//    func downloadImg(imgStr: String , image : UIImageView){
//        image.kf.setImage(with: URL.init(string: imgStr),placeholder:nil,options:nil, progressBlock:nil ,completionHandler:nil )
//    }
    
//
    
    func openSideMenu(){
        
        if AppLanguage.currentLanguage().contains("en"){


//            let sideMenuVc = Storyboard.Home.viewController(SideMenuVC.self)
//            let menuNavgtion = SideMenuNavigationController(rootViewController: sideMenuVc)
//            SideMenuManager.default.addPanGestureToPresent(toView: (self.navigationController?.navigationBar)!)
//            SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView:(self.navigationController?.navigationBar)! )
//            menuNavgtion.statusBarEndAlpha = 0
//            menuNavgtion.menuWidth = 320
//            menuNavgtion.presentationStyle = .menuSlideIn
//            menuNavgtion.presentationStyle.onTopShadowOpacity = 1
//            SideMenuManager.default.leftMenuNavigationController = menuNavgtion
//            present(SideMenuManager.default.leftMenuNavigationController!, animated: true, completion: nil)

        }else{


//            let sideMenuVc = Storyboard.Home.viewController(SideMenuVC.self)
//            let menuNavgtion = SideMenuNavigationController(rootViewController: sideMenuVc)
//            SideMenuManager.default.addPanGestureToPresent(toView: (self.navigationController?.navigationBar)!)
//        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView:(self.navigationController?.navigationBar)! )
//            menuNavgtion.statusBarEndAlpha = 0
//            menuNavgtion.menuWidth = 320
//            menuNavgtion.presentationStyle = .menuSlideIn
//            menuNavgtion.presentationStyle.onTopShadowOpacity = 1
//            SideMenuManager.default.rightMenuNavigationController = menuNavgtion
//            present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
        }
    }
    
    // dismiss
    func dismissDetails() {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        self.view.window?.layer.add(transition, forKey: kCATransition)
        
        dismiss(animated: false, completion: nil)
    }
    
    
    public static var defaultNib: String {
        return self.description().components(separatedBy: ".").dropFirst().joined(separator: ".")
    }
    
    public static var storyboardIdentifier: String {
        return self.description().components(separatedBy: ".").dropFirst().joined(separator: ".")
    }
    
}

extension UIViewController : NVActivityIndicatorViewable{
    func showProgress(){
        NVActivityIndicatorView.DEFAULT_COLOR = UIColor.BasicColor
        NVActivityIndicatorView.DEFAULT_TYPE = NVActivityIndicatorType.ballRotateChase
        self.startAnimating()

    }
    
    func hideProgress(){
        self.stopAnimating()
        
    }
}


//===================alerts

extension UIViewController {
    
    func showCustomAlertForNetworkError(){

           var attributes = EKAttributes.centerFloat
        attributes.entryBackground = .gradient(gradient: .init(colors: [EKColor(UIColor.BasicColor), EKColor(UIColor.lightGray)], startPoint: .zero, endPoint: CGPoint(x: 1, y: 1)))
           attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 1), scale: .init(from: 1, to: 0.7, duration: 0.7)))
           attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 10, offset: .zero))
           attributes.statusBar = .dark
           attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
           attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.bounds.width - 40), height: .intrinsic)

           var title = EKProperty.LabelContent(text: "No internet connection".localized, style: .init(font: UIFont(name: "JFFlat-Medium", size: 16)!, color: .white))
           var description = EKProperty.LabelContent(text: "you seem to be ofline, open WLAN or Use data".localized, style: .init(font: UIFont(name: "JFFlat-Medium", size: 14)!, color: .white))
           title.style.alignment = .center
           description.style.alignment = .center
           let simpleMessage = EKSimpleMessage(title: title, description: description)
           let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)

           let contentView = EKNotificationMessageView(with: notificationMessage)
           SwiftEntryKit.display(entry: contentView, using: attributes)
       }
       
       //showAlert
       func showAlert(title: String, message:String, okAction: String , completion: ((UIAlertAction) -> Void)? = nil ) {
           
           let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
           
           alert.addAction(UIAlertAction(title: okAction, style: .default, handler: completion))
           
           present(alert, animated: true, completion: nil)

           
           
       }
       
       //alert with cancel
       func showAlertWithCancel(title: String, message:String, okAction: String = "ok", completion: ((UIAlertAction) -> Void)? = nil ) {
           
           let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
           
           alert.addAction(UIAlertAction(title: okAction, style: .default, handler: completion))
            alert.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel))
                         present(alert, animated: true, completion: nil)
      
       }
    
    func viewSuccessMsgBanner(title: String, body: String){
            
            var titleLbl = ""
            if title == "" {
                 titleLbl = "Success".localized
            }else{
                titleLbl = title
            }
            
            var attributes = EKAttributes.topFloat
            attributes.entryBackground = .gradient(gradient: .init(colors: [EKColor(UIColor.SuccessColor), EKColor(UIColor.SecondaryColor)], startPoint: .zero, endPoint: CGPoint(x: 1, y: 1)))
            attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3), scale: .init(from: 1, to: 0.7, duration: 0.7)))
            attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 10, offset: .zero))
            attributes.statusBar = .dark
            attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
            attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.bounds.width - 20), height: .intrinsic)
            
            var title = EKProperty.LabelContent(text: "  " + titleLbl, style: .init(font: UIFont(name: "JFFlat-Medium", size: 17)!, color: EKColor(UIColor.white)))
            var description = EKProperty.LabelContent(text:  "  " + body, style: .init(font: UIFont(name: "JFFlat-Medium", size: 15)!, color: EKColor(UIColor.white)))
            title.style.alignment = .right
            description.style.alignment = .right
            let simpleMessage = EKSimpleMessage(title: title, description: description)
            let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)
            
            let contentView = EKNotificationMessageView(with: notificationMessage)
            SwiftEntryKit.display(entry: contentView, using: attributes)
        }
        
        func viewErrorMsgBanner(title: String, body: String){
            var titleLbl = ""
            if title == "" {
                titleLbl = "Fail".localized
            }else{
                titleLbl = title
            }
            
            var attributes = EKAttributes.topFloat
            attributes.entryBackground = .gradient(gradient: .init(colors: [EKColor(UIColor.ErrorColor), EKColor(UIColor.lightGray)], startPoint: .zero, endPoint: CGPoint(x: 1, y: 1)))
            attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3), scale: .init(from: 1, to: 0.7, duration: 0.7)))
            attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 10, offset: .zero))
            attributes.statusBar = .dark
            attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
            attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.bounds.width - 20), height: .intrinsic)
            
            var title = EKProperty.LabelContent(text:  "  " + titleLbl, style: .init(font: UIFont(name: "Cairo-SemiBold", size: 17)!, color: EKColor(UIColor.white)))
            var description = EKProperty.LabelContent(text:  "  " + body, style: .init(font: UIFont(name: "Cairo-SemiBold", size: 15)!, color: EKColor(UIColor.white)))
            title.style.alignment = .right
            description.style.alignment = .right
            let simpleMessage = EKSimpleMessage(title: title, description: description)
            let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)
            
            let contentView = EKNotificationMessageView(with: notificationMessage)
            SwiftEntryKit.display(entry: contentView, using: attributes)
        }
        func viewWarnningMsgBanner(title: String, body: String){
            var titleLbl = ""
            if title == "" {
                titleLbl = "Warning".localized
            }else{
                titleLbl = title
            }
            
            var attributes = EKAttributes.topFloat
            attributes.entryBackground = .gradient(gradient: .init(colors: [EKColor(UIColor.orange), EKColor(UIColor.lightGray)], startPoint: .zero, endPoint: CGPoint(x: 1, y: 1)))
            attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3), scale: .init(from: 1, to: 0.7, duration: 0.7)))
            attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 10, offset: .zero))
            attributes.statusBar = .dark
            attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
            attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.bounds.width - 20), height: .intrinsic)
            var title = EKProperty.LabelContent(text:  "  " + titleLbl, style: .init(font: UIFont(name: "Cairo-SemiBold", size: 17)!, color: EKColor(UIColor.white)))
            var description = EKProperty.LabelContent(text:  "  " + body, style: .init(font: UIFont(name: "Cairo-SemiBold", size: 15)!, color: EKColor(UIColor.white)))
            title.style.alignment = .right
            description.style.alignment = .right
            
            let simpleMessage = EKSimpleMessage(title: title, description: description)
            let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)
            
            let contentView = EKNotificationMessageView(with: notificationMessage)
            SwiftEntryKit.display(entry: contentView, using: attributes)
        }
        func viewWarnningVisitorMsgBanner(title: String, body: String){
            var titleLbl = ""
            if title == "" {
                titleLbl = "Warning".localized
            }else{
                titleLbl = title
            }
            
            var attributes = EKAttributes.topFloat
            attributes.entryBackground = .gradient(gradient: .init(colors: [EKColor(UIColor.orange), EKColor(UIColor.lightGray)], startPoint: .zero, endPoint: CGPoint(x: 1, y: 1)))
            attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3), scale: .init(from: 1, to: 0.7, duration: 0.7)))
            attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 10, offset: .zero))
            attributes.statusBar = .dark
            attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
            attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.bounds.width - 20), height: .intrinsic)
    //        let action = {
    //            UserDefaults.standard.removeObject(forKey: "user_data")
    //            UserDefaults.standard.removeObject(forKey: "user_id")
    //            let vc =  Storyboard.Authentication.instantiate(LoginViewController.self)
    //            self.navigationController?.pushViewController(vc, animated: false)
    //        }
          //  attributes.entryInteraction.customTapActions.append(action)
            
            var title = EKProperty.LabelContent(text:  "  " + titleLbl, style: .init(font: UIFont(name: "Cairo-SemiBold", size: 17)!, color: .white))
            var description = EKProperty.LabelContent(text: "  " +  body, style: .init(font: UIFont(name: "Cairo-SemiBold", size: 15)!, color: .white))
            title.style.alignment = .right
            description.style.alignment = .right
            
            let simpleMessage = EKSimpleMessage(title: title, description: description)
            let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)
            
            let contentView = EKNotificationMessageView(with: notificationMessage)
            SwiftEntryKit.display(entry: contentView, using: attributes)
        }
    
        open override func awakeFromNib() {
           super.awakeFromNib()
           if Language.currentLanguage().contains("ar") {
               navigationController?.view.semanticContentAttribute = .forceRightToLeft
               navigationController?.navigationBar.semanticContentAttribute = .forceRightToLeft
           }
           
       }
}
