//
//  SocketConnection.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/21/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import Foundation
import SocketIO


open class SocketConnection {
    
    public static let sharedInstance = SocketConnection()
    let manager: SocketManager
    public var socket: SocketIOClient
    
    private init() {
        manager = SocketManager(socketURL: URL(string: "http://Pingek.4hoste.com:8907")!, config: [.log(true)])
        socket = manager.defaultSocket
    }
}
