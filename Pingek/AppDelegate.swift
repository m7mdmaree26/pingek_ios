//
//  AppDelegate.swift
//  Pingek
//
//  Created by Engy Bakr on 2/9/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import IQKeyboardManagerSwift
import Firebase
import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging
import NotificationBannerSwift
import SwiftyJSON
import SwiftEntryKit
import AVKit
import AVFoundation
import EzPopup

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    static var FCMTOKEN = "1234567"


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        IQKeyboardManager.shared.enable = true
        
        let navigationBarAppearance = UINavigationBar.appearance()
        navigationBarAppearance.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        navigationBarAppearance.isTranslucent = true
        navigationBarAppearance.shadowImage = UIImage()
        navigationBarAppearance.setBackgroundImage(UIImage(), for: .default)
        
        
        
        if(UserDefaults.standard.string(forKey: is_LoogedIn) == nil || UserDefaults.standard.string(forKey: is_LoogedIn) == "no" ){
            window = UIWindow(frame: UIScreen.main.bounds)
            let vc = UIStoryboard.instantiateInitialViewController(.Authentication)
            self.window?.rootViewController = vc
            window?.makeKeyAndVisible()
        }else{
          let vc = UIStoryboard.instantiateInitialViewController(.Home)
            self.window?.rootViewController = vc
            window?.makeKeyAndVisible()
        }
        FirebaseApp.configure()
            Messaging.messaging().delegate = self
            Messaging.messaging().shouldEstablishDirectChannel = true
            UNUserNotificationCenter.current().delegate = self
            initNotificationSetupCheck()
            return true
        }

        func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
                    // If you are receiving a notification message while your app is in the background,
                    // this callback will not be fired till the user taps on the notification launching the application.
                    // TODO: Handle data of notification
                    // With swizzling disabled you must let Messaging know about the message, for Analytics
            Messaging.messaging().appDidReceiveMessage(userInfo)
                    // Print message ID.
            if let messageID = userInfo[gcmMessageIDKey] {
                print("Message ID: \(messageID)")
            }
                    // Print full message.
            print(userInfo)
        }
        func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
                    // If you are receiving a notification message while your app is in the background,
                    // this callback will not be fired till the user taps on the notification launching the application.
                    // TODO: Handle data of notification
                    // With swizzling disabled you must let Messaging know about the message, for Analytics
                    // Messaging.messaging().appDidReceiveMessage(userInfo)
                    // Print message ID.
                if let messageID = userInfo[gcmMessageIDKey] {
                    print("Message ID: \(messageID)")
                }
                    
                    // Print full message.
                print(userInfo)
                    
                completionHandler(UIBackgroundFetchResult.newData)
            }
                
            func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
                print("Unable to register for remote notifications: \(error.localizedDescription)")
                    //   hasAllowedNotification = false
            }
            func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
                print("APNs token retrieved: \(deviceToken)")
                    //   hasAllowedNotification = true
                    /*
                     var token = ""
                     for i in 0..<deviceToken.count {
                     token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
                     }
                     print("Registration succeeded! Token: ", token)
                     
                     if let refreshedToken = InstanceID.instanceID().token() {
                     print("InstanceID token: \(refreshedToken)")
                     }
                     */
                    
                    // With swizzling disabled you must set the APNs token here.
                    // Messaging.messaging().apnsToken = deviceToken
            }
            
        }

        extension UIApplication {
            var statusBarView: UIView? {
                if responds(to: Selector("statusBar")) {
                    return value(forKey: "statusBar") as? UIView
                }
                return nil
            }
        }
            // [START ios_10_message_handling]
        @available(iOS 10, *)
        extension AppDelegate : UNUserNotificationCenterDelegate {
                
            func registerForLocalNotification(on application:UIApplication) {
                    
            }
                
            func dispatchlocalNotification(with title: String, body: String, userInfo: [AnyHashable: Any]? = nil, at date:Date) {
                    
            }
                
                // Receive displayed notifications for iOS 10 devices.
            func userNotificationCenter(_ center: UNUserNotificationCenter,
                                            willPresent notification: UNNotification,
                                            withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
                    
        //            let pushContent = notification.request.content
        //            let badgeCount = pushContent.badge as? Int
        //            let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
        //            tabBar!.tabBar.items![3].badgeValue = String(describing: badgeCount)
                NotificationCenter.default.post(name: .FireNotfication, object:nil)

                    
                let userInfo = notification.request.content.userInfo
                let title = JSON(userInfo[AnyHashable("title")]!).string
                let message_ar = JSON(userInfo[AnyHashable("message_ar")]!).string
                let message_en = JSON(userInfo[AnyHashable("message_en")]!).string
                    var massgae = ""
                if(getServerLang() == "ar"){
                    massgae = message_ar!
                }else{
                    massgae = message_en!
                }
                if let messageID = userInfo[gcmMessageIDKey] {
                    print("Message ID: \(messageID)")
                }
                print("⛩\(JSON(userInfo[AnyHashable("key")]!).string!)")
                print("from foreground \(userInfo)")
                    
                    //========================show alert
                    let systemSoundID: SystemSoundID = 1007
                    AudioServicesPlaySystemSound(systemSoundID)
                    var attributes = EKAttributes.topFloat
                    attributes.entryBackground = .gradient(gradient: .init(colors: [EKColor(UIColorFromRGB(rgbValue: 0xF3F3F3)), EKColor(UIColorFromRGB(rgbValue: 0xF3F3F3))], startPoint: .zero, endPoint: CGPoint(x: 1, y: 1)))
                    attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3), scale: .init(from: 1, to: 0.7, duration: 0.7)))
                    attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 10, offset: .zero))
                    attributes.statusBar = .dark
                    attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
                    attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.bounds.width - 20), height: .intrinsic)
                    var titleLbl = EKProperty.LabelContent(text:  "  " + title!, style: .init(font: UIFont(name: "JFFlat-Medium", size: 17)!, color: EKColor(UIColor.BasicColor)))
                    var descriptionLbl = EKProperty.LabelContent(text: "  " +  massgae, style: .init(font: UIFont(name: "JFFlat-Medium", size: 15)!, color: .black))
                    let image = EKProperty.ImageContent(image: UIImage(named: "logo")!, size: CGSize(width: 35, height: 35))
                    if AppLanguage.getLang().contains("en"){
                        titleLbl.style.alignment = .left
                        descriptionLbl.style.alignment = .left
                    }else{
                        titleLbl.style.alignment = .right
                        descriptionLbl.style.alignment = .right
                    }
                            
                    let simpleMessage = EKSimpleMessage(image: image,title: titleLbl, description: descriptionLbl)
                    let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)
                            
                    let contentView = EKNotificationMessageView(with: notificationMessage)
                    let action = {
                        switch JSON(userInfo[AnyHashable("key")]!).string {
                            case "new_message":
                                       
                                let conversation_id =  JSON(userInfo[AnyHashable("conversation_id")]!).string
                                let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                                if let topVc = tabBar!.viewControllers![tabBar!.selectedIndex] as? UINavigationController{
                                    let vc =  Storyboard.Home.viewController(ChatViewController.self)
                                     vc.ConvrstionId = conversation_id!
                                    SocketConnection.sharedInstance.socket.off(clientEvent: .connect)
                                    SocketConnection.sharedInstance.manager.disconnect()
                                    topVc.pushViewController(vc, animated: true)
                                }

                                       
                            case "newOrder":
                                       
                                let order_id = JSON(userInfo[AnyHashable("order_id")]!).string
                                let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                                if let topVc = tabBar!.viewControllers![tabBar!.selectedIndex] as? UINavigationController{
                                    let vc =  Storyboard.Home.viewController(OrderDetilsViewController.self)
                                    vc.orderId = order_id!
                                    topVc.pushViewController(vc, animated: true)
                                }
                                       
                            case "agreeBid":
                                    let conversation_id = JSON(userInfo[AnyHashable("conversation_id")]!).string
                                    let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                                    if let topVc = tabBar!.viewControllers![tabBar!.selectedIndex] as? UINavigationController{
                                    let vc =  Storyboard.Home.viewController(ChatViewController.self)
                                    vc.ConvrstionId = conversation_id!
                                    SocketConnection.sharedInstance.socket.off(clientEvent: .connect)
                                    SocketConnection.sharedInstance.manager.disconnect()
                                    topVc.pushViewController(vc, animated: true)
                                }
                        case "applyBid":
                                       
                            let bid_id = JSON(userInfo[AnyHashable("bid_id")]!).string
                            let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                            if let topVc = tabBar!.viewControllers![tabBar!.selectedIndex] as? UINavigationController{
                                let topController = tabBar!.viewControllers![tabBar!.selectedIndex]
                                let vc = Storyboard.Home.viewController(OffersBidAlert.self)
                                vc.BidId = bid_id!
                                vc.viewController = topController
                                let popupVC = PopupViewController(contentController: vc, popupWidth: UIScreen.main.bounds.width-60, popupHeight: 200)
                                            
                                popupVC.backgroundAlpha = 0.5
                                popupVC.backgroundColor = .black
                                popupVC.canTapOutsideToDismiss = true
                                popupVC.cornerRadius = 20
                                popupVC.shadowEnabled = true
                                            
                                            
                                topController.present(popupVC, animated: true)
                            }
        //                case "finishOrder":
        //
        //                    let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
        //                    tabBar?.selectedIndex = 3
                        case "answerContact":
                            let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                            tabBar?.selectedIndex = 3

                        case "createInvoice":
                                       
                            let conversation_id =  JSON(userInfo[AnyHashable("conversation_id")]!).string
                            

                            if defaults.object(forKey: "isopenChat") != nil {
                                if defaults.string(forKey: "isopenChat") == "true"{
                                    if defaults.object(forKey: "Convrstion_Id") != nil {
                                    let current_conv_id = defaults.string(forKey: "Convrstion_Id")
                                        if current_conv_id == conversation_id {
                                                NotificationCenter.default.post(name: .FireNotfication, object:nil)

                                            
                                            
                                        }else{
                                            let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                                            if let topVc = tabBar!.viewControllers![tabBar!.selectedIndex] as? UINavigationController{
                                                let vc =  Storyboard.Home.viewController(ChatViewController.self)
                                                vc.ConvrstionId = conversation_id!
                                                SocketConnection.sharedInstance.socket.off(clientEvent: .connect)
                                                SocketConnection.sharedInstance.manager.disconnect()
                                                topVc.pushViewController(vc, animated: true)
                                            }
                                        }
                                    }
                                }else{
                                    let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                                    if let topVc = tabBar!.viewControllers![tabBar!.selectedIndex] as? UINavigationController{
                                        let vc =  Storyboard.Home.viewController(ChatViewController.self)
                                        vc.ConvrstionId = conversation_id!
                                        SocketConnection.sharedInstance.socket.off(clientEvent: .connect)
                                        SocketConnection.sharedInstance.manager.disconnect()
                                        topVc.pushViewController(vc, animated: true)
                                    }
                                }
                            }
                            

                        case "from_admin":
                                       
                            let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                            tabBar?.selectedIndex = 3
                                       
                        default:
                            let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                            tabBar?.selectedIndex = 3
                        }
                    }
                    attributes.entryInteraction.customTapActions.append(action)
                    
                    if  JSON(userInfo[AnyHashable("key")]!).string == "createInvoice"{
                        let conversation_id =  JSON(userInfo[AnyHashable("conversation_id")]!).string
                        if defaults.object(forKey: "isopenChat") != nil {
                            if defaults.string(forKey: "isopenChat") == "true"{
                                if defaults.object(forKey: "Convrstion_Id") != nil {
                                    let current_conv_id = defaults.string(forKey: "Convrstion_Id")
                                    if current_conv_id != conversation_id {
                                        SwiftEntryKit.display(entry: contentView, using: attributes)
                                    }
                                }else{
                                    SwiftEntryKit.display(entry: contentView, using: attributes)
                                }
                            }else{
                                SwiftEntryKit.display(entry: contentView, using: attributes)
                            }
                        }else{
                            SwiftEntryKit.display(entry: contentView, using: attributes)
                        }
                        
                    }else{
                        SwiftEntryKit.display(entry: contentView, using: attributes)

                    }
                    
                    
                    //=======
                    switch JSON(userInfo[AnyHashable("key")]!).string {
                         
                        
                        case "createInvoice":
                                   
                            let conversation_id =  JSON(userInfo[AnyHashable("conversation_id")]!).string
                        
                            //    defaults.set("true", forKey: "isopenChat")

                            if defaults.object(forKey: "isopenChat") != nil {
                                if defaults.string(forKey: "isopenChat") == "true"{
                                    if defaults.object(forKey: "Convrstion_Id") != nil {
                                        let current_conv_id = defaults.string(forKey: "Convrstion_Id")
                                        if current_conv_id == conversation_id {
                                            NotificationCenter.default.post(name: .FireNotfication, object:nil)
                                        }
                                    }
                                }
                            }
                                        
                        case "cancelOrder":
            
                           let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                                if let topVc = tabBar!.viewControllers![tabBar!.selectedIndex] as? UINavigationController{
                                topVc.popToRootViewController(animated: true)
                            }

                        case "withdrawOrder":
                            let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                                if let topVc = tabBar!.viewControllers![tabBar!.selectedIndex] as? UINavigationController{
                                topVc.popToRootViewController(animated: true)
                            }
                        
                        case "block_user":
                            SocketConnection.sharedInstance.socket.disconnect()
                            SocketConnection.sharedInstance.manager.disconnect()
                            defaults.set("no", forKey: is_LoogedIn)
                            let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                            if let topVc = tabBar!.viewControllers![tabBar!.selectedIndex] as? UINavigationController{
                                let Reg = Storyboard.Authentication.viewController(LoginViewController.self)
                                topVc.pushViewController(Reg, animated: true)
                            }
                        case "delete_user":
                                                   
                            SocketConnection.sharedInstance.socket.disconnect()
                            SocketConnection.sharedInstance.manager.disconnect()
                            defaults.set("no", forKey: is_LoogedIn)
                            let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                            if let topVc = tabBar!.viewControllers![tabBar!.selectedIndex] as? UINavigationController{
                                let Reg = Storyboard.Authentication.viewController(LoginViewController.self)
                                topVc.pushViewController(Reg, animated: true)
                            }
                        
                        case "finishOrder":
                            let order_id =  JSON(userInfo[AnyHashable("order_id")]!).string
                                if defaults.object(forKey: "isopenChat") != nil {
                                    if defaults.string(forKey: "isopenChat") == "true"{
                                        if defaults.object(forKey: "order_id") != nil {
                                            let current_order_id = defaults.string(forKey: "order_id")
                                            if current_order_id == order_id {
                                                NotificationCenter.default.post(name: .reteView, object:nil)
                                            }
                                        }
                                    }
                                }
                        case "ratingYou":
                        let conversation_id =  JSON(userInfo[AnyHashable("conversation_id")]!).string
                            if defaults.object(forKey: "isopenChat") != nil {
                                if defaults.string(forKey: "isopenChat") == "true"{
                                    if defaults.object(forKey: "Convrstion_Id") != nil {
                                        let current_conv_id = defaults.string(forKey: "Convrstion_Id")
                                        if current_conv_id == conversation_id {
                                            NotificationCenter.default.post(name: .reteView, object:nil)
                                        }
                                    }
                                }else{
                                    let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                                    if let topVc = tabBar!.viewControllers![tabBar!.selectedIndex] as? UINavigationController{
                                        let vc =  Storyboard.Home.viewController(ChatViewController.self)
                                        vc.ConvrstionId = conversation_id!
                                        SocketConnection.sharedInstance.socket.off(clientEvent: .connect)
                                        SocketConnection.sharedInstance.manager.disconnect()
                                        topVc.pushViewController(vc, animated: true)
                                        NotificationCenter.default.post(name: .reteView, object:nil)

                                    }
                                }
                            }
                        case "activeDelegate":
                            defaults.set("yes", forKey: is_Delgate)
             
                           
                                                   
                        default:
                            print("noti")
                        }

                    
                    completionHandler([])
                }
                
                func userNotificationCenter(_ center: UNUserNotificationCenter,
                                            didReceive response: UNNotificationResponse,
                                            withCompletionHandler completionHandler: @escaping () -> Void) {
                    
                    
        //            let pushContent = notification.request.content
        //            let badgeCount = pushContent.badge as? Int
        //            let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
        //            tabBar!.tabBar.items![3].badgeValue = String(describing: badgeCount)
                    NotificationCenter.default.post(name: .FireNotfication, object:nil)
                    print("did receive response")
                    print("will present notification")
                    print("🤩 \(response.notification.request.content.userInfo)")
                    
                    let userInfo = response.notification.request.content.userInfo
                    _ = JSON(userInfo[AnyHashable("title")]!).string
                    let message_ar = JSON(userInfo[AnyHashable("message_ar")]!).string
                    let message_en = JSON(userInfo[AnyHashable("message_en")]!).string
                    var massgae = ""
                    if(getServerLang() == "ar"){
                        massgae = message_ar!
                    }else{
                        massgae = message_en!
                    }
                    
                    
                    if let messageID = userInfo[gcmMessageIDKey] {
                        print("Message ID: \(messageID)")
                    }
                    print("⛩\(JSON(userInfo[AnyHashable("key")]!).string!)")
                    print("from foreground \(userInfo)")
                    
                    
                    
                switch JSON(userInfo[AnyHashable("key")]!).string {
                    case "new_message":
                        let conversation_id =  JSON(userInfo[AnyHashable("conversation_id")]!).string
                        let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                        if let topVc = tabBar!.viewControllers![tabBar!.selectedIndex] as? UINavigationController{
                            let vc =  Storyboard.Home.viewController(ChatViewController.self)
                            vc.ConvrstionId = conversation_id!
                            SocketConnection.sharedInstance.socket.off(clientEvent: .connect)
                            SocketConnection.sharedInstance.manager.disconnect()
                            topVc.pushViewController(vc, animated: true)
                        }

                                   
                    case "newOrder":
                                   
                        let order_id = JSON(userInfo[AnyHashable("order_id")]!).string
                        let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                        if let topVc = tabBar!.viewControllers![tabBar!.selectedIndex] as? UINavigationController{
                            let vc =  Storyboard.Home.viewController(OrderDetilsViewController.self)
                            vc.orderId = order_id!
                            topVc.pushViewController(vc, animated: true)
                        }
                                   
                    case "agreeBid":
                        let conversation_id = JSON(userInfo[AnyHashable("conversation_id")]!).string
                        let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                        if let topVc = tabBar!.viewControllers![tabBar!.selectedIndex] as? UINavigationController{
                            let vc =  Storyboard.Home.viewController(ChatViewController.self)
                            vc.ConvrstionId = conversation_id!
                            SocketConnection.sharedInstance.socket.off(clientEvent: .connect)
                            SocketConnection.sharedInstance.manager.disconnect()
                            topVc.pushViewController(vc, animated: true)
                        }
                    case "applyBid":
                        
                        let bid_id = JSON(userInfo[AnyHashable("bid_id")]!).string
                        let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                        if let topVc = tabBar!.viewControllers![tabBar!.selectedIndex] as? UINavigationController{
                            let topController = tabBar!.viewControllers![tabBar!.selectedIndex]
                            let vc = Storyboard.Home.viewController(OffersBidAlert.self)
                            vc.BidId = bid_id!
                            vc.viewController = topController
                            let popupVC = PopupViewController(contentController: vc, popupWidth: UIScreen.main.bounds.width-60, popupHeight: 200)
                                        
                            popupVC.backgroundAlpha = 0.5
                            popupVC.backgroundColor = .black
                            popupVC.canTapOutsideToDismiss = true
                            popupVC.cornerRadius = 20
                            popupVC.shadowEnabled = true
                                        
                                        
                            topController.present(popupVC, animated: true)
                        }
                    case "cancelOrder":
                    
                        let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                        if let topVc = tabBar!.viewControllers![tabBar!.selectedIndex] as? UINavigationController{
                            topVc.popToRootViewController(animated: true)
                        }

                    case "withdrawOrder":
                        let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                        if let topVc = tabBar!.viewControllers![tabBar!.selectedIndex] as? UINavigationController{
                            topVc.popToRootViewController(animated: true)
                        }
                    case "answerContact":
                        let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                        tabBar?.selectedIndex = 3
        //            case "finishOrder":
        //
        //                let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
        //                tabBar?.selectedIndex = 3
                    case "createInvoice":
                        
        //               let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
        //                tabBar?.selectedIndex = 3
                        let conversation_id =  JSON(userInfo[AnyHashable("conversation_id")]!).string
                              
                        let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                        if let topVc = tabBar!.viewControllers![tabBar!.selectedIndex] as? UINavigationController{
                            let vc =  Storyboard.Home.viewController(ChatViewController.self)
                            vc.ConvrstionId = conversation_id!
                            SocketConnection.sharedInstance.socket.off(clientEvent: .connect)
                            SocketConnection.sharedInstance.manager.disconnect()
                            topVc.pushViewController(vc, animated: true)
                        }
                    case "activeDelegate":
                        
                        defaults.set("yes", forKey: is_Delgate)
                    
                        
                    case "block_user":
                        SocketConnection.sharedInstance.socket.disconnect()
                        SocketConnection.sharedInstance.manager.disconnect()
                        defaults.set("no", forKey: is_LoogedIn)
                        let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                        if let topVc = tabBar!.viewControllers![tabBar!.selectedIndex] as? UINavigationController{
                            let Reg = Storyboard.Authentication.viewController(LoginViewController.self)
                            topVc.pushViewController(Reg, animated: true)
                        }
                    case "delete_user":
                                               
                        SocketConnection.sharedInstance.socket.disconnect()
                        SocketConnection.sharedInstance.manager.disconnect()
                        defaults.set("no", forKey: is_LoogedIn)
                        let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                        if let topVc = tabBar!.viewControllers![tabBar!.selectedIndex] as? UINavigationController{
                            let Reg = Storyboard.Authentication.viewController(LoginViewController.self)
                            topVc.pushViewController(Reg, animated: true)
                        }
                    
                    case "from_admin":
                        
                        let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                        tabBar?.selectedIndex = 3
                    case "finishOrder":
                    
                        let order_id =  JSON(userInfo[AnyHashable("order_id")]!).string
                        if defaults.object(forKey: "isopenChat") != nil {
                            if defaults.string(forKey: "isopenChat") == "true"{
                                if defaults.object(forKey: "order_id") != nil {
                                    let current_order_id = defaults.string(forKey: "order_id")
                                    if current_order_id == order_id {
                                        NotificationCenter.default.post(name: .reteView, object:nil)
                                    }
                                }
                            }
                        }
        //            case "ratingYou":
        //
        //                let conversation_id =  JSON(userInfo[AnyHashable("conversation_id")]!).string
        //                     let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
        //                    if let topVc = tabBar!.viewControllers![tabBar!.selectedIndex] as? UINavigationController{
        //                        let vc =  Storyboard.Home.viewController(ChatViewController.self)
        //                        vc.ConvrstionId = conversation_id!
        //                        SocketConnection.sharedInstance.socket.off(clientEvent: .connect)
        //                        SocketConnection.sharedInstance.manager.disconnect()
        //                        topVc.pushViewController(vc, animated: true)
        //                        NotificationCenter.default.post(name: .reteView, object:nil)
        //
        //                    }
                    
                        
                    default:
                        let tabBar = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController
                        tabBar?.selectedIndex = 3
                    }
                    completionHandler()
                }
                
            }
            // [END ios_10_message_handling]

            extension AppDelegate : MessagingDelegate {
                
                func showAlertAppDelegate(title: String,message : String,buttonTitle: String,window: UIWindow){
                    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.default, handler: nil))
                    window.rootViewController?.present(alert, animated: false, completion: nil)
                }
                
                
                // [START refresh_token]
                func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
                    print("Firebase registration token: \(fcmToken)")
                    //        temp.token = fcmToken
                    temp.token = fcmToken
                    let token = Messaging.messaging().fcmToken
                    
                    AppDelegate.FCMTOKEN = token!
                    //UserDefaults.standard.set(token, forKey: "FCMTOKEN")
                    print("FCM token: \(token ?? "")")
                }
                
                
                // [END refresh_token]
                // [START ios_10_data_message]
                // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
                // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
                
                
                
                func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
                    print("Received data message: \(remoteMessage.appData)")
                    
                    //when open or return to app when push from fcm direct
                    //         let d : [String : Any] = remoteMessage.appData["notification"] as! [String : Any]
                    //         let body : String = d["body"] as! String
                    //         let title : String = d["title"] as! String
                    //         self.showAlertAppDelegate(title: title,message:body,buttonTitle:"اغلاق",window:self.window!)
                    
                    //        if let d = remoteMessage.appData as? [String : Any] {
                    //            if let m = d["body"] as? String {
                    //                showNot(b: m)
                    //            }else if let m = d["sender_id"] as? String {
                    //                print(m)
                    //                print("recMsg")
                    //                NotificationCenter.default.post(name: NSNotification.Name("recMsg"), object: nil)
                    //                //showNot(b: msg.msg)
                    //            }else if let m = d["notification_type"] as? String {
                    //                print(m)
                    //                showNot(b: d["msg"] as! String)
                    //            }
                    //        }
                }
                // [END ios_10_data_message]
                
                func initNotificationSetupCheck() {
                    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                        (granted, error) in
                        print("Permission granted: \(granted)")
                        
                        guard granted else { return }
                        self.getNotificationSettings()
                    }
                }
                
                func getNotificationSettings() {
                    UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                        print("Notification settings: \(settings)")
                        guard settings.authorizationStatus == .authorized else { return }
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
                
                func showNot(b:String){
                    let notification = UNMutableNotificationContent()
                    notification.title = "my own title"
                    //notification.subtitle = ""
                    notification.body = b
                    //notification.badge = 1
                    notification.sound = UNNotificationSound.default
                    notification.setValue(true, forKey: "shouldAlwaysAlertWhileAppIsForeground")
                    
                    let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
                    let request = UNNotificationRequest(identifier: "notification1", content: notification, trigger: notificationTrigger)
                    
                    UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                }
                
            }

