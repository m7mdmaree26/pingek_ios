//
//  OrdersVC.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/16/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import Canvas
import Alamofire
import EzPopup

class OrdersVC: UIViewController {
    
    var statusType = "act"
    var orderType =  "order"
    var userOrders = [UserOrdersModel]()
    var userDelegates = [UserOrdersModel]()
    var selectedOrderId = ""
    
    
    //========buttons
    @IBOutlet weak var userOrderButtonButton: UIButton!
    @IBOutlet weak var userDelegateButton: UIButton!
    @IBOutlet weak var selectedViewLeading: NSLayoutConstraint!
    @IBOutlet weak var noRequstLbl: UILabel!
    @IBOutlet weak var segmantedView: UIView!
    
    @IBOutlet weak var activeView: CSAnimationView!
    @IBOutlet weak var finshedButton: UIButton!
    @IBOutlet weak var activeButton: UIButton!
    @IBOutlet weak var selectStatusButton: UIButton!
    @IBOutlet weak var ordersTableView: UITableView!
    @IBOutlet weak var semanticHeight: NSLayoutConstraint!


    override func viewWillAppear(_ animated: Bool) {
        activeView.isHidden = true
        self.selectStatusButton.setTitle("Active".localized, for: .normal)
        self.orderType = "order"
        self.statusType = "act"
        activeView.isHidden = true
        self.noRequstLbl.isHidden = true
        getMyOrders(status: statusType)
         self.tabBarController?.tabBar.isHidden = false
        
           UIView.animate(withDuration: 0.3) {
               self.userDelegateButton.setTitleColor(UIColor.darkGray, for: .normal)
               self.userOrderButtonButton.setTitleColor(UIColor.white, for: .normal)
               self.selectedViewLeading.constant = 5
               self.view.updateConstraints()
               self.view.layoutIfNeeded()
           }
        
        self.noRequstLbl.isHidden = true
        if getisDelgate() == "yes"{
            segmantedView.isHidden = false
            semanticHeight.constant = 45
        }else{
            segmantedView.isHidden = true
            semanticHeight.constant = 0

        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
        // Do any additional setup after loading the view.
    }
    func setupView(){
        activeView.duration = 0.5
        activeView.delay = 0
        activeView.type = "pop"
        activeView.layer.cornerRadius = 5
        activeView.clipsToBounds = true
        activeView.isHidden = true
        ordersTableView?.register(UINib(nibName: "MyOrdersTableViewCell", bundle: nil), forCellReuseIdentifier: "MyOrdersTableViewCell")
        ordersTableView?.animateTable()
        self.ordersTableView.refreshControl = refreshControl
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
       // self.view.addGestureRecognizer(tap)
        ordersTableView.allowsSelection = true
        activeView.shadowOpacity = 1
        activeView.shadowOffset.height = 1
        activeView.shadowColor = UIColor.lightGray.withAlphaComponent(30)

    }
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        if !activeView.isHidden{
            activeView.isHidden = true
        }
    }
    
    //=============active or finished
    
    @IBAction func selectOrderStatus(_ sender: Any) {
        if activeView.isHidden {
            if self.statusType == "act"{
                activeButton.backgroundColor = UIColor.BasicColor
                activeButton.setTitleColor(UIColor.white, for: .normal)
                finshedButton.backgroundColor = UIColor.white
                finshedButton.setTitleColor(UIColor.darkGray, for: .normal)
            }else{
                finshedButton.backgroundColor = UIColor.BasicColor
                finshedButton.setTitleColor(UIColor.white, for: .normal)
                activeButton.backgroundColor = UIColor.white
                activeButton.setTitleColor(UIColor.darkGray, for: .normal)
            }
            activeView.isHidden = false
            activeView.startCanvasAnimation()
        }else{
            activeView.isHidden = true
        }
        

    }
    
    @IBAction func selectFinish(_ sender: Any) {
        
        self.selectStatusButton.setTitle("Finished".localized, for: .normal)
        self.statusType = "Fin"
        activeView.isHidden = true
        self.noRequstLbl.isHidden = true
        if self.orderType ==  "order" {
            getMyOrders(status: statusType)
        }else{
            getMyDelivarys(status: statusType)
        }
    }
    
    @IBAction func selectActive(_ sender: Any) {
        self.selectStatusButton.setTitle("Active".localized, for: .normal)
        self.statusType = "act"
        activeView.isHidden = true
        self.noRequstLbl.isHidden = true
        if self.orderType ==  "order" {
            getMyOrders(status: statusType)
        }else{
            getMyDelivarys(status: statusType)
        }
    }
    
    //===================order 0r delegate ============
    
    @IBAction func userOrders(_ sender: Any) {
        //============init data to active
        self.selectStatusButton.setTitle("Active".localized, for: .normal)
        self.orderType = "order"
        self.statusType = "act"
        activeView.isHidden = true
        self.noRequstLbl.isHidden = true
        getMyOrders(status: statusType)
        
           UIView.animate(withDuration: 0.3) {
               self.userDelegateButton.setTitleColor(UIColor.darkGray, for: .normal)
               self.userOrderButtonButton.setTitleColor(UIColor.white, for: .normal)
               self.selectedViewLeading.constant = 5
               self.view.updateConstraints()
               self.view.layoutIfNeeded()
           }
        
        self.noRequstLbl.isHidden = true
        

    }
       
    @IBAction func userDelegates(_ sender: Any) {
        //============init data to active
        self.selectStatusButton.setTitle("Active".localized, for: .normal)
        self.orderType = "delegate"
        self.statusType = "act"
        activeView.isHidden = true
        self.noRequstLbl.isHidden = true
        getMyDelivarys(status: statusType)
        
        UIView.animate(withDuration: 0.3) {
            self.userDelegateButton.setTitleColor(UIColor.white, for: .normal)
            self.userOrderButtonButton.setTitleColor(UIColor.darkGray, for: .normal)
            self.selectedViewLeading.constant = 5 + self.userOrderButtonButton.frame.width
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
        }
        self.noRequstLbl.isHidden = true
    }
    
    //===============refresControllers
    
    lazy var refreshControl: UIRefreshControl = {
           
           let refreshControl = UIRefreshControl()
           refreshControl.addTarget(self, action:#selector(handleRefresh(_:)),for: UIControl.Event.valueChanged)
           refreshControl.tintColor = UIColor.BasicColor
           
           return refreshControl
       }()
       
       @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
            self.ordersTableView.animateTable()
            if self.orderType ==  "order" {
                getMyOrders(status: statusType)
            }else{
                getMyDelivarys(status: statusType)
            }
            refreshControl.endRefreshing()
       }
    
}


extension OrdersVC :UITableViewDataSource ,UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.userOrders.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 100
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyOrdersTableViewCell", for: indexPath) as! MyOrdersTableViewCell
         //  if(FlagType == "myorders"){
        let item = self.userOrders[indexPath.section]
        cell.configureCell(UserOrder: item)
        if orderType == "order" {
            if userOrders[indexPath.section].status == "finished" {
                cell.finishedStack.isHidden = false
                cell.watingStack.isHidden = true
                cell.inDelivryLbl.isHidden = true
                cell.delegateStack.isHidden = true
            }else if userOrders[indexPath.section].status == "open" {
                cell.watingStack.isHidden = false
                cell.inDelivryLbl.isHidden = true
                cell.finishedStack.isHidden = true
                cell.delegateStack.isHidden = true

            }else{
                cell.inDelivryLbl.isHidden = false
                cell.watingStack.isHidden = true
                cell.finishedStack.isHidden = true
                cell.delegateStack.isHidden = true
            }
        }else{
            if userOrders[indexPath.section].status == "finished" {
                cell.delegateStack.isHidden = true
                cell.finishedStack.isHidden = false
                cell.watingStack.isHidden = true
                cell.inDelivryLbl.isHidden = true
            }else{
                cell.delegateStack.isHidden = false
                cell.finishedStack.isHidden = true
                cell.watingStack.isHidden = true
                cell.inDelivryLbl.isHidden = true
            }
            
        }
        
        return cell
       
       }
    
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            print("epwqeqwewqe")
            if userOrders[indexPath.section].status == "open" {
                if orderType == "order" {
                    selectedOrderId = userOrders[indexPath.section].id
                     let vc = Storyboard.Home.viewController(OrderAlertWatingVC.self)
                    vc.vc = self
                    let popupVC = PopupViewController(contentController: vc, popupWidth: UIScreen.main.bounds.width-60, popupHeight: 350)
                    popupVC.backgroundAlpha = 0.5
                    popupVC.backgroundColor = .black
                    popupVC.canTapOutsideToDismiss = true
                    popupVC.cornerRadius = 20
                    popupVC.shadowEnabled = true

                    self.present(popupVC, animated: true)

                }
            }else if userOrders[indexPath.section].status == "inprogress" {
                let vc = Storyboard.Home.viewController(ChatViewController.self)
                vc.ConvrstionId = String(describing: self.userOrders[indexPath.section].conversation_id)

                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
    
    
   
}

extension OrdersVC {

    func getMyOrders(status: String){
        userOrders.removeAll()
        self.ordersTableView.animateTable()
        self.showProgress()
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        var url = ""
        if status == "act" {
            url = useractiveOrders_URL
        }else{
            url = userFinshedOrders_URL
        }
        API.POST(url: url, parameters: [:], headers: header) { (succss, value) in
            if succss{
                self.stopAnimating()
                let key = value["key"] as? String

                if(key == "success"){
                    let data = value["data"] as! [[String:Any]]
                    if(data.count == 0){
                        self.noRequstLbl.isHidden = false
                        self.ordersTableView.isHidden = true

                    }else{
                        self.noRequstLbl.isHidden = true
                        self.ordersTableView.isHidden = false

                        for i in data{
                            self.userOrders.append(UserOrdersModel().getObject(dicc: i))

                        }
                    }
                    self.ordersTableView.animateTable()
                }

            }else{
                self.stopAnimating()
            }
        }

    }

    func getMyDelivarys(status: String){
        
        userOrders.removeAll()
        self.ordersTableView.animateTable()
        self.showProgress()
        let parmas :Parameters = [
            "lat": getUserLat(),
            "long":getUserLong()
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        var url = ""
        if status == "act" {
            url = activeOrders_URL
        }else{
            url = finishedOrders_URL
        }
        API.POST(url: url, parameters: parmas, headers: header) { (succss, value) in
            print("🤭\(parmas)")
            if succss{
                self.stopAnimating()
                let key = value["key"] as? String
                if(key == "success"){
                    let data = value["data"] as! [[String:Any]]
                     if(data.count == 0){
                        self.noRequstLbl.isHidden = false
                        self.ordersTableView.isHidden = true

                    }else{
                        self.noRequstLbl.isHidden = true
                        self.ordersTableView.isHidden = false
                        for i in data{
                            self.userOrders.append(UserOrdersModel().getObject(dicc: i))

                        }
                        self.ordersTableView.animateTable()
                    }
                }

            }else{
                self.stopAnimating()
            }
        }
    }
    
    
    func CancelOrder(order_id:String){
        self.showProgress()
        let parmas:Parameters = [
            "order_id":order_id,
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        API.POST(url: CancelOrder_URL, parameters: parmas, headers: header) { (succcss, value) in
            if succcss{
                self.stopAnimating()
                if value["key"] as! String == "success"{
                    ShowTrueMassge(massge: value["msg"] as! String , title: "Done".localized)
                }else{
                    ShowErrorMassge(massge: value["msg"] as! String , title: "Error".localized)
                }
                
                
            }
        }
    }

}


