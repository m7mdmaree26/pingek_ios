//
//  NotificationsVC.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/16/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import EzPopup

class NotificationsVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noNotfiLbl: UILabel!
    
    var NotficationDataSourse = [NotficationsModel]()
    var bidID = ""
    var pageCount = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.showsVerticalScrollIndicator = false
        self.noNotfiLbl.isHidden = true
        self.tableView.refreshControl = refreshControl
        self.tableView.backgroundColor = UIColor.clear
        tableView.register(UINib(nibName: "NotficationsCellTableViewCell", bundle: nil), forCellReuseIdentifier: "NotficationsCellTableViewCell")
        tableView.animateTable()
    }

    override func viewWillAppear(_ animated: Bool) {
//       if(getFromnearOrder() == "notfy"){
//            let Reg =   Storyboard.Main.instantiate(NearPlacesProviderViewController.self)
//                self.navigationController?.pushViewController(Reg, animated: false)
//            }
//        if(getViewOrder() == "notfy"){
//
//            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//            let Reg = storyBoard.instantiateViewController(withIdentifier: "ViewOrderViewController") as! ViewOrderViewController
//            Reg.BidId = getGlobalBid()
//            self.navigationController?.pushViewController(Reg, animated: false)
//        }
        self.tabBarController?.tabBar.items![3].badgeValue = nil
        self.tabBarController?.tabBar.isHidden = false
        self.pageCount = 1
        self.NotficationDataSourse = []
        self.tableView.reloadData()
        self.showProgress()
        getNotfications(page:self.pageCount)
        SocketConnection.sharedInstance.socket.off(clientEvent: .connect)
        SocketConnection.sharedInstance.manager.disconnect()
        
    }

    
    //==============refrresh controller
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:#selector(handleRefresh(_:)),for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.BasicColor
            
        return refreshControl
    }()
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        getNotfications(page:self.pageCount)
        refreshControl.endRefreshing()
    }
}



extension NotificationsVC{
    
    func getNotfications(page:Int){
        self.NotficationDataSourse.removeAll()
        self.tableView.reloadData()
        let parmas :Parameters = [
            "page": page
            
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        API.POST(url: Notficatios_URL, parameters: parmas, headers: header) { (succss, value) in
            print("🤭\(parmas)")
            print("🌸\(value)")
            if succss{
                self.stopAnimating()
                let key = value["key"] as? String
                
                if(key == "success"){
                    let data = value["data"] as! [[String:Any]]
                    if(data.count == 0 && self.NotficationDataSourse.count == 0){
                        self.noNotfiLbl.isHidden = false
                        return
                    }else{
                         for i in data{
                            self.noNotfiLbl.isHidden = true
                            let user_id = i["user_id"] as! Int
                            let id = i["id"] as! Int
                            let name = i["name"] as! String
                            let image = i["image"] as! String
                            let text = i["text"] as! String
                            let order_status = i["order_status"] as! String
                            let seen = i["seen"] as! String
                            let date = i["date"] as! String
                            let conversation_id = i["conversation_id"] as? Int ?? 0
                            let key = i["key"] as! String
                           // let bid_id = i["bid_id"] as? String ?? ""
                            let dataID = i["data"] as! [String:Any]
                            
                            switch key {
                            case "applyBid":
                                ///بتوديني علي صفحه القبول او الرفض
                                let bid_id = dataID["bid_id"] as! String
                                print("bid_id\(bid_id)")
                                self.NotficationDataSourse.append(NotficationsModel(id: id, user_id: user_id, name: name, image: image, text: text,order_status: order_status, seen: seen, date: date,orderNextPage:bid_id, orderRoute:key, place_ref: "", place_id: "", conversation_id: conversation_id))
                                
                            case "newOrder":
                                //بتوديني على صفحه الطلبات القريبه مني بصفه عامه
                                let order_id = dataID["order_id"] as! String
                                print("order_id\(order_id)")
                                var place_id = ""
                                var place_ref = ""
                                if dataID["place_id"] as? String != nil {
                                    place_id = dataID["place_id"] as! String
                                }
                                if dataID["place_id"] as? String != nil {
                                    place_id = dataID["place_id"] as! String
                                }
                                if dataID["store_id"] as? String != nil {
                                    place_id = dataID["store_id"] as! String
                                }
                                self.NotficationDataSourse.append(NotficationsModel(id: id, user_id: user_id, name: name, image: image, text: text,order_status: order_status, seen: seen, date: date,orderNextPage:order_id, orderRoute:key, place_ref: place_ref, place_id: place_id, conversation_id: conversation_id))
                                
                            case "agreeBid":
                                //بتوديني على صفخه الشات
                                let conversationagree_id = dataID["conversation_id"] as! String
                                print("conversation_id\(conversation_id)")
                                self.NotficationDataSourse.append(NotficationsModel(id: id, user_id: user_id, name: name, image: image, text: text,order_status: order_status, seen: seen, date: date,orderNextPage:String(conversationagree_id), orderRoute:key, place_ref: "", place_id: "", conversation_id: conversation_id))
                                
                            case "newOrderwithPlace":
                            //بتوديني على صفحه الطلبات القريبه الي خاصه بمكان معين
                                let place_id = dataID["place_id"] as! String
                                let place_ref = dataID["place_ref"] as! String
                                print("newOrderwithPlace)")
                                self.NotficationDataSourse.append(NotficationsModel(id: id, user_id: user_id, name: name, image: image, text: text, order_status: order_status, seen: seen, date: date, orderNextPage: "", orderRoute: key, place_ref: place_ref, place_id: place_id, conversation_id: conversation_id))
                            case "ratingYou":
                                //بتودني على صفحه الكومنتات
                                
                                self.NotficationDataSourse.append(NotficationsModel(id: id, user_id: user_id, name: name, image: image, text: text,order_status: order_status, seen: seen, date: date,orderNextPage:"", orderRoute:key, place_ref: "", place_id: "", conversation_id: conversation_id))
                          
                            default:
                                var order_id = ""
                                if dataID["order_id"] as? String != nil {
                                    order_id = dataID["order_id"] as! String
                                }
                                self.NotficationDataSourse.append(NotficationsModel(id: id, user_id: user_id, name: name, image: image, text: text,order_status: order_status, seen: seen, date: date,orderNextPage:order_id, orderRoute:key, place_ref: "", place_id: "", conversation_id: conversation_id))
                            }
                            
                            
                        }
                        print("💙\(self.NotficationDataSourse.count)")
                        self.tableView.reloadData()
                    }
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
        
    }
    
    func deleteNotifications(index : Int){
        self.tableView.reloadData()
        let parmas :Parameters = [
            "id": self.NotficationDataSourse[index].id
            
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        API.POST(url: NotficatiosDelete_URL, parameters: parmas, headers: header) { (succss, value) in
            print("🤭\(parmas)")
            print("🌸\(value)")
            if succss{
                self.stopAnimating()
                let key = value["key"] as? String
                if(key == "success"){
                    self.NotficationDataSourse.remove(at: index)
                    self.tableView.reloadData()
                }else{
                   let msg = value["msg"] as? String
                    ShowErrorMassge(massge: msg!, title: "Error".localized)
                }
                if self.NotficationDataSourse.count == 0 {
                    self.noNotfiLbl.isHidden = false
                }else{
                   self.noNotfiLbl.isHidden = true
                }
        

            }else{
                self.stopAnimating()
                    
                
            }
            
        }
        
    }
    
}

    



extension NotificationsVC : UITableViewDelegate , UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return NotficationDataSourse.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 80
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotficationsCellTableViewCell", for: indexPath) as! NotficationsCellTableViewCell
        if(self.NotficationDataSourse.count != 0){
            let item = self.NotficationDataSourse[indexPath.section]
            cell.configureCell(notfi: item)
        }
        return cell
    }
            
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("🌷🌷🌷\(self.NotficationDataSourse[indexPath.section].orderRoute)")
        switch self.NotficationDataSourse[indexPath.section].orderRoute {
        case "applyBid":
            if(self.NotficationDataSourse[indexPath.section].conversation_id == 0){
                let vc = Storyboard.Home.viewController(OffersBidAlert.self)
                vc.BidId = self.NotficationDataSourse[indexPath.section].orderNextPage
                vc.vc = self
                let popupVC = PopupViewController(contentController: vc, popupWidth: UIScreen.main.bounds.width-60, popupHeight: 200)
                            
                popupVC.backgroundAlpha = 0.5
                popupVC.backgroundColor = .black
                popupVC.canTapOutsideToDismiss = true
                popupVC.cornerRadius = 20
                popupVC.shadowEnabled = true
                            
                            
                self.present(popupVC, animated: true)
            }else{
                let vc = Storyboard.Home.viewController(ChatViewController.self)
                vc.ConvrstionId = String(self.NotficationDataSourse[indexPath.section].conversation_id)
                self.navigationController?.pushViewController(vc, animated: false)
            }
        case "agreeBid":

                let vc = Storyboard.Home.viewController(ChatViewController.self)
                
                vc.ConvrstionId = self.NotficationDataSourse[indexPath.section].orderNextPage
                self.navigationController?.pushViewController(vc, animated: false)
        case "ratingYou":
            let vc = Storyboard.Home.viewController(CustomrComentsViewController.self)
            self.navigationController?.pushViewController(vc, animated: false)
//
        case "newOrder":
//            let vc = Storyboard.Home.viewController(StoreDetailsVC.self)
//            vc.notificaion = self.NotficationDataSourse[indexPath.section]
//            self.navigationController?.pushViewController(vc, animated: false)
            self.tabBarController?.selectedIndex = 2
               
    
        case "newOrderwithPlace":
            let vc = Storyboard.Home.viewController(StoreDetailsVC.self)
            vc.notificaion = self.NotficationDataSourse[indexPath.section]
            self.navigationController?.pushViewController(vc, animated: false)

    
        case "open":
            print("oppen")
    
        default:
            break
        }

    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(indexPath.section == self.NotficationDataSourse.count - 1){
            if(self.NotficationDataSourse.count == 10 || self.NotficationDataSourse.count > 10){
                self.pageCount  += 1
                print("🍉\( self.pageCount)")
                self.getNotfications(page: pageCount)
            }
        }
        
    }
    
    
     func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let delete = UIContextualAction(style: .normal, title: "delete"){(action, view , completion) in
            self.deleteNotifications(index: indexPath.row)
        }
        delete.backgroundColor =  UIColorFromRGB(rgbValue: 0xE0342F)
            
           
        delete.image = #imageLiteral(resourceName: "binn")
            
           
        return UISwipeActionsConfiguration(actions :[delete])
    }
    
}
