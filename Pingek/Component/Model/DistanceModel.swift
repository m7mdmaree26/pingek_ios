//
//  DistanceModel.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/28/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import Foundation
class DistanceModel: NSObject {
    var rows = [RowsModel]()
  
    func getObject(dicc: [String: Any]) -> DistanceModel {
        let dic = HandleJSON.getObject().handle(dicc: dicc)
        let model = DistanceModel()
        let rows = dic["rows"] as! [[String: Any]]
        model.rows.removeAll()
        for item in rows {
            model.rows.append(RowsModel().getObject(dicc: item))
        }
        print("💄\(self.rows.count)")
        
        
        
        return model
    }
}
