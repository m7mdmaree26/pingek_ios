//
//  RegisterViewController.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/20/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import GoogleMaps
import SKCountryPicker

class RegisterViewController: UIViewController,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var checkTermsButton: UIButton!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var mobileNumber: UITextField!
    @IBOutlet weak var email: UITextField!
//    @IBOutlet weak var Location: BorderBaddedTextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var RegistrBtn: UIButton!
    @IBOutlet weak var pickCountryButton: UIButton!
    @IBOutlet weak var countryImageView: UIImageView!
    @IBOutlet weak var viewImage: UIButton!

    var utils = AppUtils.getObject()
    var flag1 = false
    var flag2 = false
    var flag3 = false
    var flag4 = false
    var lat:Double?
    var lng:Double?
    var termsIsOn = false
    var pickedCountrycode = ""
    
    var newLat : Double = 0.0
    var newLng : Double = 0.0
   

    override func viewDidLoad() {
        super.viewDidLoad()
        self.logo.image = #imageLiteral(resourceName: "logo")

        self.hideKeyboardWhenTappedAround()
//        Location.isUserInteractionEnabled = false
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        mobileNumber.setLeftPaddingPoints(20)
        mobileNumber.setRightPaddingPoints(20)
        password.setLeftPaddingPoints(20)
        password.setRightPaddingPoints(40)
        userName.setLeftPaddingPoints(20)
        userName.setRightPaddingPoints(20)
        email.setLeftPaddingPoints(20)
        email.setRightPaddingPoints(20)
        mobileNumber.keyboardType = .asciiCapableNumberPad
        
        //=========country
        guard let country = CountryManager.shared.currentCountry else {
            self.countryImageView.isHidden = true
            return
        }

        
        pickCountryButton.setTitle("        " + country.dialingCode!, for: .normal)
        self.pickedCountrycode = country.dialingCode!
        countryImageView.image = country.flag
        pickCountryButton.clipsToBounds = true
        
        mobileNumber.addTarget(self, action: #selector(FoucseUIEditText), for: UIControl.Event.editingDidBegin)
        mobileNumber.addTarget(self, action: #selector(UnFoucseUIEditText), for: UIControl.Event.editingDidEnd)
        password.addTarget(self, action: #selector(FoucseUIEditText), for: UIControl.Event.editingDidBegin)
        password.addTarget(self, action: #selector(UnFoucseUIEditText), for: UIControl.Event.editingDidEnd)
        userName.addTarget(self, action: #selector(FoucseUIEditText), for: UIControl.Event.editingDidBegin)
        userName.addTarget(self, action: #selector(UnFoucseUIEditText), for: UIControl.Event.editingDidEnd)
        email.addTarget(self, action: #selector(FoucseUIEditText), for: UIControl.Event.editingDidBegin)
        email.addTarget(self, action: #selector(UnFoucseUIEditText), for: UIControl.Event.editingDidEnd)

    }
    
    @objc func FoucseUIEditText(text:UITextField){
        text.layer.borderColor = UIColor.BasicColor.cgColor
        text.layer.borderWidth = 2
    }
    
    @objc func UnFoucseUIEditText(text:UITextField){
        text.layer.borderColor = UIColor.clear.cgColor
        text.layer.borderWidth = 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @IBAction func viewPassword(_ sender: Any) {
        if password.isSecureTextEntry {
            self.viewImage.setImage(#imageLiteral(resourceName: "view-1"), for: .normal)
            password.isSecureTextEntry = false
        }else{
            self.viewImage.setImage(#imageLiteral(resourceName: "unview"), for: .normal)
            password.isSecureTextEntry = true
        }
    }
    
    @IBAction func pickCountry(_ sender: Any) {
        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { [weak self] (country: Country) in

            guard let self = self else { return }
            self.countryImageView.image = country.flag
            self.pickedCountrycode = country.dialingCode!
            self.pickCountryButton.setTitle("        " + country.dialingCode!, for: .normal)

        }

        countryController.flagStyle = .circular
        countryController.detailColor = UIColor.BasicColor

    }
    
    @IBAction func checkConditions(_ sender: Any) {
        if termsIsOn {
            termsIsOn = false
            checkTermsButton.setImage(#imageLiteral(resourceName: "check_off"), for: .normal)

        }else{
            checkTermsButton.setImage(#imageLiteral(resourceName: "check_on"), for: .normal)
            termsIsOn = true
        }
    }
    @IBAction func openTerms(_ sender: Any) {
        let Reg = Storyboard.Home.viewController(TermsViewController.self)
        self.navigationController?.pushViewController(Reg, animated: true)

    }
    
    @IBAction func goToLogin(_ sender: Any) {
        let Reg = Storyboard.Authentication.viewController(LoginViewController.self)
        
        self.navigationController?.pushViewController(Reg, animated: true)
            }
//    @IBAction func goToLocationView(_ sender: Any) {
//        let Reg = Storyboard.Authentication.viewController(GetLocationViewController.self)
//         Reg.delegate = self
//        self.navigationController?.pushViewController(Reg, animated: true)
//    }
 
//    func didSelectAddrress(lat: Double, lng: Double, add: String) {
//        if lat != 0.0{
//            newLat = lat
//            newLng  = lng
//            self.Location.text! = add
//        }
//    }
    
    static var UserLat = UserDefaults.standard.float(forKey: User_Lat)
    static var UserLng = UserDefaults.standard.float(forKey: User_Lng)
    
    
    @IBAction func RegisterAction(_ sender: Any) {
        if(email.text?.isEmpty == true || userName.text?.isEmpty == true || mobileNumber.text?.isEmpty == true  || password.text?.isEmpty == true){
            self.RegistrBtn.shake(duration: 0.5, values: [-12.0, 12.0, -12.0, 12.0, -6.0, 6.0, -3.0, 3.0, 0.0])
            ShowErrorMassge(massge: "Please complete your Data".localized, title: "Error".localized)
        }else{
            if (utils.isValidMobileNumber(mobil:mobileNumber.text!)){
                editTextValid(editText: mobileNumber)
                flag1 = true
            }
            else{
                self.RegistrBtn.shake(duration: 0.5, values: [-12.0, 12.0, -12.0, 12.0, -6.0, 6.0, -3.0, 3.0, 0.0])
                ShowErrorMassge(massge: "Phone number is incorrect".localized, title: "Error".localized)
                
                editTextError(editText: mobileNumber)
              }
            if (utils.isValidpassword(mobil: password.text!)){
                editTextValid(editText: password)
                flag2 = true
            }
            else{
                self.RegistrBtn.shake(duration: 0.5, values: [-12.0, 12.0, -12.0, 12.0, -6.0, 6.0, -3.0, 3.0, 0.0])
                ShowErrorMassge(massge: "You must increase your password for 6 symbols".localized(), title: "Error".localized)
                editTextError(editText: password)
            }
            
            if (utils.isValidEmail(testStr: email.text!)){
                editTextValid(editText: email)
                flag3 = true
            }
            else{
                self.RegistrBtn.shake(duration: 0.5, values: [-12.0, 12.0, -12.0, 12.0, -6.0, 6.0, -3.0, 3.0, 0.0])
                ShowErrorMassge(massge: "You must Enter valid email".localized, title: "Error".localized())
                editTextError(editText: email)
            }
           
            if (flag1 && flag2 && flag3 == true){
                if termsIsOn {
                    self.RegisterNewUser(name: self.userName.text!, phone: self.mobileNumber.text!, password: self.password.text!)
                }else{
                    ShowErrorMassge(massge: "You must agree terms".localized, title: "Error".localized())

                }
            }
        }
    }
    
    func RegisterNewUser(name:String,phone:String,password:String){
        self.startAnimating()
        let params: Parameters = [
            
            "name": name,
            "phone":phone,
            "email":email.text!,
            "password": password,
            "device_id":AppDelegate.FCMTOKEN,
            "country_iso": self.pickedCountrycode,
            "device_type":"ios"
            
        ]
        API.POST(url: Register_URL, parameters: params, headers: ["lang":getServerLang()]) { (success, value) in
            if success{
             let key = value["key"] as! String
                if(key == "fail"){
                     let msg = value["msg"] as! String
                    ShowErrorMassge(massge: msg, title: "Error".localized)
                }else{
                    ShowTrueMassge(massge: "Successfully registered".localized, title: "Success".localized)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        let data = value["data"] as! [String:Any]
                        let id = data["id"] as! Int
                        let token = data["token"] as! String
                        let googlekey = data["googlekey"] as! String
                        let phone = data["phone"] as? String
                        let lat = data["lat"] as? Double
                        let long = data["long"] as? Double
                        let address = data["address"] as? String ?? ""
                        let avatar = data["avatar"] as? String
                        let name = data["name"] as? String
                        let email = data["email"] as? String
                        let time_zone = data["time_zone"] as! String

                        GMSServices.provideAPIKey(googlekey)
                        defaults.set(googlekey, forKey: GOOGLE_KEY)
                        defaults.set(id, forKey: User_ID)
                        defaults.set(token, forKey: Token_ID)
                        defaults.set("no", forKey: is_Delgate)
                        defaults.set("no", forKey: is_Vistor)
                        defaults.set(email, forKey: User_Email)
                        defaults.set(address, forKey: User_Name)
                        defaults.set(time_zone, forKey: Time_zone)
                        defaults.set(address, forKey: User_Address)
                        defaults.set(phone, forKey: Mobile_Number)
                        defaults.set(avatar, forKey: User_Avatar)
                        defaults.set(name, forKey: User_Name)
                        defaults.set(String(describing: lat!), forKey: User_Lat)
                        defaults.set(String(describing: long!), forKey: User_Lng)

                        print("🎾\(id)")
                        print("🍋\(token)")
                        let vc = Storyboard.Authentication.viewController(VrefcationCodeViewController.self)
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                print("🖍\(value)")
                self.stopAnimating()
                
            }else{
                self.stopAnimating()
            }
        }
    }
}
//extension RegisterViewController : sendDataBackDelegate{
//    func finishPassing(location: String, lat: Double, lng: Double) {
//        self.Location.text = location
//        self.lat = lat
//        self.lng = lng
//    }
//}
