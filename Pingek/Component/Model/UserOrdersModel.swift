//
//  UserOrdersModel.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/16/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import Foundation
class UserOrdersModel{
    var id = ""
    var provider_id = ""
    var providername = ""
    var rate = ""
    var avatar = ""
    var store_id = ""
    var place_name = ""
    var place_icon = ""
    var details = [wattingOrdersDetails]()
    var deliver_time = ""
    var price = ""
    var status = ""
    var conversation_id = ""
    var date = ""
    
    func getObject(dicc: [String: Any]) ->  UserOrdersModel {
        let dic = HandleJSON.getObject().handle(dicc: dicc)
        let model =  UserOrdersModel()
        model.id = dic["id"] as! String
        if dic["provider_id"] as? String != nil {
            model.provider_id = dic["provider_id"] as! String
        }
        if dic["providername"] as? String != nil {
            model.providername = dic["providername"] as! String
        }else if dic["client_name"] as? String != nil {
            model.providername = dic["client_name"] as! String
        }
        model.rate = dic["rate"] as! String
        model.avatar = dic["avatar"] as! String
        if dic["store_id"] as? String != nil {
            model.store_id = dic["store_id"] as! String
        }
        model.place_name = dic["place_name"] as! String
        model.place_icon = dic["place_icon"] as! String
        model.deliver_time = dic["deliver_time"] as! String
        model.price = dic["price"] as! String
        model.status = dic["status"] as! String
        model.conversation_id = dic["conversation_id"] as! String
        model.date = dic["date"] as! String
    
        
        if  dic["details"] as? [[String: Any]] != nil {
            for item in dic["details"] as! [[String: Any]] {
                model.details.append(wattingOrdersDetails().getObject(dicc: item))
            }
        }else if dic["details"] as? String != nil {
            let details = wattingOrdersDetails()
            details.value = dic["details"] as! String
            details.key = "text"
            model.details.append(details)
        }
        return model
                  
    }
}
