//
//  insideTableViewCell.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/26/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit

class insideTableViewCell: UITableViewCell {
    @IBOutlet weak var ContView: UIView!
    @IBOutlet weak var restruntName: UILabel!
    @IBOutlet weak var restruntSpace: UILabel!
    @IBOutlet weak var restruntImage: UIImageView!
    @IBOutlet weak var storeDistance: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.restruntName.adjustsFontSizeToFitWidth = true
        self.restruntImage.layer.cornerRadius = self.restruntImage.frame.height/2
        self.restruntImage.layer.masksToBounds = true
        self.restruntImage.clipsToBounds = true
        self.selectionStyle = UITableViewCell.SelectionStyle.none
        self.shadowColor = UIColor.lightGray.withAlphaComponent(0.5)
        self.shadowOpacity = 1
    }

   
}
