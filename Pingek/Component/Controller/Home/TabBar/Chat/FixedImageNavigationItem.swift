//
//  FixedImageNavigationItem.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/30/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import Foundation
import UIKit
class FixedImageNavigationItem: UINavigationItem {
    
    private let fixedImage : UIImage = UIImage(named: "222")!
    private let imageView : UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 37.5))
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        imageView.contentMode = .scaleAspectFit
        imageView.image = fixedImage
        self.titleView = imageView
        self.leftBarButtonItem?.image  = fixedImage
        //self.leftBarButtonItem?.image = imageView.image
    }
    
}
