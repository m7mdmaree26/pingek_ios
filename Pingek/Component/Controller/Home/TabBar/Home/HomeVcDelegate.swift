//
//  HomeVcDelegate.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/21/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit


extension HomeVC: UITableViewDataSource ,UITableViewDelegate {
    
    
    func configureMenuTableView(){
        tableView.register(UINib(nibName: "insideTableViewCell", bundle: nil), forCellReuseIdentifier: "insideTableViewCell")
        tableView.animateTable()
    }
    /////////////////==================>>>>>>> TableView((SecondView))
    func numberOfSections(in tableView: UITableView) -> Int {
        return AllNearStores.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
        
        
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
            
        return 90
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    //        UserDefaults.standard.set("home", forKey: orderDetilsFrom)
    //        let Reg =   Storyboard.Main.instantiate(RestrantDetilsViewController.self)
    //        Reg.RestruntLat = Double(self.AllNearStores[indexPath.section].lat)
    //        Reg.RestruntLng = Double(self.AllNearStores[indexPath.section].long)
    //        Reg.RestruntDestince =  self.AllNearStores[indexPath.section].distance
    //        Reg.RestruntIcon = self.AllNearStores[indexPath.section].icon
    //        Reg.RestruntID =  self.AllNearStores[indexPath.section].place_id
    //        Reg.RestruntRef = self.AllNearStores[indexPath.section].reference
    //        Reg.StoreID = self.AllNearStores[indexPath.section].storeID
    //
    //        self.navigationController?.pushViewController(Reg, animated: true)
        let vc = Storyboard.Home.viewController(StoreDetailsVC.self)
        vc.store = self.AllNearStores[indexPath.section]
        if self.AllNearStores[indexPath.section].reference == "" {
            vc.isGoogleStore = false
        }else{
            vc.isGoogleStore = true
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
            
            
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "insideTableViewCell", for: indexPath) as! insideTableViewCell
        cell.restruntName.text = AllNearStores[indexPath.section].name
        cell.restruntImage.setImageWith(AllNearStores[indexPath.section].icon)
        cell.restruntSpace.text =  AllNearStores[indexPath.section].vicinity
        cell.storeDistance.text =  AllNearStores[indexPath.section].distance
        

            
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
     //   if indexPath.section == self.AllNearStores.count - 1{
//
//            if(self.isNextToken == true){
//                if isSearchLastRun{
//                    getSearchData(lat: getUserLat(), long: getUserLong(), next_page_token: self.next_page_token,name:searchText.text!)
//
//
//                }else{
//                    self.getHomeData(lat: getUserLat(), long: getUserLong(), next_page_token: next_page_token, cats: self.CurrntType)
//                }
//            }

//        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollViewHeight = scrollView.frame.size.height
        let scrollContentSizeHeight = scrollView.contentSize.height
        let scrollOffset = scrollView.contentOffset.y

        if (scrollOffset == 0)
        {
            // then we are at the top
        }
        else if (scrollOffset + scrollViewHeight == scrollContentSizeHeight){
            if(self.isNextToken == true){
                if isSearchLastRun{
                    self.tableIndcator.startAnimating()
                    getSearchData(lat: getUserLat(), long: getUserLong(), next_page_token: self.next_page_token,name:searchText.text!)


                }else{
                    self.tableIndcator.startAnimating()
                    self.getHomeData(lat: getUserLat(), long: getUserLong(), next_page_token: next_page_token, cats: self.CurrntType)
                }
            }
        }
    }
    
}



//=============collection


extension HomeVC : UICollectionViewDelegate ,UICollectionViewDataSource,UIScrollViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
              
        if collectionView == self.collectionView {
            return self.icon.count

        }else{
            return self.specialOrdersImages.count

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collectionView {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
                cell.iconImage.image = self.icon[indexPath.row]
                cell.iconLbl.text = self.iconName[indexPath.row]
                return cell
            }else{
               let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SpecialOrderCollectionCell", for: indexPath) as! SpecialOrderCollectionCell
         
                cell.itemImage.image = self.specialOrdersImages[indexPath.row]
                cell.itemTitle.text = self.specialPrdersTitle[indexPath.row]
            
                return cell
            }
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            if collectionView == self.collectionView {
                return CGSize(width: 80 , height:100)
            }else{
                return CGSize(width: (UIScreen.main.bounds.width)/3 - 20 , height:110)
            }
        }
        
      
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            if collectionView == self.collectionView {
                switch indexPath.row {
                    
                    case 0 :
                        if(getoIsVistor() == "yes"){
                            ShowErrorMassge(massge: "You must Login first".localized, title: "Error".localized)
                            let Reg = Storyboard.Authentication.viewController(LoginViewController.self)
                            self.navigationController?.pushViewController(Reg, animated: true)
                        }else{
                           let vc = Storyboard.Home.viewController(SpecialOrderVC.self)
                           vc.orderType = self.specialPrdersTitle[indexPath.row]
                           self.navigationController?.pushViewController(vc, animated: true)
                        }
                    case 1 :
                        
                        self.AllNearStores.removeAll()
                        self.showProgress()
                        self.CurrntType = ""
                        self.next_page_token = ""
                        self.showProgress()
                        getHomeData(lat: getUserLat(), long: getUserLong(), next_page_token: self.next_page_token, cats: self.CurrntType)
                        self.icon = [#imageLiteral(resourceName: "spisal-2"),#imageLiteral(resourceName: "all"),#imageLiteral(resourceName: "noun_storee-1"),#imageLiteral(resourceName: "noun_restaurant-1"),#imageLiteral(resourceName: "noun_pharmacy-1"),#imageLiteral(resourceName: "caffSelected")]
                        self.collectionView.reloadData()
    //                    if let cell = collectionView.cellForItem(at: indexPath) as? HomeCollectionViewCell {
    //                        cell.backgroundColor = UIColor.DeactiveBasicColor
    //                        cell.layer.borderColor = UIColor.BasicColor.cgColor
    //                    }
                        

                    case 2 :
                        self.AllNearStores.removeAll()
                        self.showProgress()
                        self.CurrntType = "store"
                        self.CurrntName = "store"
                        self.next_page_token = ""
                        self.showProgress()
                        getHomeData(lat: getUserLat(), long: getUserLong(), next_page_token: self.next_page_token, cats: self.CurrntType)
                      //
                        
                        self.icon = [#imageLiteral(resourceName: "spisal-2"),#imageLiteral(resourceName: "alll"),#imageLiteral(resourceName: "noun_storeee"),#imageLiteral(resourceName: "noun_restaurant-1"),#imageLiteral(resourceName: "noun_pharmacy-1"),#imageLiteral(resourceName: "caffSelected")]
                        self.collectionView.reloadData()
                    case 3:
                        self.AllNearStores.removeAll()
                        self.showProgress()
                        self.next_page_token = ""
                        self.CurrntType = "restaurant"
                        self.CurrntName = "restaurant"
                        self.showProgress()
                        getHomeData(lat: getUserLat(), long: getUserLong(), next_page_token: self.next_page_token, cats: self.CurrntType)
                        self.icon = [#imageLiteral(resourceName: "spisal-2"),#imageLiteral(resourceName: "alll"),#imageLiteral(resourceName: "noun_storee-1"),#imageLiteral(resourceName: "noun_restaurantt"),#imageLiteral(resourceName: "noun_pharmacy-1"),#imageLiteral(resourceName: "caffSelected")]
                        self.collectionView.reloadData()

                    case 4:
                        self.AllNearStores.removeAll()
                       self.showProgress()
                        self.next_page_token = ""
                        self.CurrntType = "pharmacy"
                        self.CurrntName = "pharmacy"
                        self.showProgress()
                        getHomeData(lat: getUserLat(), long: getUserLong(), next_page_token: self.next_page_token, cats: self.CurrntType)
                        self.icon = [#imageLiteral(resourceName: "spisal-2"),#imageLiteral(resourceName: "alll"),#imageLiteral(resourceName: "noun_storee-1"),#imageLiteral(resourceName: "noun_restaurant-1"),#imageLiteral(resourceName: "noun_pharmacyy"),#imageLiteral(resourceName: "caffSelected")]
                        self.collectionView.reloadData()

                        
                    case 5:
                        self.AllNearStores.removeAll()
                        self.showProgress()
                        self.next_page_token = ""
                        self.CurrntType = "cafe"
                        self.CurrntName = "cafe"
                        self.showProgress()
                        getHomeData(lat: getUserLat(), long: getUserLong(), next_page_token: self.next_page_token, cats: self.CurrntType)
                        self.icon = [#imageLiteral(resourceName: "spisal-2"),#imageLiteral(resourceName: "alll"),#imageLiteral(resourceName: "noun_storee-1"),#imageLiteral(resourceName: "noun_restaurant-1"),#imageLiteral(resourceName: "noun_pharmacy-1"),#imageLiteral(resourceName: "coffeee")]
                        self.collectionView.reloadData()

                    default:
                        print("👏🏻\(indexPath.row)")
                }
            }else{
               ///========
                if(getoIsVistor() == "yes"){
                    ShowErrorMassge(massge: "You must Login first".localized, title: "Error".localized)
                    let Reg = Storyboard.Authentication.viewController(LoginViewController.self)
                    self.navigationController?.pushViewController(Reg, animated: true)
                }else{
                   let vc = Storyboard.Home.viewController(SpecialOrderVC.self)
                   vc.orderType = self.specialPrdersTitle[indexPath.row]
                   self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
        }
        func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
            if let cell = collectionView.cellForItem(at: indexPath) as? HomeCollectionViewCell {
                cell.backgroundColor = UIColor.clear
                cell.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
                
            }
        }
}
