
//
//  BideTableCell.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/28/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import Cosmos

class BideTableCell: UITableViewCell {
    
    var agreeOffer : (()->())?
    var refuseOffer : (()->())?
    
    @IBOutlet weak var noOfOffers: UILabel!
    @IBOutlet weak var noOfRates: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var delivryTime: UILabel!
    @IBOutlet weak var delivryCost: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var bideImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        bideImage.clipsToBounds = true
        noOfOffers.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func acceptBide(_ sender: Any) {
        agreeOffer?()
    }
    @IBAction func refuseBide(_ sender: Any) {
        refuseOffer?()
    }
}
