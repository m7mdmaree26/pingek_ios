//
//  ViewDesign.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/24/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import Foundation
import UIKit
func viewDesign(view:UIView){
    view.layer.shadowColor = UIColor.gray.cgColor
    view.layer.shadowOpacity = 1
    view.layer.shadowOffset = CGSize.zero
    view.layer.shadowRadius = 5
}

func ButtonDesign(Btn:UIButton){
    Btn.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    Btn.layer.borderWidth = 1
    Btn.layer.shadowColor = UIColor.lightGray.cgColor
    Btn.layer.shadowOffset = CGSize(width: 3, height: 3)
    Btn.layer.shadowRadius = 5
    Btn.layer.shadowOpacity = 1.0
    
}
func EditTextDesign(text:UITextField){
    text.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
    text.layer.borderWidth = 1
}
func cellshadow(cell:UITableViewCell){
    cell.layer.applySketchShadow(color: UIColor.darkGray, alpha: 0.3, x: 0, y: 0, blur: 4, spread: 0)
    cell.layer.cornerRadius = 10
    cell.layer.borderWidth = 0.1
    cell.layer.borderColor = UIColor.lightGray.cgColor
    
    cell.layer.backgroundColor = UIColor.white.cgColor
    cell.layer.shadowColor = UIColor.gray.cgColor
    cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)//CGSizeMake(0, 2.0);
    cell.layer.shadowRadius = 2.0
    cell.layer.shadowOpacity = 1.0
    cell.layer.masksToBounds = false
}
