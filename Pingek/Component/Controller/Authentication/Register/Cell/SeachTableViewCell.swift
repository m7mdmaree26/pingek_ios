//
//  SeachTableViewCell.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 1/7/19.
//  Copyright © 2019 Sara Ashraf. All rights reserved.
//

import UIKit

class SeachTableViewCell: UITableViewCell {
    @IBOutlet weak var placeAddress: UILabel!
    @IBOutlet weak var placeName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
