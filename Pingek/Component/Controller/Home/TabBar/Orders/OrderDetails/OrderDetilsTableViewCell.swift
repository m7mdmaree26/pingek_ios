//
//  OrderDetilsTableViewCell.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/10/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit

class OrderDetilsTableViewCell: UITableViewCell {
    @IBOutlet weak var massgeLbl: UILabel!
    @IBOutlet weak var massgeImage: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var continerView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.massgeLbl.layer.masksToBounds = true
        self.massgeLbl.layer.cornerRadius = 5
        self.massgeImage.layer.cornerRadius = 5
        self.massgeImage.clipsToBounds = true
        self.userImage.layer.borderColor = UIColor.lightGray.withAlphaComponent(30).cgColor
        self.userImage.layer.borderWidth = 1
        self.continerView.layer.cornerRadius = 5
        
    }

  
}
