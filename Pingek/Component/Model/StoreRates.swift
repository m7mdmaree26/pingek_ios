//
//  StoreRates.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/22/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import Foundation


class StoreRates{
    var id = ""
    var name = ""
    var avatar = ""
    var rate = ""
    var comment = ""
    var date = ""
    var is_liked = ""
    var num_dislike = ""
    var num_like = ""
    var action = ""

    func getObject(dicc: [String: Any]) -> StoreRates {
        let dic = HandleJSON.getObject().handle(dicc: dicc)
        let model = StoreRates()
        model.id = dic["id"] as! String
        model.name = dic["name"] as! String
        model.avatar = dic["avatar"] as! String
        model.rate = dic["rate"] as! String
        model.comment = dic["comment"] as! String
        model.date = dic["date"] as! String
        model.is_liked = dic["is_liked"] as! String
        model.num_dislike = dic["num_dislike"] as! String
        model.num_like = dic["num_like"] as! String
        model.action = dic["action"] as! String

        return model
        
    }
}
