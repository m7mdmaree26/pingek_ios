//
//  ImagePreviewViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 2/26/19.
//  Copyright © 2019 Sara Ashraf. All rights reserved.
//

import UIKit
import ImageSlideshow
class ImagePreviewViewController: UIViewController {
    @IBOutlet weak var imageSlider: ImageSlideshow!
    @IBOutlet weak var shareBtn: UIButton!
    let imageSlide = UIImageView()
    @IBAction func shareAction(_ sender: Any) {
        self.dataToShare = []
        for i in images{
            self.imageSlide.setImageWith(i)
            self.dataToShare.append(self.imageSlide.image!)
            print("🍋\(i)")
        }
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: dataToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBOutlet weak var back: UIButton!
    var kingfisherArray = [KingfisherSource]()
    var images = [String]()
    var dataToShare = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ImageSliderSetting()
        self.tabBarController?.tabBar.isHidden = true
        self.shareBtn.setImage(#imageLiteral(resourceName: "share"), for: .normal)
        for dic in images{
            let img = KingfisherSource(urlString: dic)
            kingfisherArray.append(img!)
        }
        self.imageSlider.setImageInputs(kingfisherArray)
        
        if(getServerLang() == "ar"){
            self.back.setImage(#imageLiteral(resourceName: "right"), for: .normal)
        }else{
            self.back.setImage(#imageLiteral(resourceName: "left"), for: .normal)
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func ImageSliderSetting(){
        
        imageSlider.slideshowInterval = 5.0
        imageSlider.contentScaleMode = .scaleToFill
        imageSlider.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
        let pageContrller = UIPageControl()
        pageContrller.currentPageIndicatorTintColor = #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
        pageContrller.pageIndicatorTintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        imageSlider.pageIndicator = pageContrller
        imageSlider.activityIndicator = DefaultActivityIndicator()
        imageSlider.currentPageChanged = { page in
            print("current page:", page)
        }
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
        imageSlider.addGestureRecognizer(recognizer)
        
    }
    @objc func didTap() {
        let fullScreenController = imageSlider.presentFullScreenController(from: self)
        // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
    
}
