//
//  UIButtonExtention.swift
//  Noon
//
//  Created by Engy Bakr on 12/25/19.
//  Copyright © 2019 Engy Bakr. All rights reserved.
//

import Foundation
import UIKit

extension UIButton{
    /// Change Sepicif words in sentence
    ///
    /// - Parameters:
    ///   - fullText: write full text
    ///   - changeText: write text you want to change
    ///   - color: add color for changed text
    ///   - font: add font for changed text
    public func halfTextColorChange(fullText : String , changeText : String, color: UIColor,fullColor:UIColor, font: UIFont) {
        let strNumber: NSString = fullText as NSString
        let range = (strNumber).range(of: changeText)
        let attribute = NSMutableAttributedString.init(string: fullText)
        attribute.addAttribute(.foregroundColor, value: fullColor, range: (strNumber).range(of: fullText))
        attribute.addAttribute(.foregroundColor, value: color, range: range)
        attribute.addAttribute(.font, value: font, range: range)
        self.setAttributedTitle(attribute, for: .normal)
//        self.attributedText = attribute
    }
    
    
}

@IBDesignable
extension UIButton {
    
    @IBInspectable var localizedText: String? {
        get {
            return currentTitle
        }
        set {
            setTitle(newValue?.localized, for: .normal)
        }
    }
    
    var localizedFont: FontTypesWithSize {
        set {
            titleLabel?.font = UIFont.getLocalizedFont(type: newValue)
        }get {
            return .regular(16)
        }
    }
    
}
