//
//  SettingViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/26/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController  ,UITableViewDataSource ,UITableViewDelegate{
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    
    var settingTitels = ["Complaints".localized(),"Edit Profile".localized(),"Recharge balance".localized(),"Language settings".localized(),"Terms of use".localized(),"Privacy policy".localized(),"Rate Application".localized(),"Contact us".localized()]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.title = ""
        configureMenuTableView()
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.tableFooterView = UIView()
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)


    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true


    }
    /////////////////==================>>>>>>> TableView((SecondView))
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
          return self.settingTitels.count
    }
    func configureMenuTableView(){
        tableView.register(UINib(nibName: "SettingCellTableViewCell", bundle: nil), forCellReuseIdentifier: "SettingCellTableViewCell")
        tableView.animateTable()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCellTableViewCell", for: indexPath) as! SettingCellTableViewCell
        cell.settingTitle.text = self.settingTitels[indexPath.row]
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        if(getServerLang() == "ar"){
               cell.settingTitle.textAlignment = .right
        }else{
            cell.settingTitle.textAlignment = .left

        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let Reg = Storyboard.Home.viewController(ComplimntsListViewController.self)
            self.navigationController?.pushViewController(Reg, animated: false)
        case 1:
            let Reg = Storyboard.Home.viewController(EditProfileViewController.self)
            self.navigationController?.pushViewController(Reg, animated: false)
        case 2 :
            guard let url = URL(string:"http://Pingek.4hoste.com/payment") else { return }
            UIApplication.shared.open(url)
        case 3:
            let Reg = Storyboard.Home.viewController(LanguageSettingViewController.self)
            self.navigationController?.pushViewController(Reg, animated: false)
        case 4 :
            let Reg = Storyboard.Home.viewController(TermsViewController.self)
            self.navigationController?.pushViewController(Reg, animated: false)
        case 5:
            let Reg = Storyboard.Home.viewController(PrivecyViewController.self)
            self.navigationController?.pushViewController(Reg, animated: false)
            
        case 6:
            //==============open app in store
            print("Rate app")
            let url  = URL(string: "itms://itunes.apple.com/de/app/x-gift/id1498975481?mt=8&uo=4")
            if UIApplication.shared.canOpenURL(url!) {
                UIApplication.shared.open(url!)
            }
        case 7:
            let Reg = Storyboard.Home.viewController(ComplaintsViewController.self)
            self.navigationController?.pushViewController(Reg, animated: false)

        default:
             print("defult")
        }
    }
 
    

}
