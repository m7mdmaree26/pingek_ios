//
//  RestruntsModel.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/3/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import Foundation
struct RestruntsModel{
    var place_id:String
    var place_name:String
    var lat:Double
    var long:Double
    var icon:String
    var distance:Double
}
