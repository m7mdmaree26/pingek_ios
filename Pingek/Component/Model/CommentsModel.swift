//
//  CommentsModel.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/3/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import Foundation
struct CommentsModel{
    var name:String
    var avatar:String
    var rate:Double
    var comment:String
    var date:String
}
