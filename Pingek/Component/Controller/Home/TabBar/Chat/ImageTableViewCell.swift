//
//  ImageTableViewCell.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/20/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell {
    @IBOutlet weak var ResiverWidth: NSLayoutConstraint!
    @IBOutlet weak var senderwidth: NSLayoutConstraint!
    @IBOutlet weak var Loder: UIActivityIndicatorView!
    @IBOutlet weak var MassgeImage: UIImageView!
    @IBOutlet weak var UserResveImage: UIImageView!
    @IBOutlet weak var UserSenderImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.Loder.startAnimating()

    }
    override func prepareForReuse() {
        self.UserSenderImage.image = nil
        self.UserResveImage.image = nil
        self.MassgeImage.image = nil
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

     
    }

    
    
}
