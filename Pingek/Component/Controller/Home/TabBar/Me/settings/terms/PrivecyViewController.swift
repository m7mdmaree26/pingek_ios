//
//  PrivecyViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/3/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class PrivecyViewController: UIViewController {

    @IBOutlet weak var termsTextView: UITextView!
    @IBOutlet weak var backButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.topItem?.title = ""
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)


    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name:  "JFFlat-Medium", size: 15)!]
        self.showProgress()
        getTerms()
    }
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func getTerms(){
        API.POST(url: Privecy_URL, parameters: [:], headers: ["lang":getServerLang()]) { (succss, value) in
            if succss{
                self.stopAnimating()
                let data = value["data"] as! [String:Any]
                let privacy = data["privacy"] as! String
                self.termsTextView.text = privacy
            }else{
                self.stopAnimating()
                
            }
        }
    }
    
}
