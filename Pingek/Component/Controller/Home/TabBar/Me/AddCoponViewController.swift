//
//  AddCoponViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 1/12/19.
//  Copyright © 2019 Sara Ashraf. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class AddCoponViewController: UIViewController {
    @IBOutlet weak var coponTextFild: UITextField!
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func doneAction(_ sender: Any) {
        if(self.coponTextFild.text?.isEmpty == true){
             ShowErrorMassge(massge: "Please complete your Data".localized(), title: "Error".localized())
        }else{
            self.showProgress()
            self.addCopon(coupon: self.coponTextFild.text!)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        coponTextFild.setLeftPaddingPoints(20)
        coponTextFild.setRightPaddingPoints(20)

    }
    func addCopon(coupon:String){
        let parmas:Parameters = [
            "coupon":coupon
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        API.POST(url: AddCoupon_URL, parameters: parmas, headers: header) { (succss, value) in
            if succss{
                self.stopAnimating()
                let key = value["key"] as! String
                let msg = value["msg"] as! String
                print("🌯\(key)")
                if(key == "fail"){
                     ShowErrorMassge(massge: msg, title: "Error".localized())
                }else{
                    ShowTrueMassge(massge: msg, title: "Done".localized())

                }
                
               
            }
        }
    }


}
