//
//  CustomrComentsViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/26/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class CustomrComentsViewController: UIViewController ,UITableViewDataSource ,UITableViewDelegate{
    
    @IBOutlet weak var emptyLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    var CommentsDataSourse = [CommentsModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        configureMenuTableView()
        self.tableView.refreshControl = refreshControl
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.showsVerticalScrollIndicator = false
        self.emptyLbl.isHidden = true
        tableView.tableFooterView = UIView()
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)

    }
    override func viewWillAppear(_ animated: Bool) {
        self.showProgress()
        getUserComments()
        self.tabBarController?.tabBar.isHidden = true
    }
    @IBAction func goback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /////////////////==================>>>>>>> TableView((SecondView))
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.CommentsDataSourse.count
    }
    func configureMenuTableView(){
        tableView.register(UINib(nibName: "CustomCommentsTableViewCell", bundle: nil), forCellReuseIdentifier: "CustomCommentsTableViewCell")
        tableView.animateTable()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCommentsTableViewCell", for: indexPath) as! CustomCommentsTableViewCell
        let item = self.CommentsDataSourse[indexPath.row]
        cell.configureCell(comment: item)
        return cell
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.BasicColor
        
        return refreshControl
    }()
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        getUserComments()
        self.tableView.animateTable()
        refreshControl.endRefreshing()
    }

    func getUserComments(){
        let params:Parameters = [
            "profile_id":getUserID()
        ]
        API.POST(url: profileComments_URL, parameters: params, headers: ["Authorization": "Bearer" + getTokenId()]) { (success, value) in
            if success{
                self.stopAnimating()
                let key = value["key"] as! String
                if(key == "success"){
                 let data = value["data"] as! [[String:Any]]
                     self.CommentsDataSourse = []
                    for d in data{
                    let name = d["name"] as! String
                    let avatar = d["avatar"] as! String
                    let rate = d["rate"] as! Double
                    let comment = d["comment"] as! String
                    let date = d["date"] as! String
                    self.CommentsDataSourse.append(CommentsModel(name: name, avatar: avatar, rate: rate, comment: comment, date: date))
                    }
                    if self.CommentsDataSourse.count > 0{
                        self.emptyLbl.isHidden = true
                    }else{
                        self.emptyLbl.isHidden = false
                    }
                    self.tableView.animateTable()
                    
                }else{
                    self.stopAnimating()
                    let alert = UIAlertController(title: "Connection Error".localized, message: "Check Your Internet Connection Then Try Again".localized, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Try Again".localized, style: .default, handler: { (_) in

                    }))
                    alert.addAction(UIAlertAction(title: "Dismiss".localized, style: .default, handler: { (_) in
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
              
                
            }else{
                self.stopAnimating()
            }
        }
    }
    

}
