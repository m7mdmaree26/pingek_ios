//
//  SavePlaceVC.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/25/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import Alamofire

class SavePlaceVC: UIViewController {

    var store = NearStoreModel()
    var lat:Double?
    var lng:Double?
    var address = ""
    var vc = MakeOrderSecondPageVc()
    @IBOutlet weak var placeName: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        placeName.setLeftPaddingPoints(10)
        placeName.setRightPaddingPoints(30)

        // Do any additional setup after loading the view.
    }

    @IBAction func notNow(_ sender: Any) {
        self.vc.favPlaceButton.setImage(#imageLiteral(resourceName: "favorite"), for: .normal)
        self.dismiss(animated: true)
    }
    @IBAction func savePlace(_ sender: Any) {
        if self.placeName.text?.count != 0 {
            setPlaceToFav()

        }else{
            ShowErrorMassge(massge: "Please Enter address name".localized, title: "Error".localized)

        }
    }
    
    func setPlaceToFav(){
        self.showProgress()
        let params:Parameters = [
            "name":self.placeName.text!,
            "address":self.address,
            "lat":self.lat!,
            "long":self.lng!
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
            
        API.POST(url: savePlace_URL, parameters: params, headers: header) { (succss, value) in
            if succss{
                self.stopAnimating()
                let key = value["value"] as! String
                if key == "1" {
                    ShowTrueMassge(massge: value["msg"] as! String, title: "Success".localized())
                    let data = value["data"] as! [[String:Any]]
                    self.vc.savedPlaces.removeAll()
                    for item in data {
                        self.vc.savedPlaces.append(SavedPlace().getObject(dicc:item))
                    }
                    self.dismiss(animated: true)
                }else{
                    ShowErrorMassge(massge: value["msg"] as! String, title: "Error".localized)

                }
            }else{
                
                self.stopAnimating()
                ShowErrorMassge(massge: "Something went wrong and tried again".localized, title: "Error".localized)
            }
        }
    }
    
}
