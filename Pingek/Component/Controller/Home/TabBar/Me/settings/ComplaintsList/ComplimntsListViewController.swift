//
//  ComplimntsListViewController.swift
//  Ri7nak
//
//  Created by Sara Ashraf on 1/21/20.
//  Copyright © 2020 Sara Ashraf. All rights reserved.
//

import UIKit

class ComplimntsListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noOrderImage: UIImageView!
    @IBOutlet weak var backButton: UIButton!

    var ComplimntsDataSource = [ComplimentList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.noOrderImage.isHidden = true
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)

    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.showProgress()
        getComplimntsList()
    }
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func getComplimntsList(){
        API.POST(url: userTickets_URL, parameters: [:], headers: ["lang":AppLanguage.getLang(),"Authorization":"Bearer" + getTokenId()], completion: { (success, value) in
            if success{
                self.ComplimntsDataSource = []
                self.stopAnimating()
                let code = value["code"] as!  Int
                if(code == 200){
                    let data = value["data"] as! [[String:Any]]
                    for i in data{
              
                        self.ComplimntsDataSource.append(ComplimentList().getObject(dicc: i))
                    }
                    if(self.ComplimntsDataSource.count == 0){
                        self.noOrderImage.isHidden = false

                    }else{
                        self.noOrderImage.isHidden = true

                    }
                                    
                    self.tableView.animateTable()
                }
            }
        })
    }

}

extension ComplimntsListViewController:  UITableViewDataSource ,UITableViewDelegate{
      func numberOfSections(in tableView: UITableView) -> Int {
        return ComplimntsDataSource.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    func configurTableView(){
        tableView.register(UINib(nibName: "insideTableViewCell", bundle: nil), forCellReuseIdentifier: "insideTableViewCell")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = Storyboard.Home.viewController(ComplimntsDetilsViewController.self)
        vc.ticket_id = ComplimntsDataSource[indexPath.section].id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ComplimntsCellTableViewCell", for: indexPath) as! ComplimntsCellTableViewCell
        
        cell.placeName.text! = self.ComplimntsDataSource[indexPath.section].store_name
        cell.discription.text! = self.ComplimntsDataSource[indexPath.section].subject
        cell.Ordernumber.text! = "Order number : ".localized + String(describing: self.ComplimntsDataSource[indexPath.section].order_id)

        if(self.ComplimntsDataSource[indexPath.section].status == "finished"){
            cell.ComplimntState.setTitle("Processed >".localized, for: .normal)
        }else{
            cell.ComplimntState.setTitle("Processing".localized, for: .normal)

        }
    
        
        cell.compImage.layer.borderWidth = 1.5
        cell.compImage.layer.borderColor = UIColor.lightGray.withAlphaComponent(30).cgColor
        cell.compImage.setImageWith(ComplimntsDataSource[indexPath.section].store_icon)
        cell.compImage.layer.cornerRadius = 25
        cell.compImage.layer.masksToBounds = true
        cell.compImage.clipsToBounds = true
        cell.layer.applySketchShadow()
        cell.layer.cornerRadius = 10
        cell.layer.shadowColor = UIColor.lightGray.withAlphaComponent(30).cgColor
        cell.layer.shadowRadius = 10
        cell.layer.shadowOpacity = 1
        cell.layer.shadowOffset.height = 1
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 115
    }
       
    
}

class ComplimntsCellTableViewCell: UITableViewCell {
    @IBOutlet weak var compImage: UIImageView!
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var discription: UILabel!
    @IBOutlet weak var Ordernumber: UILabel!
    @IBOutlet weak var ComplimntState: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       // viewDesign(view: self.viewContiner)
       
        
    }
    
   
}
