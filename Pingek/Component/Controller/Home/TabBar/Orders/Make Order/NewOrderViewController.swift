//
//  NewOrderViewController.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/23/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import OpalImagePicker

class NewOrderViewController: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var storeImage: UIImageView!
    @IBOutlet weak var storeName: UILabel!
    
    @IBOutlet weak var sentOrderButton: UIButton!
    @IBOutlet weak var imageCollection: UICollectionView!
    @IBOutlet weak var orderDetails: UITextView!
    
    var store = NearStoreModel()
    var imagesArray:[UIImage] = []
    var imagesKeys = [String]()
    var imagesData = [Data]()
    var flagImage = false
    
    


    override func viewDidLoad() {
        super.viewDidLoad()
        setupview()

    }
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func addImage(_ sender: Any) {
        let imagePicker = OpalImagePickerController()
        imagePicker.imagePickerDelegate = self
        imagePicker.maximumSelectionsAllowed = 5
        imagePicker.selectionTintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.5706335616)
        imagePicker.selectionImage = #imageLiteral(resourceName: "checked")
        present(imagePicker, animated: true, completion: nil)
    }
    @IBAction func goToNextPage(_ sender: Any) {
        if self.orderDetails.text.count == 0 || self.imagesArray.count == 0 {
            self.sentOrderButton.shake(duration: 0.5, values: [-12.0, 12.0, -12.0, 12.0, -6.0, 6.0, -3.0, 3.0, 0.0])
            ShowErrorMassge(massge: "Please complete your Data".localized, title: "Error".localized)
        }else{
            for i in imagesArray{
                self.imagesData.append((i).pngData()! as Data)
                  print("😆\(i)")
                imagesKeys.append("images[]")
            }
            let vc = Storyboard.Home.viewController(MakeOrderSecondPageVc.self)
            vc.store = self.store
            vc.imagesData = self.imagesData
            vc.imagesKeys = self.imagesKeys
            vc.orderDetails = self.orderDetails.text
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func setupview(){
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)
        self.storeName.text = store.name
        self.storeImage.setImageWith(store.icon)
    }
}


