//
//  KelomtrModel.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/28/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import Foundation
class KelomtrModel: NSObject {
    var text = ""
  
    
    func getObject(dicc: [String: Any]) -> KelomtrModel {
        let dic = HandleJSON.getObject().handle(dicc: dicc)
        let model = KelomtrModel()
        model.text = dic["text"] as! String
        
 return model
    }
}
