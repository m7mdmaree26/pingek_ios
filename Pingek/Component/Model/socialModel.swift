//
//  socialModel.swift
//  Sandkom
//
//  Created by Engy Bakr on 2/5/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import Foundation
class  SocialModel{
    var name = ""
    var link = ""
    var logo = ""

    func getObject(dicc: [String: Any]) ->  SocialModel {
        let dic = HandleJSON.getObject().handle(dicc: dicc)
        let model =  SocialModel()
        model.name = dic["name"] as! String
        model.link = dic["link"] as! String
        model.logo = dic["logo"] as! String


        return model
        
    }
}
