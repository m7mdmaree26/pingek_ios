//
//  MassgeModel.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/17/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import Foundation
struct MassgeModel{
    var user_id:Int
    var username:String
    var content:String
    var type:String
    var date:String
    var avatar:String
    var sender:String
    var lat:Double
    var long:Double
}
