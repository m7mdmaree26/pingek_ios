//
//  HomeVCService.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/21/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import Foundation
import Alamofire


extension HomeVC {
    
    func logOut(){
           let header:HTTPHeaders = [
               "Authorization":"Bearer" + getTokenId(),
               "lang":getServerLang()
           ]
           API.POST(url: LogOut_URL, parameters: ["device_id":AppDelegate.FCMTOKEN], headers: header) { (succss, value) in
               if succss{
                   self.stopAnimating()
                   let key = value["key"] as? String
                   if(key == "success"){
                       let msg = value["msg"] as? String
                       ShowInformationMassge(massge: "You have been blocked  from the administration".localized(), title: "")
                       DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                           SocketConnection.sharedInstance.socket.disconnect()
                           SocketConnection.sharedInstance.manager.disconnect()
                           defaults.set("no", forKey: is_LoogedIn)
                           let Reg = Storyboard.Authentication.viewController(LoginViewController.self)
                           self.navigationController?.pushViewController(Reg, animated: true)
                       }
                   }
                   
               }else{
                   self.stopAnimating()
                   //   print("🍉")
               }
           }
       }

    func getUnseenNotfication(){
       let header:HTTPHeaders = [
           "Authorization":"Bearer" + getTokenId(),
           "lang":getServerLang()
       ]
       API.POST(url: UnSeenNotication_URL, parameters: [:], headers: header) { (sucss, value) in
           if sucss {
               let data = value["data"] as! [String:Any]
               let num_notifications = data["num_notifications"] as! Int
               print("😋\(num_notifications)")
               if(num_notifications == 0){
                   print("😋\(value)")
                   
               }else{
                   self.tabBarController?.tabBar.items![3].badgeValue = String(describing: num_notifications)
               }
               
           }
       }
   }
   func getCheackExpictedPrice(){
       let header:HTTPHeaders = [
           "Authorization":"Bearer" + getTokenId(),
           "lang":getServerLang()
       ]
       API.POST(url: CheckUseExpectedPrice_URL, parameters: [:], headers: header) { (sucss, value) in
           if sucss {
               let data = value["data"] as! [String:Any]
               let allow_km_price = data["allow_km_price"] as! Int
               defaults.set(allow_km_price, forKey: AllowkmPrice)
               print("⚽️\(value)")
           }
       }
   }
   func getHomeData(lat:String,long:String,next_page_token:String,cats:String){
        self.isSearchLastRun = false
       let params:Parameters = [
           "lat":lat,
           "long":long,
           "cats":cats,
           "next_page_token":next_page_token,
       ]
       let header:HTTPHeaders = [
           "Authorization":"Bearer" + getTokenId(),
           "lang":getServerLang()
       ]
        
        var url = ""
        if cats == "" {
            url = Home_URL
        }else{
            url = Nearstores_URL
        }
       
    
       API.POST(url: url, parameters: params, headers: header, withIndicator: false) { (succss, value) in
           if succss{
               self.stopAnimating()
               print("🤭\(value)")
               let key = value["key"] as! String
               if (key == "success"){
                   let data = value["data"] as! [String:Any]
                    var places = [[String:Any]]()
                   //let next_page_token = data["next_page_token"] as! String
                   if let nextToken = data["next_page_token"] as? String{
                       self.next_page_token = nextToken
                       self.isNextToken = true
                   }else{
                        self.isNextToken = false
                        self.next_page_token = ""
                        self.AllNearStores.removeAll()
                   }
                    if url == Home_URL {
                        let spcPlaces = data["specialstores"] as! [[String:Any]]
                        if(spcPlaces.count != 0){
                            for i in spcPlaces {
                                // let id = i["id"] as! Int
                                let name = i["name"] as! String
                                let icon = i["icon"] as! String
                                let vicinity = i["address"] as! String
                                let lat = i["lat"] as! Double
                                let long = i["long"] as! Double
                                let distance = i["distance"] as! String
                                let place_id = String(describing: i["id"] as! Int)
                                let opening_hours = i["opening_hours"] as! [String]
                                self.AllNearStores.append(NearStoreModel(name: name, icon: icon, vicinity: vicinity, lat: lat, long: long, place_id: place_id, opening_hours: opening_hours, distance: distance, storeID: ""))
                                
                            }
                        }
                    }
                    if url == Home_URL {
                         places = data["nearstores"] as! [[String:Any]]
                    }else{
                         places = data["places"] as! [[String:Any]]
                    }

                    if(places.count != 0){
                        for i in places {
                            // let id = i["id"] as! Int
                            let name = i["name"] as! String
                            let icon = i["icon"] as! String
                            let vicinity = i["vicinity"] as! String
                            let reference = i["reference"] as! String
                            let lat = i["lat"] as! Double
                            let long = i["lng"] as! Double
                            let place_id = i["place_id"] as! String
                            let distance = i["distance"] as! String
                            let opening_hours = i["opening_hours"] as! [String]
                            self.AllNearStores.append(NearStoreModel(name: name, icon: icon, vicinity: vicinity, lat: lat, long: long, place_id: place_id, reference: reference, opening_hours: opening_hours, distance: distance, storeID: ""))
                               
                        }
                    }

               }
                self.tableIndcator.stopAnimating()
                self.tableIndcator.hidesWhenStopped = true
                self.tableView.reloadData()


           }else{
               self.stopAnimating()
            
           }
           
           
           
       }
   }

    func getSearchData(lat:String,long:String,next_page_token:String,name:String){
        self.AllNearStores.removeAll()
        self.isSearchLastRun = true
          let params:Parameters = [
              "lat":lat,
              "long":long,
              "next_page_token":next_page_token,
              "name":name
          ]
          let header:HTTPHeaders = [
              "Authorization":"Bearer" + getTokenId(),
              "lang":getServerLang()
          ]
          print("💜\(params)")
          print("💝\(getTokenId())")
          API.POST(url: SearchNearstores_URL, parameters: params, headers: header, withIndicator: false) { (succss, value) in
              if succss{
                  self.stopAnimating()
                  print("🤭\(value)")
                  let key = value["key"] as! String
                  if (key == "success"){
                      let data = value["data"] as! [String:Any]
                      let places = data["places"] as! [[String:Any]]
                      //let next_page_token = data["next_page_token"] as! String
                      if let nextToken = data["next_page_token"] as? String{
                        if nextToken == "" {
                             self.isNextToken = false
                            self.next_page_token = ""
                        }else{
                          self.next_page_token = nextToken
                          self.isNextToken = true
                        }
                      }else{
                           self.isNextToken = false
                           self.next_page_token = ""
                           
                      }
                   if(places.count != 0){
                       for i in places {
                           // let id = i["id"] as! Int
                           let name = i["name"] as! String
                           let icon = i["icon"] as! String
                           let vicinity = i["vicinity"] as! String
                           let reference = i["reference"] as! String
                           let lat = i["lat"] as! Double
                           let long = i["lng"] as! Double
                           let place_id = i["place_id"] as! String
                           let distance = i["distance"] as! String
                           let opening_hours = i["opening_hours"] as! [String]
                           self.AllNearStores.append(NearStoreModel(name: name, icon: icon, vicinity: vicinity, lat: lat, long: long, place_id: place_id, reference: reference, opening_hours: opening_hours, distance: distance, storeID: ""))
                              
                       }
                   }
                      
                  }
                  // self.flagData = "home"
                self.tableIndcator.stopAnimating()
                self.tableIndcator.hidesWhenStopped = true

                self.tableView.reloadData()


              }else{
                  self.stopAnimating()
              }
              
              
              
          }
      }
   func getSpesialCat(lat:String,long:String,next_page_token:String,category_id:String){
       let params:Parameters = [
           "lat":lat,
           "long":long,
           "next_page_token":next_page_token,
           "category_id":category_id
       ]
       let header:HTTPHeaders = [
           "Authorization":"Bearer" + getTokenId(),
           "lang":getServerLang()
       ]
       print("💜\(params)")
       print("💝\(getTokenId())")
       API.POST(url: Specialstores_URL, parameters: params, headers: header, withIndicator: false) { (succss, value) in
           if succss{
               self.stopAnimating()
               print("🤭\(value)")
               let key = value["key"] as! String
               if (key == "success"){
                   //  let data = value["data"] as! [String:Any]
                   let places = value["data"] as! [[String:Any]]
                   
                   if(places.count != 0){
                       for i in places {
                           let id = i["id"] as! Int
                           let name = i["name"] as! String
                           let icon = i["icon"] as! String
                           let vicinity = i["vicinity"] as! String
                           let reference = i["reference"] as! String
                           let lat = i["lat"] as! Double
                           let long = i["lng"] as! Double
                           let place_id = i["place_id"] as! String
                           let distance = i["distance"] as! String
                           let opening_hours = i["opening_hours"] as! [String]
                           self.AllNearStores.append(NearStoreModel(name: name, icon: icon, vicinity: vicinity, lat: lat, long: long, place_id: place_id, reference: reference, opening_hours: opening_hours, distance: distance, storeID: String(id)))
                           
                       }
                   }
                    self.tableIndcator.stopAnimating()
                    self.tableIndcator.hidesWhenStopped = true
                    self.tableView.reloadData()
               }
               
           }else{
               self.stopAnimating()
           }
           
           
           
       }
   }
    
    func updateUserLocal(){
        let Parmas:Parameters = [
            "lat": getUserLat(),
            "long": getUserLong(),
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        API.POST(url: updateUserlocale, parameters:Parmas , headers: header) { (succss, value) in
            if succss{
                print("😆\(Parmas)")
                
            }
        }
        
    }
    
}
