//
//  PaddingRequstTableViewCell.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/25/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit

class PaddingRequstTableViewCell: UITableViewCell {
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var orderName: UILabel!
    @IBOutlet weak var fromSpace: UILabel!
    @IBOutlet weak var toSpace: UILabel!

    @IBOutlet weak var orderDate: UILabel!
    @IBOutlet weak var imageDelvry: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.layer.cornerRadius = 5
        self.shadowColor = UIColor.lightGray.withAlphaComponent(30)
        self.shadowRadius = 5
        self.shadowOpacity = 0.5
        self.shadowOffset.height = 1

    }

    func configureCell(order:wattingOrders){
        self.userName.text = order.client_name
        self.fromSpace.text = "- - - - -" + order.distance_to_store + "- - - - -"
        self.toSpace.text =  "- - - - -" + order.distance_to_client  + "- - - - -"
        self.imageDelvry.setImageWith(order.avatar)
        
        self.orderDate.text = order.date
        if order.details.count != 0 {
            if  order.details[0].key == "text" {
                self.orderName.text = order.details[0].value
            }else{
                self.orderName.text = ""
            }
        }
    }

}
