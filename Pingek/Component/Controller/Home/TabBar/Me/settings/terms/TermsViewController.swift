//
//  TermsViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/26/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TermsViewController: UIViewController {
    @IBOutlet weak var termsTextView: UITextView!
    @IBOutlet weak var backButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = ""
      self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name:  "JFFlat-Medium", size: 15)!]
        self.showProgress()
        getTerms()
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)

    }
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getTerms(){
        API.POST(url: Terms_URL, parameters: [:], headers: ["lang":getServerLang()]) { (succss, value) in
            if succss{
                self.stopAnimating()
                let data = value["data"] as! [String:Any]
                let terms = data["terms"] as! String
                self.termsTextView.text = terms
            }else{
                self.stopAnimating()

            }
        }
    }

}
