//
//  ReadytoDeliverViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/10/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit
import MapKit
import MIAlertController
import SwiftyJSON
import Alamofire
import EzPopup
import GoogleMaps
import SwiftyJSON


class ReadytoDeliverViewController: UIViewController ,GMSMapViewDelegate{

    
    @IBOutlet weak var googleMap: GMSMapView!
    @IBOutlet weak var resiveAddress: UILabel!
    @IBOutlet weak var delvireAddress: UILabel!
    @IBOutlet weak var RestruntName: UILabel!
    @IBOutlet weak var price: UITextField!
    @IBOutlet weak var expictedPriceTextFild: BorderBaddedTextField!
    @IBOutlet weak var expictedPriceMsg: UILabel!
    @IBOutlet weak var totalCostData: UILabel!
    @IBOutlet weak var backButton: UIButton!

    var app_percentage = ""
    var orderId = ""
    var resiveAdd = ""
    var deliverAdd = ""
    var resName = ""
    var distnsfromStore = ""
    var distansfromClint = ""
    var delverTime = ""
    var ExpixtedMsg = ""
    var MaxExpixtedPrice = ""
    var MinExpixtedPrice = ""
    var Resivelat = ""
    var ResiveLong = ""
    var Delgatelat = ""
    var DelgateLng = ""
    let path = GMSMutablePath()
    var customAlertControllerConfig: MIAlertController.Config!
    var ButtoncustomAlertControllerConfig: MIAlertController.Button.Config!
    private var currentCoordinate: CLLocationCoordinate2D?
    private let locationManager = CLLocationManager()
    let myMarker = GMSMarker()
    
    @IBAction func sendOffer(_ sender: Any) {
        if(price.text?.isEmpty ==  true){
            self.price.shake(duration: 0.5, values: [-12.0, 12.0, -12.0, 12.0, -6.0, 6.0, -3.0, 3.0, 0.0])
            ShowErrorMassge(massge: "You must Enter the Offer Price First".localized(), title: "Error".localized())
        }else{
            self.showProgress()
            self.addOffer(order_id: self.orderId, price: self.price.text!)
        }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.resiveAddress.text = self.resiveAdd
        self.delvireAddress.text = self.deliverAdd
        self.price.keyboardType = .asciiCapableNumberPad
        
        self.hideKeyboardWhenTappedAround()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        price.setLeftPaddingPoints(40)
        price.setRightPaddingPoints(10)
        self.price.layer.cornerRadius = 10
        self.expictedPriceTextFild.isHidden = true
        price.addTarget(self, action: #selector(CalaulateTotalCost(_:)), for: .editingChanged)
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)
       self.expictedPriceMsg.text = self.ExpixtedMsg
        
        
        //=========draw line fom delegate to store
        self.googleMap.delegate = self
        let camera = GMSCameraPosition.camera(withLatitude:  Double(getUserLat())!, longitude: Double(getUserLong())!, zoom: 11)
        self.googleMap.camera = camera
        DrawStorAnnotion()
        DrawHomeAnnotion()
        path.add(CLLocationCoordinate2DMake(Double(self.Resivelat)!, Double(self.ResiveLong)!))
        path.add(CLLocationCoordinate2DMake(Double(self.Delgatelat)!, Double(self.DelgateLng)!))
        
        self.DrawLine(SoursLat: Double(Resivelat)!, SourceLng: Double(ResiveLong)!, DistLat: Double(Delgatelat)!, DistLng: Double(DelgateLng)!)
        
        
        

        
    }
    @objc func CalaulateTotalCost(_ TextFeild : UITextField){
        if price.text!.count !=  0 {
            totalCostData.text = String(describing: ((Double(app_percentage)! * Double(price.text!)!) + Double(price.text!)!))
        }
    }
    
    func DrawLine(SoursLat:Double,SourceLng:Double,DistLat:Double,DistLng:Double){

        let origin = "\(SoursLat),\(SourceLng)"
        let destination = "\(DistLat),\(DistLng)"
        
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&key=\(defaults.string(forKey: GOOGLE_KEY)!)"
        
        //Rrequesting Alamofire and SwiftyJSON
        API.GET(url: url) { (succss, value) in
            if succss{
                self.stopAnimating()
                print("🤭\(value)")
                 let routes = value["routes"] as! [[String : Any]]
                 for route in routes
                 {
                     let routeOverviewPolyline = route["overview_polyline"] as! NSDictionary
                    let points = routeOverviewPolyline["points"] as! String
                     let path = GMSPath.init(fromEncodedPath: points)
                     let polyline = GMSPolyline.init(path: path)
                     polyline.strokeColor = UIColor.WarningColor
                     polyline.strokeWidth = 2
                     polyline.map = self.googleMap
                 }
                         
            }else{
                self.stopAnimating()
            }
        }
        
        path.add(CLLocationCoordinate2DMake(Double(self.Delgatelat)!, Double(self.DelgateLng)!))
        let bounds = GMSCoordinateBounds(path: path)
        self.googleMap.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
        
    }
    
    //==============delegate
    func DrawHomeAnnotion(){

        //====================google
        let marker = GMSMarker()
        marker.position =  CLLocationCoordinate2D(latitude: Double(self.Delgatelat)!, longitude: Double(self.DelgateLng)!)
        marker.icon = #imageLiteral(resourceName: "vector")
        marker.map = googleMap
        
    }
    
    //==============store
    func DrawStorAnnotion(){
        //====================google
        let marker = GMSMarker()
        marker.position =  CLLocationCoordinate2D(latitude: Double(self.Resivelat)!, longitude: Double(self.ResiveLong)!)
        marker.icon = #imageLiteral(resourceName: "vectorstore")
        marker.map = googleMap
        
        
    }

    ///////============>Configer MapView
    private func configureLocationServices() {
        locationManager.delegate = self
        let status = CLLocationManager.authorizationStatus()
        
        if status == .notDetermined {
            locationManager.requestAlwaysAuthorization()
        } else if status == .authorizedAlways || status == .authorizedWhenInUse {
            beginLocationUpdates(locationManager: locationManager)
        }
    }
    
    private func beginLocationUpdates(locationManager: CLLocationManager) {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
    
    
    func addOffer(order_id:String,price:String){
        let params:Parameters = [
            "order_id":order_id,
            "price":price,
            "lat":getUserLat(),
            "long":getUserLong()
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        API.POST(url: applyID_URL, parameters: params, headers: header) { (succss, value) in
            if succss{
                self.stopAnimating()
                let msg = value["msg"] as! String
                let key = value["key"] as! String
                if(key == "success"){
                    print("shooowAllllert🍉")
                    let vc = Storyboard.Home.viewController(SentOrderAlertVC.self)
                    let popupVC = PopupViewController(contentController: vc, popupWidth: UIScreen.main.bounds.width-80, popupHeight: 200)
                                
                    popupVC.backgroundAlpha = 0.5
                    popupVC.backgroundColor = .black
                    popupVC.canTapOutsideToDismiss = true
                    popupVC.cornerRadius = 10
                    popupVC.shadowEnabled = true
                                
                                
                    self.present(popupVC, animated: true)
                    
                }else{
                    ShowErrorMassge(massge: msg, title: "Error".localized())
                }
            }else{
                self.stopAnimating()

            }
        }
    }
}

extension ReadytoDeliverViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //==============draw myLoc
       
        myMarker.position =  CLLocationCoordinate2D(latitude:manager.location!.coordinate.latitude, longitude: manager.location!.coordinate.longitude)
        myMarker.icon = #imageLiteral(resourceName: "vectorperson")
        myMarker.map = googleMap
        
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        path.add(CLLocationCoordinate2DMake(locValue.latitude, locValue.longitude))

        
        //=========draw line fom store to client
        self.DrawLine(SoursLat: locValue.latitude, SourceLng: locValue.longitude, DistLat: Double(Resivelat)! , DistLng: Double(ResiveLong)!)

        guard locations.first != nil else { return }
        manager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("The status changed")
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            beginLocationUpdates(locationManager: manager)
        }
    }
    
}

