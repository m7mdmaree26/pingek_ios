//
//  WeekdayTextModel.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/28/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import Foundation
class WeekdayTextModel: NSObject {
    var vicinity = ""
    var name = ""
    var icon = ""
    var id = ""
    var distance = ""
    var geometry = geometryModel()
    var opening_hours = OpeningHoursModel()
    
    func getObject(dicc: [String: Any]) -> PlaceModel {
        let dic = HandleJSON.getObject().handle(dicc: dicc)
        let model = PlaceModel()
        model.id = dic["id"] as! String
        model.name = dic["name"] as! String
        model.icon = dic["icon"] as! String
        model.vicinity = dic["vicinity"] as! String
        
        model.geometry = geometryModel().getObject(dicc: dic["geometry"] as! [String:Any])
        model.opening_hours = OpeningHoursModel().getObject(dicc: dic["opening_hours"] as! [String:Any])
        
        
        return model
    }
}
