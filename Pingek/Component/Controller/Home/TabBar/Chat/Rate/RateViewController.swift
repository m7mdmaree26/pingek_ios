//
//  RateViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/30/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit
import Cosmos
import KMPlaceholderTextView
import SwiftyJSON
import Alamofire
import EzPopup

class RateViewController: UIViewController {
    
    var uerAvatar = ""
    var Name = ""
    var RateValu = 0.0
    var ProviderID = ""
    var isRateStore = false
    var vc = ChatViewController()
    
    
    @IBOutlet weak var rate: RoundedButton!
    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var ratingBar: CosmosView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var commentTextView: KMPlaceholderTextView!
    
    
    
    @IBAction func rateAction(_ sender: Any) {
        if(self.commentTextView.text.isEmpty == true){
            self.rate.shake(duration: 0.5, values: [-12.0, 12.0, -12.0, 12.0, -6.0, 6.0, -3.0, 3.0, 0.0])
            ShowErrorMassge(massge: "Please complete your Data".localized(), title: "Error".localized)
        }else{
            self.showProgress()
            self.RateUser(comment: self.commentTextView.text!, user_id:self.ProviderID , rating: self.ratingBar.rating)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.commentTextView.placeholder = "Comment".localized
        self.commentTextView.layer.cornerRadius = 20
        self.commentTextView.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        self.commentTextView.layer.borderWidth = 1
        self.userName.text = Name
      //  self.ratingBar.rating =  RateValu
        self.userAvatar.setImageWith(uerAvatar)
        self.userAvatar.clipsToBounds = true
        
    }
    func RateUser(comment:String,user_id:String,rating:Double){
        var parmas:Parameters = [
            "rating":rating,
            "comment":comment
        ]
        
        var url = ""
        if isRateStore {
            url = StorRating_URL
            parmas.updateValue(user_id, forKey: "store_id")

        }else{
            url = Rateing_URL
            parmas.updateValue(user_id, forKey: "user_id")
        }
        
        API.POST(url: url, parameters: parmas, headers: ["Authorization":"Bearer" + getTokenId(),"lang":getServerLang()]) { (success, value) in
            if success {
                self.stopAnimating()
                let key = value["key"] as? String
                if key == "success"{
                    let msg = value["msg"] as? String
                    ShowTrueMassge(massge: msg!, title: "Done".localized)
                    self.dismiss(animated: true)
                    if self.isRateStore{
                        guard let window = UIApplication.shared.keyWindow else { return }
                        
                        let vc =  Storyboard.Home.viewController(TabBarViewController.self)
                        window.rootViewController = vc
                    }else{
                        if self.vc.storeId == "0" {
                            guard let window = UIApplication.shared.keyWindow else { return }
                            let vc =  Storyboard.Home.viewController(TabBarViewController.self)
                            window.rootViewController = vc
                        }else{
                            let vc = Storyboard.Home.viewController(RateViewController.self)
                            vc.Name = self.vc.placeName
                            vc.ProviderID = self.vc.storeId
                            vc.uerAvatar = self.vc.storeIcon
                            vc.isRateStore = true
                            let popupVC = PopupViewController(contentController: vc, popupWidth: UIScreen.main.bounds.width-60, popupHeight: 340)
                                                               
                            popupVC.backgroundAlpha = 0.5
                            popupVC.backgroundColor = .black
                            popupVC.canTapOutsideToDismiss = false
                            popupVC.cornerRadius = 20
                            popupVC.shadowEnabled = true
                                                                   
                                                                   
                            self.vc.present(popupVC, animated: true)
                        }
                    }
                }else{
                    let msg = value["msg"] as? String
                    ShowErrorMassge(massge: msg!, title: "Done".localized)
                }
                
            }else{
                self.stopAnimating()
                
            }
            
        }
    }
}
