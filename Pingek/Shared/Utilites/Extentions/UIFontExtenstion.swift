//
//  UIFontExtenstion.swift
//  TaxiAwamerCaptain
//
//   Created by Sara Ashraf on 11/18/19.
//  Copyright © 2019 aait. All rights reserved.
//

import UIKit

extension UIFont {
    
    static func getLocalizedFont(type: FontTypesWithSize) -> UIFont {
        switch type {

        case .regular(let value):
            return JfflatFont.regular.font(for: value)
        case .light(let value):
            return JfflatFont.light.font(for: value)
        case .semiBold(let value):
            return JfflatFont.semiBold.font(for: value)
        case .bold(let value):
           return JfflatFont.semiBold.font(for: value)
        }
    }
}

enum FontTypesWithSize {
    case regular(CGFloat), light(CGFloat), semiBold(CGFloat), bold(CGFloat)
}
