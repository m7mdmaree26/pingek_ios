
//
//  MyOrdersTableViewCell.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/25/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit

class MyOrdersTableViewCell: UITableViewCell {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var orderDetils: UILabel!
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var orderNumber: UILabel!
    @IBOutlet weak var inDelivryLbl: UILabel!
    @IBOutlet weak var watingStack: UIStackView!
    @IBOutlet weak var finishedStack: UIStackView!
    @IBOutlet weak var delegateStack: UIStackView!
    @IBOutlet weak var orderPrice: UILabel!
    @IBOutlet weak var orderTime: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       // self.selectionStyle = .none
        self.layer.cornerRadius = 5
        self.shadowColor = UIColor.lightGray.withAlphaComponent(0.5)
        self.shadowOpacity = 1
        userImage.clipsToBounds = true
        inDelivryLbl.isHidden = true
        watingStack.isHidden = true
        finishedStack.isHidden = true
        delegateStack.isHidden = true

        // Initialization code
    }
    
    func configureCell(UserOrder:UserOrdersModel){
        self.orderNumber.text =  UserOrder.id
        self.userImage.setImageWith(UserOrder.place_icon)
        self.orderPrice.text = "Delivary cost".localized + " " + UserOrder.price + "SAR".localized
        self.orderTime.text = "Arrived through".localized + " " + UserOrder.deliver_time
        self.placeName.text = UserOrder.place_name
        for i in UserOrder.details{
            if i.key == "text" {
                self.orderDetils.text = i.value
            }
        }
    }

    
   
}
