//
//  SearchDataModel.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 1/7/19.
//  Copyright © 2019 Sara Ashraf. All rights reserved.
//

import Foundation
struct SearchDataModel{
    var lat:Double
    var long:Double
    var placeName :String
    var placAddress:String
    
}
