//
//  HomeVC.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/16/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation
import GooglePlaces
import GoogleMaps
import SocketIO

class HomeVC: UIViewController , CLLocationManagerDelegate {
    
    
    @IBOutlet weak var searchText: UITextField!
    @IBOutlet weak var tableIndcator: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var ContinerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var storesTableHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var specialOrderCollection: UICollectionView!
    
    
    
    var icon = [#imageLiteral(resourceName: "spisal-2"),#imageLiteral(resourceName: "all"),#imageLiteral(resourceName: "noun_storee-1"),#imageLiteral(resourceName: "noun_restaurant-1"),#imageLiteral(resourceName: "noun_pharmacy-1"),#imageLiteral(resourceName: "caffSelected")]
    var iconName  = ["Special Order".localized,"All".localized,"Stores".localized,"Restaurants".localized,"Pharmacies".localized,"Cafes".localized]
    
    var specialOrdersImages = [#imageLiteral(resourceName: "icon"),#imageLiteral(resourceName: "gasbottle"),#imageLiteral(resourceName: "truck")]
    var specialPrdersTitle = ["Special Order".localized,"Gas delivery".localized,"Water delivery".localized]

    var iconNameAr = [String]()
    var recordsArray:[Int] = Array()
    var DataArray = [PlaceModel]()
    var homeModel = HomeModel()
    var Kelomtr = [String]()
    var dataFlag = false
    var DistanceModele = DistanceModel()
    var AllNearStores = [NearStoreModel]()
    var isNextToken = false
    var isSearchLastRun = false
    var nextPage = ""
    var CurrntType = ""
    var CurrntName = ""
    var next_page_token = ""
    var locationManager: CLLocationManager = CLLocationManager()
    static var UserLat:Double!
    static var UserLong :Double!
    
    
    
    //=====================Will appear
    override func viewWillAppear(_ animated: Bool) {
        self.dataFlag = false
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController!.view.backgroundColor = UIColor.clear
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(),for: .default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "JFFLAT-Medium", size: 16)!,NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)]
//        NotificationCenter.default.addObserver(self, selector: #selector(GetLocationData(notifcation:)), name: .getData, object: nil)

        if(getisDelgate() == "no"){
            self.tabBarController?.tabBar.isHidden = false
            
        }else{
            
            self.tabBarController?.tabBar.isHidden = false
        }
        if(getoIsVistor() == "yes"){
            self.tabBarController?.tabBar.isHidden = true
        }else{
            self.tabBarController?.tabBar.isHidden = false
            getUnseenNotfication()
            //getCheackExpictedPrice()
        }
        if(SocketConnection.sharedInstance.socket.status == .notConnected){
            print("🌝")
            SocketConnection.sharedInstance.manager.connect()
            SocketConnection.sharedInstance.socket.connect()
        }
        if(SocketConnection.sharedInstance.socket.status == .disconnected){
            print("🌝")
            SocketConnection.sharedInstance.manager.connect()
            SocketConnection.sharedInstance.socket.connect()
        }
        print("🌝\(SocketConnection.sharedInstance.socket.status)")
         updateUserLocal()
        
        self.scrollView.delegate = self
        
        self.tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tableView.layer.removeAllAnimations()
        storesTableHeight.constant = self.tableView.contentSize.height
        self.view.updateConstraints()
        self.view.layoutIfNeeded()
        
    }
    //===================didload
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
         getHomeData(lat: getUserLat(), long: getUserLong(), next_page_token: self.next_page_token, cats: self.CurrntType)
    }
    
    
    func setupView(){
        //=========google key======
        if defaults.object(forKey: GOOGLE_KEY) != nil{
            GMSServices.provideAPIKey(defaults.string(forKey: GOOGLE_KEY)!)
        }else{
            GMSServices.provideAPIKey("AIzaSyBPftOQyR7e_2mv9MRu-TeNoW2qaOEK0fw")
        }
        
        searchText.addTarget(self, action: #selector(searchData(_:)), for:.editingChanged)
        searchText.setLeftPaddingPoints(20)
        searchText.setRightPaddingPoints(50)
        self.hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(getNotfy(notifcation:)), name: .FireNotfication, object: nil)
        //==========location
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        //==========table&&Collection
        registerCell()
        configureMenuTableView()
        self.tableView.refreshControl = refreshControl
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableIndcator.stopAnimating()
        tableView.tableFooterView = UIView(frame: .zero)
        self.collectionView.showsHorizontalScrollIndicator = false
        self.tableView.backgroundColor = UIColor.white
       // self.navigationController?.navigationBar.topItem?.title = ""

        self.tableView.showsVerticalScrollIndicator = false
        self.CurrntType = ""
        defaults.set("order", forKey: convirsationFrom)
        defaults.set("home", forKey: nearOrderfrom)
        defaults.set("home", forKey: ViewOrderfrom)
        self.tableView.backgroundColor = UIColor.clear
        
        //=================
        
        if (AppLanguage.getLang().contains("en")){
            collectionView.semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight

        }else{

            collectionView.semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft

        }

        
        let indexPath = self.collectionView.indexPathsForSelectedItems?.last ?? IndexPath(item: 0, section: 0)
        self.collectionView.selectItem(at: indexPath, animated: false, scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)
       
        

    }
    
    @objc func searchData(_ TextField : UITextField){
        getSearchData(lat: getUserLat(), long: getUserLong(), next_page_token: self.next_page_token,name:searchText.text!)
    }
    
    @IBAction func searchResult(_ sender: Any) {
        getSearchData(lat: getUserLat(), long: getUserLong(), next_page_token: self.next_page_token,name:searchText.text!)
    }
    
    
    //==============Notification==============//
    
    @objc func getNotfy(notifcation:Notification){
        self.getUnseenNotfication()
//        print("notficccation🍋🍋🍋\(notifcation))")
//        guard let item = notifcation.object as? NotficationModel else{return}
//        print("notficccation🐳🐳🐳🍋\(item.key))")
//        switch  item.key{
//        case "applyBid":
//            self.tabBarController?.selectedIndex = 3
//            defaults.set("notfy", forKey: ViewOrderfrom)
//            defaults.set(item.id, forKey: GlobalBid)
//        case "agreeBid":
//
//            let Reg = Storyboard.Home.viewController(ChatViewController.self)
//            Reg.ConvrstionId = item.id
//            tabBarController?.selectedViewController?.navigationController?.pushViewController(Reg, animated: false)
//        case "new_message":
//            let Reg = Storyboard.Home.viewController(ChatViewController.self)
//             Reg.ConvrstionId = item.id
//            SocketConnection.sharedInstance.socket.off(clientEvent: .connect)
//                   SocketConnection.sharedInstance.manager.disconnect()
//
////            let navigation = UINavigationController()
////            navigation.navigationController?.pushViewController(Reg, animated: false)
//            tabBarController?.selectedViewController?.navigationController?.pushViewController(Reg, animated: false)
//
//        case "newOrder":
//            defaults.set("notfy", forKey: nearOrderfrom)
//            let Reg = Storyboard.Home.viewController(OrderDetilsViewController.self)
//            Reg.orderId = item.id
//            tabBarController?.selectedViewController?.navigationController!.pushViewController(Reg, animated: false)
//        case "withdrawOrder":
//            tabBarController?.selectedViewController?.navigationController?.popViewController(animated: true)
//            self.tabBarController?.selectedIndex = 3
//        case "answerContact":
//           self.tabBarController?.selectedIndex = 3
//        case "finishOrder":
//            self.tabBarController?.selectedIndex = 3
//        case "block_user":
//            self.logOut()
//        case "activeDelegate":
//            self.tabBarController?.selectedIndex = 3
//        case "delete_user":
//            self.logOut()
//        case"disagreeBid":
//            self.tabBarController?.selectedIndex = 3
//        case "createInvoice":
//            let Reg = Storyboard.Home.viewController(ChatViewController.self)
//            Reg.ConvrstionId = item.id
//            print("☘️\(item.id)")
//            SocketConnection.sharedInstance.socket.off(clientEvent: .connect)
//            SocketConnection.sharedInstance.manager.disconnect()
//        tabBarController?.selectedViewController?.navigationController?.pushViewController(Reg, animated: false)
//
//        default:
//            break
//        }
    }
    
    
   
    
//    @objc func GetLocationData(notifcation:Notification){
//       // self.showProgress()
//
//
//
//    }
    
    func registerCell(){
        collectionView.register(UINib.init(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionViewCell")
        
    }
    
    
    
    
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.BasicColor
        
        return refreshControl
    }()
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {

            if isSearchLastRun{
                getSearchData(lat: getUserLat(), long: getUserLong(), next_page_token: self.next_page_token,name:searchText.text!)
                               
            }else{
                self.showProgress()
                self.getHomeData(lat: getUserLat(), long: getUserLong(), next_page_token: next_page_token, cats: self.CurrntType)
            }
            self.tableView.animateTable()
            refreshControl.endRefreshing()
            
     //   }
        
    }
    

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let lastLocation: CLLocation = locations[locations.count - 1]
        let test = ["user_id": getUserID(), "lat": lastLocation.coordinate.latitude,"lng": lastLocation.coordinate.longitude] as [String : Any]
        SocketConnection.sharedInstance.socket.emit("updatelocation", test)
        HomeVC.UserLat = lastLocation.coordinate.latitude
        HomeVC.UserLong = lastLocation.coordinate.longitude
        defaults.set(lastLocation.coordinate.latitude, forKey: User_Lat)
        defaults.set(lastLocation.coordinate.longitude, forKey: User_Lng)
        
        if(dataFlag == false ){
            NotificationCenter.default.post(name: .getData, object: nil)
            dataFlag = true
        }
    }
    
    
}




extension Notification.Name {
    static let getData = Notification.Name(
        rawValue: "getData")
    static let FireNotfication = Notification.Name(
        rawValue: "notfication")
    static let IssuBill = Notification.Name(
        rawValue: "IssuBill")
    static let reteView = Notification.Name(
    rawValue: "reteView")
}
