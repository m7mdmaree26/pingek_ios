//
//  locationMidel.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/27/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import Foundation
class locationMidel: NSObject {
    var lat = ""
    var lng = ""
    
    
    func getObject(dicc: [String: Any]) -> locationMidel {
        let dic = HandleJSON.getObject().handle(dicc: dicc)
        let model = locationMidel()
        model.lat = dic["lat"] as! String
        model.lng = dic["lng"] as! String
             return model
    }
}


