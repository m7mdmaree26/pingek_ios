


//
//  CommentsTableCell.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/22/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import Cosmos

class CommentsTableCell: UITableViewCell {

    
    var likeAction : (()->())?
    var disLikeAction : (()->())?
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var LikeButton: UIButton!
    @IBOutlet weak var disLikeButton: UIButton!
    @IBOutlet weak var rateView: CosmosView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    @IBAction func dislikeComment(_ sender: Any) {
        disLikeAction?()
    }
    @IBAction func likeComment(_ sender: Any) {
        likeAction?()
    }
}
