//
//  geometryModel.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/27/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import Foundation
class geometryModel: NSObject {
    var location = locationMidel()
  

    
    func getObject(dicc: [String: Any]) -> geometryModel {
        let dic = HandleJSON.getObject().handle(dicc: dicc)
        let model = geometryModel()

        model.location = locationMidel().getObject(dicc: dic["location"] as! [String:Any])
        
        
        return model
    }
}
