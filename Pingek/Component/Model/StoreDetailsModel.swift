//
//  StoreDetailsModel.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/22/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import Foundation
class StoreDetailsModel{
    var id = ""
    var name = ""
    var parent_id = ""
    var phone = ""
    var email = ""
    var icon = ""
    var cover = ""
    var address = ""
    var lat = ""
    var lng = ""
    var rate = ""
    var num_rating = ""
    var num_comments = ""
    var website = ""
    var opening_hours = [String]()
    var distance = ""
    var menus = [String]()

    func getObject(dicc: [String: Any]) -> StoreDetailsModel {
        let dic = HandleJSON.getObject().handle(dicc: dicc)
        let model = StoreDetailsModel()
        model.id = dic["id"] as! String
        model.name = dic["name"] as! String
        model.parent_id = dic["parent_id"] as! String
        model.phone = dic["phone"] as! String
        model.email = dic["email"] as! String
        model.icon = dic["icon"] as! String
        model.cover = dic["cover"] as! String
        model.address = dic["address"] as! String
        model.lat = dic["lat"] as! String
        model.lng = dic["lng"] as! String
        model.rate = dic["rate"] as! String
        model.num_rating = dic["num_rating"] as! String
        model.num_comments = dic["num_comments"] as! String
        model.website = dic["website"] as! String
        model.opening_hours = dic["opening_hours"] as! [String]
        model.distance = dic["distance"] as! String
        model.menus = dic["menus"] as! [String]

        return model
        
    }
}
class BranchesModel{
    var id = ""
    var address = ""
    var lat = ""
    var lng = ""
    var distance = ""


    func getObject(dicc: [String: Any]) -> BranchesModel {
        let dic = HandleJSON.getObject().handle(dicc: dicc)
        let model = BranchesModel()
        model.id = dic["id"] as! String
        model.address = dic["address"] as! String
        model.lat = dic["lat"] as! String
        model.lng = dic["lng"] as! String
        model.distance = dic["distance"] as! String

        return model
        
    }
}
