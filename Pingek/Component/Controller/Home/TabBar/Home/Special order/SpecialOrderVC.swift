//
//  SpecialOrderVC.swift
//  Pingek
//
//  Created by Engy Bakr on 2/10/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import OpalImagePicker
import EzPopup
import Alamofire


class SpecialOrderVC : UIViewController {
    
    var orderType = ""
    var imagesArray:[UIImage] = []
    var imagesKeys = [String]()
    var imagesData = [Data]()
    var flagImage = false
    
    
    //======
    var recLat:Double?
    var recLng:Double?
    var delLat:Double?
    var delLng:Double?
    var minExpictedPrice:Double!
    var MaxExpictedPrice:Double!
    var PrayFlag:Bool?
    let paragraphStyle = NSMutableParagraphStyle()
    var HourArray = [String]()
    var selectedValue = ""
    var savedPlaces = [SavedPlace]()


    @IBOutlet weak var ttitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var imageCollectionHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var orderDurationText: UITextField!
    @IBOutlet weak var delivaryPointText: UITextField!
    @IBOutlet weak var recivingPointText: UITextField!
    @IBOutlet weak var favRecPlaceButton: UIButton!
    @IBOutlet weak var favDelivPlaceButton: UIButton!
    @IBOutlet weak var imageCollection: UICollectionView!
    @IBOutlet weak var orderDetails: UITextView!
    @IBOutlet weak var sentOrderButton: UIButton!

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        getSavedPlaces()

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupview()
    }
    
    func setupview(){
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)
        self.ttitle.text = orderType
        self.imageCollection.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        orderDurationText.setLeftPaddingPoints(30)
        delivaryPointText.setLeftPaddingPoints(30)
        delivaryPointText.setLeftPaddingPoints(30)
        orderDurationText.setRightPaddingPoints(30)
        recivingPointText.setRightPaddingPoints(30)
        recivingPointText.setRightPaddingPoints(30)

        paragraphStyle.alignment = .center


    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if imagesArray.count == 0{
            self.imageCollectionHeight.constant = 0

        }else{
            self.imageCollectionHeight.constant = 120

        }
        self.view.updateConstraints()
        self.view.layoutIfNeeded()
    }
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func makeOrder(_ sender: Any) {
        validation()
    }
    
    @IBAction func addImage(_ sender: Any) {
        let imagePicker = OpalImagePickerController()
        imagePicker.imagePickerDelegate = self
        imagePicker.maximumSelectionsAllowed = 5
        imagePicker.selectionTintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.5706335616)
        imagePicker.selectionImage = #imageLiteral(resourceName: "checked")
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func addAddressRecToFavourite(_ sender: Any) {
        if self.recivingPointText.text?.count != 0 {
            favRecPlaceButton.setImage(#imageLiteral(resourceName: "star"), for: .normal)
            let vc = Storyboard.Home.viewController(SavePlaceVC.self)
            vc.lat = self.recLat
            vc.lng = self.recLng
            vc.address = self.recivingPointText.text!
               
            let popupVC = PopupViewController(contentController: vc, popupWidth: UIScreen.main.bounds.width-60, popupHeight: 150)
               
            popupVC.backgroundAlpha = 0.5
            popupVC.backgroundColor = .black
            popupVC.canTapOutsideToDismiss = true
            popupVC.cornerRadius = 20
            popupVC.shadowEnabled = true
               
               
            self.present(popupVC, animated: true)
        }
    }
    @IBAction func addAddressdelvToFavourite(_ sender: Any) {
        if self.delivaryPointText.text?.count != 0 {
            favDelivPlaceButton.setImage(#imageLiteral(resourceName: "star"), for: .normal)
            let vc = Storyboard.Home.viewController(SavePlaceVC.self)
            vc.lat = self.delLat
            vc.lng = self.delLng
            vc.address = self.recivingPointText.text!
                   
            let popupVC = PopupViewController(contentController: vc, popupWidth: UIScreen.main.bounds.width-60, popupHeight: 150)
                   
            popupVC.backgroundAlpha = 0.5
            popupVC.backgroundColor = .black
            popupVC.canTapOutsideToDismiss = true
            popupVC.cornerRadius = 20
            popupVC.shadowEnabled = true
                   
                   
            self.present(popupVC, animated: true)
        }
    }
    //=======
    @IBAction func selectRecLocation(_ sender: Any) {
        if savedPlaces.count == 0 {
            let vc = Storyboard.Authentication.viewController(GetLocationViewController.self)
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = Storyboard.Home.viewController(SavedPlacesVC.self)
            vc.specialOrdervc = self
            vc.delegate = self
            vc.isRecLocation = true
            vc.savedPlaces = self.savedPlaces
               
            let popupVC = PopupViewController(contentController: vc, popupWidth: UIScreen.main.bounds.width-60, popupHeight: 300)
               
            popupVC.backgroundAlpha = 0.5
            popupVC.backgroundColor = .black
            popupVC.canTapOutsideToDismiss = true
            popupVC.cornerRadius = 20
            popupVC.shadowEnabled = true
               
               
            self.present(popupVC, animated: true)
        }
        
        
    }
    //=======
    @IBAction func selectDelLocation(_ sender: Any) {
        if savedPlaces.count == 0 {
            print("myTargetFunction")
            let vc = Storyboard.Authentication.viewController(GetLocationViewController.self)
            vc.delegate2 = self
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = Storyboard.Home.viewController(SavedPlacesVC.self)
            vc.specialOrdervc = self
            vc.delegate = self
            vc.isRecLocation = false
            vc.savedPlaces = self.savedPlaces
                  
            let popupVC = PopupViewController(contentController: vc, popupWidth: UIScreen.main.bounds.width-60, popupHeight: 300)
                  
            popupVC.backgroundAlpha = 0.5
            popupVC.backgroundColor = .black
            popupVC.canTapOutsideToDismiss = true
            popupVC.cornerRadius = 20
            popupVC.shadowEnabled = true
                  
                  
            self.present(popupVC, animated: true)
        }
           
    }
    @IBAction func chosseOrderDuration(_ sender: Any) {
        if(PrayFlag == true){
            self.HourArray = ["2","3","4","5","6","7","8"]
        }else{
            self.HourArray = ["1","2","3","4","5","6","7","8"]
        }
        
        let vc = Storyboard.Home.viewController(SelectDurationVC.self)
        vc.hours = self.HourArray
        vc.vc = self
        let popupVC = PopupViewController(contentController: vc, popupWidth: UIScreen.main.bounds.width-100, popupHeight: 250)
           
        popupVC.backgroundAlpha = 0.5
        popupVC.backgroundColor = .black
        popupVC.canTapOutsideToDismiss = true
        popupVC.cornerRadius = 10
        popupVC.shadowEnabled = true
           
           
        self.present(popupVC, animated: true)
    }
}

extension SpecialOrderVC{
    func validation(){
        if delivaryPointText.text?.isEmpty == true || orderDurationText.text?.isEmpty == true || self.orderDetails.text.count == 0 || self.imagesArray.count == 0 {
            self.sentOrderButton.shake(duration: 0.5, values: [-12.0, 12.0, -12.0, 12.0, -6.0, 6.0, -3.0, 3.0, 0.0])
            ShowErrorMassge(massge: "Please complete your Data".localized, title: "Error".localized)
        }else{
            for i in imagesArray{
                self.imagesData.append((i).pngData()! as Data)
                  print("😆\(i)")
                imagesKeys.append("images[]")
            }
            
            self.sentOrder()
        }
    }
      
    func sentOrder(){
        
        let parmas:Parameters = [
            "receive_address":self.recivingPointText.text!,
            "receive_lat":self.recLat!,
            "receive_long":self.recLng!,
            "deliver_address":self.delivaryPointText.text!,
            "deliver_lat":self.delLat!,
            "deliver_long":self.delLng!,
            "deliver_time":self.selectedValue,
            "description":self.delivaryPointText.text!,
            "min_expected_price":self.minExpictedPrice!,
            "max_expected_price":self.MaxExpictedPrice!
        ]
        
        self.showProgress()
        API.POSTImage(url: CreateOrder_URL, Images: imagesData, Keys: imagesKeys, header: ["Authorization":"Bearer" + getTokenId(),"lang": getServerLang()], parameters: parmas) { (succss, value) in
               if succss{
                   let msg = value["msg"] as! String
                   self.stopAnimating()
                   print("🌸\(value)")
                   ShowTrueMassge(massge: msg, title: "Done".localized())
                   DispatchQueue.main.asyncAfter(deadline: .now() + 1) { // change 2 to desired number of
                       let vc = Storyboard.Home.viewController(ThanksForOrderViewController.self)
                       self.navigationController?.pushViewController(vc, animated: true)
                       
                   }
               }else{
                   self.stopAnimating()
                   ShowErrorMassge(massge: "Something went wrong and tried again".localized(), title: "Error".localized())
               }
           }
        
        
    }
    
    func getSavedPlaces(){
        self.savedPlaces.removeAll()
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        
        API.POST(url: savedPlaces_URL, parameters: [:], headers: header) { (succss, value) in
            if succss{
                let key = value["value"] as! String
                if key == "1" {
                    let data = value["data"] as! [[String:Any]]
                    for item in data {
                        self.savedPlaces.append(SavedPlace().getObject(dicc:item))
                    }
                    
                }
            }
        }
    }
    func getExpectedPrice(){
        let params:Parameters = [
            "receive_lat":self.recLat!,
            "receive_long":self.recLng!,
            "deliver_lat":self.delLat!,
            "deliver_long":self.delLng!
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
               
        API.POST(url: ExpectedPricePlace_URL, parameters: params, headers: header) { (succss, value) in
            if succss{
                let key = value["value"] as! String
                if key == "1" {
                    let data = value["data"] as! [String:Any]
                    // let msg = data["msg"] as! String
                    let min_expected_price = data["min_expected_price"] as! Double
                    let max_expected_price = data["max_expected_price"] as! Double
                    self.minExpictedPrice = min_expected_price
                    self.MaxExpictedPrice = max_expected_price
                //    self.expectedPrice.text = msg
                    let praynote = data["praynote"] as! String
                    if praynote == ""{
                        print("🏀\(value)")
                        self.PrayFlag = false
                        
                    }else{
                        PrayNote(massge: praynote)
                        print("🕌")
                        self.PrayFlag = true
                    }
                           
                }
            }
                
        }
    }
}


extension SpecialOrderVC :sendDataBackDelegateTwo, sendDataBackDelegate , choseSavedPlaceProtocole {
    
    func finishPassing2(location: String, lat: Double, lng: Double) {
         self.delivaryPointText.text = location
        self.delLat = lat
        self.delLng = lng
        if delLat != nil && delLng != nil &&  recLat != nil && recLng != nil {
            getExpectedPrice()

        }
    }
    
    
    //======== from map
    func finishPassing(location: String, lat: Double, lng: Double) {
        self.recivingPointText.text = location
        self.recLat = lat
        self.recLng = lng
        if delLat != nil && delLng != nil &&  recLat != nil && recLng != nil {
            getExpectedPrice()

        }
    }
    
    //=======from savedPlace
    func sentData(savedPlaces: SavedPlace , isRecLocation : Bool) {
        if isRecLocation {
            self.recivingPointText.text = savedPlaces.address
            self.recLat = Double(savedPlaces.lat)!
            self.recLng = Double(savedPlaces.long)!
            favRecPlaceButton.setImage(#imageLiteral(resourceName: "star"), for: .normal)

        }else{
            self.delivaryPointText.text = savedPlaces.address
            self.delLat = Double(savedPlaces.lat)!
            self.delLng = Double(savedPlaces.long)!
            favDelivPlaceButton.setImage(#imageLiteral(resourceName: "star"), for: .normal)
        }

        if delLat != nil && delLng != nil &&  recLat != nil && recLng != nil {
            getExpectedPrice()
        }
    }
}
