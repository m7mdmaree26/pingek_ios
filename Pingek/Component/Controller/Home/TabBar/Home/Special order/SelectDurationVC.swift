//
//  SelectDurationVC.swift
//  Pingek
//
//  Created by Engy Bakr on 2/10/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit

class SelectDurationVC: UIViewController {
    
    var hours = [String]()
    var vc = SpecialOrderVC()
    var vc2 : MakeOrderSecondPageVc?
    var currentSelectedItem = 0

    @IBOutlet weak var hoursPicker: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if vc2 == nil {
            self.vc.selectedValue = hours[0]
        }else{
            self.vc2!.selectedValue = hours[0]
        }
        

        // Do any additional setup after loading the view.
    }
    @IBAction func scrollToTop(_ sender: Any) {
        if currentSelectedItem != 0 {
            self.currentSelectedItem = self.currentSelectedItem - 1
            self.hoursPicker.selectRow(currentSelectedItem, inComponent: 0, animated: true)
            if vc2 == nil {
                self.vc.selectedValue = hours[self.currentSelectedItem]
            }else{
                self.vc2!.selectedValue = hours[self.currentSelectedItem]
            }
        }
    }
    @IBAction func scrollToButton(_ sender: Any) {
        if currentSelectedItem != self.hours.count - 1 {
            self.currentSelectedItem = self.currentSelectedItem + 1
            self.hoursPicker.selectRow(currentSelectedItem , inComponent: 0, animated: true)
            if vc2 == nil {
                self.vc.selectedValue = hours[self.currentSelectedItem]
            }else{
                self.vc2!.selectedValue = hours[self.currentSelectedItem]
            }
        }
    }
    @IBAction func yesAction(_ sender: Any) {
        if vc2 == nil {
            self.vc.orderDurationText.text = self.vc.selectedValue  + " "  + "Hour".localized()
        }else{
            self.vc2!.orderDurationText.text = self.vc2!.selectedValue  + " "  + "Hour".localized()
        }
        self.dismiss(animated: true )
    }
    
    @IBAction func noAction(_ sender: Any) {
        if vc2 == nil {
            self.vc.selectedValue = ""
            self.vc.orderDurationText.text = ""
        }else{
            self.vc2!.selectedValue = ""
            self.vc2!.orderDurationText.text = ""
        }
        self.dismiss(animated: true )
    }
}
extension SelectDurationVC : UIPickerViewDelegate , UIPickerViewDataSource{
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return hours.count
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = UILabel()
        label.textColor = UIColor.BasicColor
        label.text =  hours[row] + " "  + "Hour".localized()
        label.textAlignment  = .center
        label.font = UIFont(name: "JFFlat-Medium", size: 15)
        
        return label
    }
    
   func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
        self.currentSelectedItem = row
        if vc2 == nil {
            self.vc.selectedValue = hours[row]

        }else{
            self.vc2!.selectedValue = hours[row]

        }
   }

}
