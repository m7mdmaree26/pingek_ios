//
//  VrefcationCodeViewController.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/21/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//
import UIKit
import Alamofire
import SwiftyJSON

class VrefcationCodeViewController: UIViewController {
    var digit = 0;
    var timerTest : Timer?
    var fromForgetPassword = false

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var confirmation: UIButton!
    @IBOutlet weak var resendCode: UIButton!
    
    @IBOutlet weak var FrCodeNumber: UITextField!
    @IBOutlet weak var SeCodeNumber: UITextField!
    @IBOutlet weak var ThCodeNumber: UITextField!
    @IBOutlet weak var FoCodeNumber: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()


    }
    
    func setupView(){
        self.logo.image = #imageLiteral(resourceName: "logo")
        self.hideKeyboardWhenTappedAround()
        self.resendCode.isHidden = true
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)
        FrCodeNumber.becomeFirstResponder()
        FrCodeNumber.addTarget(self, action: #selector(codeValidation(_:)), for: .editingChanged)
        SeCodeNumber.addTarget(self, action: #selector(codeValidation(_:)), for: .editingChanged)
        ThCodeNumber.addTarget(self, action: #selector(codeValidation(_:)), for: .editingChanged)
        FoCodeNumber.addTarget(self, action: #selector(codeValidation(_:)), for: .editingChanged)
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)
        
        if AppLanguage.currentLanguage().contains("en"){
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
            UITableView.appearance().semanticContentAttribute = .forceLeftToRight
            UIImageView.appearance().semanticContentAttribute = .forceLeftToRight
            UITextField.appearance().semanticContentAttribute = .forceLeftToRight
        }else{
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
            UITableView.appearance().semanticContentAttribute = .forceRightToLeft
            UIImageView.appearance().semanticContentAttribute = .forceRightToLeft
            UITextField.appearance().semanticContentAttribute = .forceRightToLeft
        }
        FrCodeNumber.addTarget(self, action: #selector(FoucseUIEditText), for: UIControl.Event.editingDidBegin)
        FrCodeNumber.addTarget(self, action: #selector(UnFoucseUIEditText), for: UIControl.Event.editingDidEnd)
        SeCodeNumber.addTarget(self, action: #selector(FoucseUIEditText), for: UIControl.Event.editingDidBegin)
        SeCodeNumber.addTarget(self, action: #selector(UnFoucseUIEditText), for: UIControl.Event.editingDidEnd)
        ThCodeNumber.addTarget(self, action: #selector(FoucseUIEditText), for: UIControl.Event.editingDidBegin)
        ThCodeNumber.addTarget(self, action: #selector(UnFoucseUIEditText), for: UIControl.Event.editingDidEnd)
        FoCodeNumber.addTarget(self, action: #selector(FoucseUIEditText), for: UIControl.Event.editingDidBegin)
        FoCodeNumber.addTarget(self, action: #selector(UnFoucseUIEditText), for: UIControl.Event.editingDidEnd)
        
    }
    
    @objc func FoucseUIEditText(text:UITextField){
        text.layer.borderColor = UIColor.BasicColor.cgColor
        text.layer.borderWidth = 2
    }
    
    @objc func UnFoucseUIEditText(text:UITextField){
        text.layer.borderColor = UIColor.clear.cgColor
        text.layer.borderWidth = 1
    }
    
    @objc func codeValidation(_ TextField : UITextField){
        if TextField == FrCodeNumber {
            if FrCodeNumber.text?.count == 1{
                SeCodeNumber.becomeFirstResponder()
            }
        }else if TextField == SeCodeNumber {
            if SeCodeNumber.text?.count == 1{
                ThCodeNumber.becomeFirstResponder()
            }
        }else if TextField == ThCodeNumber {
            if ThCodeNumber.text?.count == 1{
                FoCodeNumber.becomeFirstResponder()
            }
        }else if TextField == FoCodeNumber {
            if FoCodeNumber.text?.count == 1{
                validation()
            }
        }
        
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func confirmationAction(_ sender: Any) {
        validation()
    }
    @IBAction func resendAction(_ sender: Any) {
       ResendVrefCode(user_id:getUserID())
       self.showProgress()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
       
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    
    func validation(){
        if(FrCodeNumber.text?.isEmpty == true) || (SeCodeNumber.text?.isEmpty == true) || (ThCodeNumber.text?.isEmpty == true) || (FoCodeNumber.text?.isEmpty == true) {
            self.confirmation.shake(duration: 1, values: [-12.0, 12.0, -12.0, 12.0, -6.0, 6.0, -3.0, 3.0, 0.0])
            ShowErrorMassge(massge: "You must Enter Vrefcation Code".localized, title: "Error".localized)
        }else{
            if fromForgetPassword{
                let forgetPass = Storyboard.Authentication.viewController(ReasetPasswordViewController.self)
                forgetPass.code = (self.FrCodeNumber.text!) + (self.SeCodeNumber.text!) + (self.ThCodeNumber.text!) + (self.FoCodeNumber.text!)
                self.navigationController?.pushViewController(forgetPass, animated: true)


            }else{
                self.showProgress()
                self.ConfirmationCode(user_id: getUserID(), code: (self.FrCodeNumber.text!) + (self.SeCodeNumber.text!) + (self.ThCodeNumber.text!) + (self.FoCodeNumber.text!))
            }
        }
    }
    
    func ConfirmationCode(user_id:String,code:String){
        let parms:Parameters = [
            "user_id":user_id,
            "code":code
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        API.POST(url:AccountActivation_URL , parameters: parms, headers: header) { (success, value) in
            if success {
                
                let key = value["key"] as? String
                if(key == "success"){
                    self.stopAnimating()
                    ShowTrueMassge(massge: "Successfully activated".localized, title: "Success".localized)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        defaults.set("yes", forKey: is_LoogedIn)
                        guard let window = UIApplication.shared.keyWindow else { return }
                        let vc = Storyboard.Home.viewController(TabBarViewController.self)
                        window.rootViewController = vc
                    }
                }else{
                    self.stopAnimating()
                    self.confirmation.shake(duration: 0.5, values: [-12.0, 12.0, -12.0, 12.0, -6.0, 6.0, -3.0, 3.0, 0.0])
                    ShowErrorMassge(massge: "Invalid activation code".localized(), title: "Error".localized)
                }
            
            }else{
                self.stopAnimating()
            }
        }
    }

    func ResendVrefCode(user_id:String){
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
       API.POST(url: SendActivation_URL, parameters: ["user_id":user_id], headers: header) { (succss, value) in
            if succss{
                self.stopAnimating()
                print("🤔\(value)")
                 ShowTrueMassge(massge: "Verification code sent".localized, title: "Success".localized)
                self.resendCode.isEnabled = false
            }else{
                   self.stopAnimating()
                  self.resendCode.isEnabled = true
            }
        }
    }
    
   
}
