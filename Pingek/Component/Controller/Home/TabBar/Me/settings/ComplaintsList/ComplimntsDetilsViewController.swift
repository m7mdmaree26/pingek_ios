//
//  ComplimntsDetilsViewController.swift
//  Ri7nak
//
//  Created by Sara Ashraf on 1/22/20.
//  Copyright © 2020 Sara Ashraf. All rights reserved.
//

import UIKit
import EzPopup

class ComplimntsDetilsViewController: UIViewController {
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var storeIcon: UIImageView!
    @IBOutlet weak var orderDetils: UITextView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var resoneLable: UILabel!
    @IBOutlet weak var notesLable: UILabel!
    @IBOutlet weak var noAttchments: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewAlert: UIView!
    @IBOutlet weak var hightAlert: NSLayoutConstraint!
    @IBOutlet weak var storeNAme: UILabel!
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var answerLable: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var orderNumber: UILabel!
    @IBOutlet weak var imageCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var storeIconHeight: NSLayoutConstraint!
    @IBOutlet weak var storeIconWedith: NSLayoutConstraint!

    var Images = [String]()
    var ticket_id = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.noAttchments.isHidden = true
        self.viewAlert.isHidden = true
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)

    }
    override func viewWillAppear(_ animated: Bool) {
        self.getComplimntsList(ticket_id: self.ticket_id)
    }
    
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func notsadtfied(_ sender: Any) {
        self.showProgress()
        self.SatisfactionOrNot(ticket_id: self.ticket_id, action: "true")
    }
    
    @IBAction func sistfied(_ sender: Any) {
        self.showProgress()
        self.SatisfactionOrNot(ticket_id: self.ticket_id, action: "false")

    }
    
    
    
    func getComplimntsList(ticket_id:String){
         self.showProgress()
            API.POST(url: userTicket_URL, parameters: ["ticket_id":ticket_id], headers: ["lang":AppLanguage.getLang(),"Authorization":"Bearer" + getTokenId()], completion: { (success, value) in
                if success{
                    self.Images =  []
                    self.stopAnimating()
                    let code = value["code"] as!  Int
                    if(code == 200){
                        let data = value["data"] as! [String:Any]
                        
                       // let id = data["id"] as! Int
                        let subject = data["subject"] as! String
                        let username = data["username"] as! String
                       // let user_id = data["user_id"] as! Int
                        let order_id = data["order_id"] as! Int

                        let avatar = data["avatar"] as! String
                        let store_name = data["store_name"] as! String
                        let store_icon = data["store_icon"] as! String

                        let text = data["text"] as! String
                        let answer = data["answer"] as! String
                        let status = data["status"] as! String
                        let details =  data["details"] as! String
                        let date = data["date"] as! String
                        let images = data["images"] as! [String]
                        let make_satisfied = data["make_satisfied"] as! String
                      
                        self.date.text! = date
                        self.storeIcon.setImageWith(store_icon)
                        self.userImage.setImageWith(avatar)
                        self.orderDetils.text! = details
                        self.userName.text! = username
                        self.resoneLable.text! = subject
                        self.notesLable.text! = text
                        self.storeIcon.clipsToBounds = true
                        self.userImage.clipsToBounds = true
                        self.answerLable.text! = answer
                        self.storeNAme.text = store_name
                        self.orderNumber.text = "Order number : ".localized + String(describing: order_id)
                        
                        if store_name == "" {
                            self.storeIcon.isHidden = true
                            self.storeNAme.isHidden = true
                            self.storeIconHeight.constant = 0
                            self.storeIconWedith.constant = 0
                        
                        }
                        
                        for i in images{
                            self.Images.append(i)
                        }
                        if(status == "finished"){
                            if(make_satisfied == "true"){
                                self.viewAlert.isHidden = true

                            }else{
                                self.viewAlert.isHidden = false
                            }
                            self.statusButton.setTitle("Processed >".localized, for: .normal)

                        }else{
                            self.viewAlert.isHidden = true
                            self.statusButton.setTitle("Processing".localized, for: .normal)
                        }
                        
                        if(self.Images.count != 0){
                            self.noAttchments.isHidden = true
                            self.imageCollectionHeight.constant = 100
                        }else{
                            self.noAttchments.isHidden = false
                            self.imageCollectionHeight.constant = 0
                        }

                        
                        self.collectionView.reloadData()
                        }
                    }
                })
            }
   
    
    func SatisfactionOrNot(ticket_id:String,action:String){
        
        API.POST(url: customerSatisfaction_URL, parameters: ["ticket_id":ticket_id,"action":action], headers: ["lang":AppLanguage.getLang(),"Authorization":"Bearer" + getTokenId()], completion: { (success, value) in
                if success{
                    self.stopAnimating()
                    let code = value["code"] as!  Int
                        if(code == 200){
                            let msg = value["msg"] as!  String

                            ShowTrueMassge(massge:msg , title:"Success".localized)
                            self.navigationController?.popViewController(animated: true)
                        }else{
                            let msg = value["msg"] as!  String
                            ShowErrorMassge(massge: msg, title: "Error".localized)

                        }
                    }
                })
            }
    
    
    

}

extension ComplimntsDetilsViewController: UICollectionViewDelegate ,UICollectionViewDataSource,UIScrollViewDelegate,UICollectionViewDelegateFlowLayout{
//
//    func configurCollectionView(){
//           collectionView.register(UINib.init(nibName: "AttchmentViewCell", bundle: nil), forCellWithReuseIdentifier: "AttchmentViewCell")
//
//       }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttchmentViewCell", for: indexPath) as! AttchmentViewCell
        cell.attceImage.setImageWith(self.Images[indexPath.row])
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.BasicColor.cgColor
        cell.layer.cornerRadius = 10
        cell.clipsToBounds = true
        return cell

    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          
        return self.Images.count
      }
      
      func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        let vc = Storyboard.Home.viewController(ShowImageVC.self)
                   vc.image = self.Images[indexPath.row]
                          
                   let popupVC = PopupViewController(contentController: vc, popupWidth: UIScreen.main.bounds.width, popupHeight:  UIScreen.main.bounds.height)
                                      
                   popupVC.backgroundAlpha = 0.5
                   popupVC.backgroundColor = .black
                   popupVC.canTapOutsideToDismiss = true
                   popupVC.cornerRadius = 0
                   popupVC.shadowEnabled = true
                                          
                                          
        self.present(popupVC, animated: true)

    }
}

class AttchmentViewCell: UICollectionViewCell {
    @IBOutlet weak var attceImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }

}
