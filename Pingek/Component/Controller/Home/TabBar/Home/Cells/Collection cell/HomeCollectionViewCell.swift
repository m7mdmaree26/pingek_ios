//
//  HomeCollectionViewCell.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/24/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var iconLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }

}
