//
//  ReasetPasswordViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/2/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ReasetPasswordViewController: UIViewController {
    
    
    var code = ""
    
    @IBOutlet weak var newpassword: UITextField!
    @IBOutlet weak var newPasswordConfirmation: UITextField!
    @IBOutlet weak var confirmationBt: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var newPasswordShowIcon: UIButton!
    @IBOutlet weak var confirmPasswordShowIcon: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newpassword.setLeftPaddingPoints(10)
        newpassword.setRightPaddingPoints(40)
        newPasswordConfirmation.setLeftPaddingPoints(10)
        newPasswordConfirmation.setRightPaddingPoints(40)
        
        self.hideKeyboardWhenTappedAround()
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)
        newpassword.addTarget(self, action: #selector(FoucseUIEditText), for: UIControl.Event.editingDidBegin)
        newpassword.addTarget(self, action: #selector(UnFoucseUIEditText), for: UIControl.Event.editingDidEnd)
        newPasswordConfirmation.addTarget(self, action: #selector(FoucseUIEditText), for: UIControl.Event.editingDidBegin)
        newPasswordConfirmation.addTarget(self, action: #selector(UnFoucseUIEditText), for: UIControl.Event.editingDidEnd)

    }
    @objc func FoucseUIEditText(text:UITextField){
        text.layer.borderColor = UIColor.BasicColor.cgColor
        text.layer.borderWidth = 2
    }
    
    @objc func UnFoucseUIEditText(text:UITextField){
        text.layer.borderColor = UIColor.clear.cgColor
        text.layer.borderWidth = 1
    }
    
    @IBAction func showNewPassword(_ sender: Any) {
        if newpassword.isSecureTextEntry {
            self.newPasswordShowIcon.setImage(#imageLiteral(resourceName: "view-1"), for: .normal)
            newpassword.isSecureTextEntry = false
        }else{
            self.newPasswordShowIcon.setImage(#imageLiteral(resourceName: "unview"), for: .normal)
            newpassword.isSecureTextEntry = true
        }
    }
    @IBAction func showConfirmPassword(_ sender: Any) {
        if newPasswordConfirmation.isSecureTextEntry {
            self.confirmPasswordShowIcon.setImage(#imageLiteral(resourceName: "view-1"), for: .normal)
            newPasswordConfirmation.isSecureTextEntry = false
        }else{
            self.confirmPasswordShowIcon.setImage(#imageLiteral(resourceName: "unview"), for: .normal)
            newPasswordConfirmation.isSecureTextEntry = true
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirmationAction(_ sender: Any) {
        if(newpassword.text?.isEmpty == true || newPasswordConfirmation.text?.isEmpty == true){
            self.confirmationBt.shake(duration: 0.5, values: [-12.0, 12.0, -12.0, 12.0, -6.0, 6.0, -3.0, 3.0, 0.0])
            ShowErrorMassge(massge: "Please complete your Data".localized, title: "Error".localized)
        }else{
            if(newpassword.text != newPasswordConfirmation.text){
                self.confirmationBt.shake(duration: 0.5, values: [-12.0, 12.0, -12.0, 12.0, -6.0, 6.0, -3.0, 3.0, 0.0])
                ShowErrorMassge(massge: "Password and Password Confirmation didn't Match".localized, title: "Error".localized)
            }else{
                self.showProgress()
                self.ReastPAssword(user_id: getUserID(), password: self.newPasswordConfirmation.text!, code: self.code)
            }
        }
    }
    
   
    func ReastPAssword(user_id:String,password:String,code:String){
        let Parmas:Parameters = [
            "user_id":user_id,
            "code":code,
            "password":password
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        API.POST(url: ReasetPassword_URL, parameters:Parmas , headers: header) { (succss, value) in
            if succss{
                let key = value["key"] as! String
                self.stopAnimating()
                print("😆\(Parmas)")
                if(key == "success"){
                    ShowTrueMassge(massge: "Password changed successfully".localized, title: "Succsse".localized)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        let vc = Storyboard.Authentication.viewController(LoginViewController.self)

                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }else{
                    let msg = value["msg"] as! String
                    self.confirmationBt.shake(duration: 0.5, values: [-12.0, 12.0, -12.0, 12.0, -6.0, 6.0, -3.0, 3.0, 0.0])
                    ShowErrorMassge(massge: msg, title: "Error".localized())
                }
                
            }else{
                self.stopAnimating()
            }
        }
    }


}
