//
//  UITextViewExtention.swift
//  Noon
//
//  Created by Engy Bakr on 12/25/19.
//  Copyright © 2019 Engy Bakr. All rights reserved.
//

import Foundation
import UIKit

extension UITextView {
    @IBInspectable var localizedText: String? {
        get {
            return text
        }
        set {
            text = newValue?.localized
        }
    }
    
    var localizedFont: FontTypesWithSize {
        set {
            font = UIFont.getLocalizedFont(type: newValue)
        }get {
            return .regular(16)
        }
    }
}
