//
//  ForgetPasswordViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/24/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class ForgetPasswordViewController: UIViewController {
    @IBOutlet weak var mobileNumber: UITextField!
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
           super.viewDidLoad()
           logo.image = #imageLiteral(resourceName: "logo")
           mobileNumber.setLeftPaddingPoints(20)
           mobileNumber.setRightPaddingPoints(20)
           self.hideKeyboardWhenTappedAround()
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)
        mobileNumber.addTarget(self, action: #selector(FoucseUIEditText), for: UIControl.Event.editingDidBegin)
        mobileNumber.addTarget(self, action: #selector(UnFoucseUIEditText), for: UIControl.Event.editingDidEnd)

       }
    @objc func FoucseUIEditText(text:UITextField){
        text.layer.borderColor = UIColor.BasicColor.cgColor
        text.layer.borderWidth = 2
    }
    
    @objc func UnFoucseUIEditText(text:UITextField){
        text.layer.borderColor = UIColor.clear.cgColor
        text.layer.borderWidth = 1
    }
       override func viewWillAppear(_ animated: Bool) {
           self.navigationController?.isNavigationBarHidden = true
           self.tabBarController?.tabBar.isHidden = true
       }
    
    
    @IBAction func sendAction(_ sender: Any) {
        if(mobileNumber.text?.isEmpty == true){
            self.sendBtn.shake(duration: 0.5, values: [-12.0, 12.0, -12.0, 12.0, -6.0, 6.0, -3.0, 3.0, 0.0])
            ShowErrorMassge(massge: "Enter Phone Number".localized, title: "Error".localized)
        }else{
            self.showProgress()
            self.forgetPass(mobileNumber: self.mobileNumber.text!)
        }
    }
    
   
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func forgetPass(mobileNumber:String){
        let Parmters: Parameters = [
        "phone":mobileNumber
        ]
        API.POST(url: FprgetPassword_URL, parameters: Parmters, headers: nil ){ (success, value) in
            if success{
                print("⏳\(Parmters)")
                print("🖍\(value)")
                self.stopAnimating()
            let key = value["key"] as! String
                if(key == "success"){
                 let data = value["data"] as! [String:Any]
                 let code = data["code"] as! String
                 let token = data["token"] as! String
                 let id = data["id"] as! Int
                    print("💙\(code)")
                    defaults.set(token, forKey: Token_ID)
                    defaults.set(String(describing: id), forKey: User_ID)
                    let forgetPass = Storyboard.Authentication.viewController(VrefcationCodeViewController.self)
                    forgetPass.fromForgetPassword = true
                    self.navigationController?.pushViewController(forgetPass, animated: true)

                }else{
                    let msg = value["msg"] as! String
                    self.sendBtn.shake(duration: 0.5, values: [-12.0, 12.0, -12.0, 12.0, -6.0, 6.0, -3.0, 3.0, 0.0])
                    ShowErrorMassge(massge: msg, title: "Error".localized)
                }
            
                
            }else{
            self.stopAnimating()
                
            }
        
        
    }
    }
    
}
