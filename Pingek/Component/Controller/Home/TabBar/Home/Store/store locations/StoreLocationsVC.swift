//
//  StoreLocationsVC.swift
//  Sandkom
//
//  Created by Engy Bakr on 2/1/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import GoogleMaps

protocol StoreBranchDelegate {
    func selectBranch(branche : BranchesModel)
}


class StoreLocationsVC: UIViewController , GMSMapViewDelegate {
    
    var selectedBranch = BranchesModel()
    var branches = [BranchesModel]()
    var store = StoreDetailsModel()
    var delegate : StoreBranchDelegate?
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var storeName: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var storeImage: UIImageView!
    @IBOutlet weak var locationsTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupview()

    }
    

    func setupview(){
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)
        locationsTable.isHidden = true
        self.storeName.text = store.name
        self.storeImage.setImageWith(store.icon)
        mapView.delegate = self
        for i in self.branches{
            self.addMarkerToMap(lat: Double(i.lat)!, lng: Double(i.lng)! , image: #imageLiteral(resourceName: "vectorstore"), title: i.address , distance : i.distance, branch : i)
        }
     
    }
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func showList(_ sender: Any) {
        if locationsTable.isHidden{
            locationsTable.isHidden = false
            mapView.isHidden = true
        }else{
            locationsTable.isHidden = true
            mapView.isHidden = false
        }
    }
    
    @IBAction func confirm(_ sender: Any) {
        delegate?.selectBranch(branche: selectedBranch)
        self.navigationController?.popViewController(animated: true)
    }
    
    func addMarkerToMap(lat: Double,lng: Double,image : UIImage,title : String?,distance : String, branch : BranchesModel){
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        if title != nil{
            marker.title = title!
            marker.snippet = distance
        }
           
        marker.appearAnimation = .pop
        marker.icon = image
        marker.map = mapView
        self.mapView.selectedMarker = marker
        self.selectedBranch = branch
        let camera = GMSCameraPosition.camera(withLatitude:lat, longitude: lng, zoom: 15)
        self.mapView.camera = camera
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let branch = BranchesModel()
        branch.lat = String(describing: marker.position.latitude)
        branch.lng = String(describing: marker.position.longitude)
        branch.address = marker.title!
        branch.lng = marker.snippet!
        self.selectedBranch = branch
        
        return true
    }
    
}


extension StoreLocationsVC :UITableViewDataSource ,UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return branches.count
    }
     
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StoreLocationTableCell", for: indexPath) as! StoreLocationTableCell
        cell.storeNAme.text = branches[indexPath.section].address
        cell.storeDistance.text = branches[indexPath.section].distance
        cell.storeImage.setImageWith(store.icon)
        
        if indexPath.row == 0 {
            cell.setSelected(true, animated: true)
        }
                
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedBranch = branches[indexPath.section]
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
                
        return 120
    }
}
