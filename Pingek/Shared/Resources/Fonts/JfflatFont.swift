//
//  JfflatFont.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/16/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import Foundation

enum JfflatFont: String, AppFont {
    case light = "JF-Flat-light"
    case regular = "JF-Flat-regular"
    case semiBold = "JF-Flat-medium"
//    case bold = "Cairo-Bold"
//    case black = "Cairo-Black"

}
