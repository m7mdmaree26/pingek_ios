//
//  TimesTableCell.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/23/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit

class TimesTableCell: UITableViewCell {

    @IBOutlet weak var timeLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
