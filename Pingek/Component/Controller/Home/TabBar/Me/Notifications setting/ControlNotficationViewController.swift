//
//  ControlNotficationViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/26/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import TKSwitcherCollection
import AVFoundation

class ControlNotficationViewController: UIViewController {
    
    var player: AVAudioPlayer!
    @IBOutlet weak var upcomingRequst: TKSmileSwitch!
    @IBOutlet weak var alertSwitch: TKSmileSwitch!
    @IBOutlet weak var advSwitch: TKSmileSwitch!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var thirdView: UIView!
    @IBOutlet weak var backButton: UIButton!

    @IBAction func goBack(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDesign()
        self.navigationController?.navigationBar.topItem?.title = ""
        
    }
    override func viewWillAppear(_ animated: Bool) {
        getDeviceData()
         self.tabBarController?.tabBar.isHidden = true
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)

        
    }
    
    func viewDesign(){
       
        firstView.layer.shadowColor = UIColor.lightGray.cgColor
        firstView.layer.shadowOpacity = 1
        firstView.layer.shadowOffset = CGSize.zero
        firstView.layer.shadowRadius = 4
        
        secondView.layer.shadowColor = UIColor.lightGray.cgColor
        secondView.layer.shadowOpacity = 1
        secondView.layer.shadowOffset = CGSize.zero
        secondView.layer.shadowRadius = 4
        
        thirdView.layer.shadowColor = UIColor.lightGray.cgColor
        thirdView.layer.shadowOpacity = 1
        thirdView.layer.shadowOffset = CGSize.zero
        thirdView.layer.shadowRadius = 4
        self.upcomingRequst.addTarget(self, action: #selector(SaveSwitchChange), for: .valueChanged)
        self.alertSwitch.addTarget(self, action: #selector(SaveSwitchChange), for: .valueChanged)
        self.advSwitch.addTarget(self, action: #selector(SaveSwitchChange), for: .valueChanged)

    }
    @objc func SaveSwitchChange(){
        self.showProgress()
       self.UpdateDeviceData(show_ads: getadvSwitch(), orders_notify:  getAlertNotfiy(), near_orders_notify: getUpcomnigRequst())
      //  playSound()
    }
//    @objc func playSound() {
//        guard let url = Bundle.main.url(forResource: "stairs", withExtension: "m4r") else { return }
//        
//        do {
//            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
//            try AVAudioSession.sharedInstance().setActive(true)
//            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
//            player.play()
//        } catch let error {
//            print(error.localizedDescription)
//        }
//    }
    func getDeviceData(){
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        API.POST(url: DeviceData_URL, parameters: ["device_id":AppDelegate.FCMTOKEN], headers: header) { (succss, value) in
            if succss{
                self.stopAnimating()
                let key = value["key"] as? String
                if(key == "success"){
                    let data = value["data"] as? [String:Any]
                    let near_orders_notify = data!["near_orders_notify"] as? String
                    let orders_notify = data!["orders_notify"] as? String
                    let show_ads = data!["show_ads"] as? String
                    if(show_ads! == "true"){
                        self.advSwitch.setOn(true)
                    }else{
                         self.advSwitch.setOn(false)
                    }
                    if(near_orders_notify! == "true"){
                        self.upcomingRequst.setOn(true)
                    }else{
                        self.upcomingRequst.setOn(false)
                    }
                    if(orders_notify! == "true"){
                        self.alertSwitch.setOn(true)
                    }else{
                        self.alertSwitch.setOn(false)
                    }
                  print("🍉\(show_ads)")
                }
                
            }else{
                self.stopAnimating()
               
            }
        }
    }
    func getadvSwitch()-> String{
        if(  self.advSwitch.isOn == true){
        return "true"
        }else{
        return "false"
        }
    }
    func getAlertNotfiy()-> String{
        if(  self.alertSwitch.isOn == true){
            return "true"
        }else{
            return "false"
        }
    }
    func getUpcomnigRequst()-> String{
        if(  self.upcomingRequst.isOn == true){
            return "true"
        }else{
            return "false"
        }
    }
    
    func UpdateDeviceData(show_ads:String,orders_notify:String,near_orders_notify:String){
        let params :Parameters = [
            "device_id":AppDelegate.FCMTOKEN,
            "show_ads":show_ads,
            "orders_notify":orders_notify,
            "near_orders_notify":near_orders_notify
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        API.POST(url: UpdateDeviceData_URL, parameters:params , headers: header) { (succss, value) in
            if succss{
                print("🔓\(params)")
                self.stopAnimating()
                let key = value["key"] as? String
                if(key == "success"){
                  self.stopAnimating()
                    print("🍉\(value)")
                }
                
            }else{
                self.stopAnimating()
                
            }
        }
    }
}
