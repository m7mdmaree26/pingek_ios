//
//  OffersBidAlert.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/27/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import Cosmos
import SwiftyJSON
import Alamofire

class OffersBidAlert: UIViewController {

    var BidId = ""
    var BideDetails = BideModel()
    var vc = NotificationsVC()
    var viewController : UIViewController?
    
    
    
    
    @IBOutlet weak var noOfOffers: UILabel!
    @IBOutlet weak var noOfRates: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var delivryTime: UILabel!
    @IBOutlet weak var delivryCost: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var bideImage: UIImageView!
    @IBOutlet weak var allOffersStack: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getViewBid(bid_id: BidId)
        noOfOffers.clipsToBounds = true
        bideImage.clipsToBounds = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        allOffersStack.addGestureRecognizer(tap)

        // Do any additional setup after loading the view.
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true)
        let vc = Storyboard.Home.viewController(AllOfferBidesVC.self)
        vc.order_id = BideDetails.id
        if viewController != nil {
            self.viewController?.navigationController?.pushViewController(vc, animated: true)

        }else{
            self.vc.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @IBAction func acceptBide(_ sender: Any) {
        agreeBid(bid_id: self.BidId, action: "agree")
        defaults.set("home", forKey: ViewOrderfrom)
    }
    @IBAction func refuseBide(_ sender: Any) {
        agreeBid(bid_id: self.BidId, action: "refuse")
    }
    
    
}


extension OffersBidAlert {
  func getViewBid(bid_id:String){
    
    self.showProgress()
        
        let parmas :Parameters = [
       
            "bid_id":bid_id
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        API.POST(url: ViewBid_URL, parameters: parmas, headers: header) { (succss, value) in
            
            if succss{
                self.stopAnimating()
                let key = value["key"] as? String
                
                if(key == "success"){
                    let data = value["data"] as! [String:Any]
                    self.BideDetails = BideModel().getObject(dicc: data)
                    self.userName.text = self.BideDetails.bid.provider_name
                    self.bideImage.setImageWith(self.BideDetails.bid.avatar)
                    self.delivryTime.text = self.BideDetails.bid.deliver_time  + " " + "Hour".localized
                    self.delivryCost.text = self.BideDetails.bid.price + " " + self.BideDetails.bid.currency
                    self.distanceLbl.text = self.BideDetails.bid.distance_provider_to_store + " " + "KM".localized
                    self.noOfRates.text = "( " + self.BideDetails.bid.num_rating + " )"
                    self.rateView.rating = Double(self.BideDetails.bid.rating)!
                    self.noOfOffers.text = self.BideDetails.bid.num_bids
                    
                }else{
                     let msg = value["msg"] as? String
                    ShowErrorMassge(massge: msg!, title: "Error".localized)
                    self.dismiss(animated: true)
                    
                }
                
            }else{
                self.stopAnimating()
               
            }
        }
    }
    
    func agreeBid(bid_id:String,action:String){
        let parmas :Parameters = [
            "bid_id":bid_id,
            "action":action
        ]
        print("🍉\(parmas)")
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        API.POST(url: agreeBid_URL, parameters: parmas, headers: header) { (succss, value) in
            if succss{
                let key = value["key"] as! String
                if(key == "fail"){
                    let msg = value["msg"] as! String
                    ShowErrorMassge(massge: msg, title: "Error".localized)
                }else{
                    
                    let data = value["data"] as! [String:Any]
                    let keyData = data["key"] as! String
                    let message_ar = data["message_ar"] as! String
                    let message_en = data["message_en"] as! String
                    
                    
                    
                    
                    
                    //===============disAgree Offer
                    if(keyData == "disagreeBid"){
                        if(getServerLang() == "ar"){
                            ShowTrueMassge(massge: message_ar, title: "")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                self.dismiss(animated: true)
                                
                            }
                        }else{
                            ShowTrueMassge(massge: message_en, title: "")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                 self.dismiss(animated: true)
                            }
                        }
                        if self.viewController == nil {
                            self.vc.getNotfications(page: self.vc.pageCount)

                        }
                    }else{
                        ShowTrueMassge(massge: "Your request has been accepted".localized, title: "Success".localized)
                        let conversation_id = data["conversation_id"] as! Int
                        let vc = Storyboard.Home.viewController(ChatViewController.self)
                        vc.ConvrstionId = String(conversation_id)
                        self.dismiss(animated: true)
                        if self.viewController != nil {
                            self.viewController?.navigationController?.pushViewController(vc, animated: true)

                        }else{
                            self.vc.navigationController?.pushViewController(vc, animated: true)
                        }

                    }
                }
            }else{
                
            }
        }
    }
}
