//
//  LoginViewController.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/20/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import TransitionButton
import Alamofire
import SwiftyJSON
import GoogleMaps
import SKCountryPicker
import TextFieldEffects

class LoginViewController: UIViewController {
    @IBOutlet weak var loginLogo: UIImageView!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var mobileNumber: TextField!
    @IBOutlet weak var LoginAsVestor: UIButton!
    @IBOutlet weak var LoginBtn: UIButton!
    @IBOutlet weak var viewImage: UIButton!
 
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mobileNumber.keyboardType = .asciiCapableNumberPad
        LoginBtn.layer.borderColor = UIColor.clear.cgColor
        ButtonDesign(Btn: LoginBtn)
        ButtonDesign(Btn: LoginAsVestor)
        LoginBtn.layer.borderColor = UIColor.clear.cgColor
        LoginAsVestor.layer.borderColor = UIColor.clear.cgColor
        LoginBtn.clipsToBounds = true
        self.hideKeyboardWhenTappedAround()
        self.view.bringSubviewToFront(LoginBtn)
        self.loginLogo.image = #imageLiteral(resourceName: "logo")
        print("😻\(AppDelegate.FCMTOKEN)")
        mobileNumber.setLeftPaddingPoints(20)
        mobileNumber.setRightPaddingPoints(20)
        password.setLeftPaddingPoints(20)
        password.setRightPaddingPoints(40)
        mobileNumber.addTarget(self, action: #selector(FoucseUIEditText), for: UIControl.Event.editingDidBegin)
        mobileNumber.addTarget(self, action: #selector(UnFoucseUIEditText), for: UIControl.Event.editingDidEnd)
        password.addTarget(self, action: #selector(FoucseUIEditText), for: UIControl.Event.editingDidBegin)
        password.addTarget(self, action: #selector(UnFoucseUIEditText), for: UIControl.Event.editingDidEnd)
        
    
        
    }
    
    @objc func FoucseUIEditText(text:UITextField){
        text.layer.borderColor = UIColor.BasicColor.cgColor
        text.layer.borderWidth = 2
    }
    
    @objc func UnFoucseUIEditText(text:UITextField){
        text.layer.borderColor = UIColor.clear.cgColor
        text.layer.borderWidth = 1
    }
 
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @IBAction func viewPassword(_ sender: Any) {
        if password.isSecureTextEntry {
            self.viewImage.setImage(#imageLiteral(resourceName: "view-1"), for: .normal)
            password.isSecureTextEntry = false
        }else{
            self.viewImage.setImage(#imageLiteral(resourceName: "unview"), for: .normal)
            password.isSecureTextEntry = true
        }
    }
    
    @IBAction func LoginAction(_ sender: Any) {
        if(mobileNumber.text?.isEmpty ==  true || password.text?.isEmpty == true){
            self.LoginBtn.shake(duration: 0.5, values: [-12.0, 12.0, -12.0, 12.0, -6.0, 6.0, -3.0, 3.0, 0.0])
            ShowErrorMassge(massge: "Please complete your Data".localized, title: "Error".localized)
        }else{
            self.showProgress()
            let params: Parameters = [
                "phone": self.mobileNumber.text!,
                "password":self.password.text!,
                "device_id":AppDelegate.FCMTOKEN,
                "device_type": "ios"
            ]
       
            API.POST(url: Login_URL, parameters: params, headers: ["lang":getServerLang()]) { (success, value) in
                if success{
                    self.stopAnimating()
                    print("⏳\(params)")
                    print("🖍\(value)")
                    let key = value["key"] as? String
                    let msg = value["msg"] as? String
                    if(key == "success"){
                        let data = value["data"] as! [String:Any]
                        let active = data["active"] as? String
                        let token = data["token"] as? String
                        let phone = data["phone"] as? String
                        let lat = data["lat"] as? Double
                        let long = data["long"] as? Double
                        let id = data["id"] as? Int
                        let address = data["address"] as? String ?? ""
                        let avatar = data["avatar"] as? String
                        let name = data["name"] as? String
                        let email = data["email"] as? String
                        let delegate = data["delegate"] as! String
                        let time_zone = data["time_zone"] as! String
                        let googlekey = data["googlekey"] as! String

                        if(delegate == "true"){
                            defaults.set("yes", forKey: is_Delgate)
                        }else{
                            defaults.set("no", forKey: is_Delgate)
                        }
                        GMSServices.provideAPIKey(googlekey)
                        defaults.set(googlekey, forKey: GOOGLE_KEY)
                        defaults.set(email, forKey: User_Email)
                        defaults.set("no", forKey: is_Vistor)
                        defaults.set(address, forKey: User_Name)
                        defaults.set(time_zone, forKey: Time_zone)
                        defaults.set(address, forKey: User_Address)
                        defaults.set(token, forKey: Token_ID)
                        defaults.set(phone, forKey: Mobile_Number)
                        defaults.set(String(describing: id!), forKey: User_ID)
                        defaults.set(avatar, forKey: User_Avatar)
                        defaults.set(name, forKey: User_Name)
                        defaults.set(String(describing: lat!), forKey: User_Lat)
                        defaults.set(String(describing: long!), forKey: User_Lng)
                        
                        HomeVC.UserLat = lat
                        HomeVC.UserLong = long
                        
                        if(active == "active"){
                            defaults.set("yes", forKey: is_LoogedIn)
                            guard let window = UIApplication.shared.keyWindow else { return }
                            let vc = Storyboard.Home.viewController(TabBarViewController.self)
                            window.rootViewController = vc
                        }else if (active == "pending"){
                            ShowErrorMassge(massge: "You must activate the account first".localized, title: "Error".localized)
                            let vc = Storyboard.Authentication.viewController(VrefcationCodeViewController.self)
                            self.navigationController?.pushViewController(vc, animated: true)
                        }else if(active == "blocked"){
                            ShowErrorMassge(massge: msg!, title: "")
                        }
                     
                        }else{
                            ShowErrorMassge(massge: msg!, title: "Error".localized())
                        }
                        
                    }else{
                        self.stopAnimating()
                    }
                }

        }
    }
    
    @IBAction func LoginAsVestorAction(_ sender: Any) {
    
        guard let window = UIApplication.shared.keyWindow else { return }
        var vc = TabBarViewController()
        vc = Storyboard.Home.viewController(TabBarViewController.self)
        defaults.set("yes", forKey: is_Vistor)
        defaults.set("no", forKey: is_Delgate)
        defaults.set("", forKey: Token_ID)
        defaults.set("", forKey: User_ID)

        window.rootViewController = vc
        
      
       
    }
    @IBAction func RegisterAction(_ sender: Any) {
        let forgetPass = Storyboard.Authentication.viewController(RegisterViewController.self)
        self.navigationController?.pushViewController(forgetPass, animated: true)

    }
    @IBAction func forgetPasswordAction(_ sender: Any) {
        
        let forgetPass = Storyboard.Authentication.viewController(ForgetPasswordViewController.self)
        self.navigationController?.pushViewController(forgetPass, animated: true)

    }

}

extension LoginViewController : UIGestureRecognizerDelegate{
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
}
