//
//  AppColors.swift
//  Noon
//
//  Created by Engy Bakr on 12/25/19.
//  Copyright © 2019 Engy Bakr. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    static let BasicColor = #colorLiteral(red: 0.1647058824, green: 0.8, blue: 0.9607843137, alpha: 1)
    static let DeactiveBasicColor = #colorLiteral(red: 0.9529411765, green: 0.737254902, blue: 0.2666666667, alpha: 0.5765732021)

    static let SecondaryColor = #colorLiteral(red: 0.2547290921, green: 0.5679958463, blue: 0.6856873035, alpha: 1)
    static let FontColor = #colorLiteral(red: 0.2470588235, green: 0.2666666667, blue: 0.3254901961, alpha: 1)
    static let ButtonColor = #colorLiteral(red: 0.1450980392, green: 0.4666666667, blue: 0.7607843137, alpha: 1)
    static let OfferTextColor = #colorLiteral(red: 0.1568627451, green: 0.8039215686, blue: 0.2549019608, alpha: 1)
    static let LightGrayTextColor = #colorLiteral(red: 0.6509803922, green: 0.6588235294, blue: 0.6862745098, alpha: 1)
    static let MainBackground = #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9607843137, alpha: 1)


    static let ErrorColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
    static let WarningColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
    static let SuccessColor = #colorLiteral(red: 0.3529411765, green: 0.7843137255, blue: 0.9803921569, alpha: 1)
    
    static let ErrorBackgroundPinkColor = #colorLiteral(red: 1, green: 0.9411764706, blue: 0.8901960784, alpha: 1)
    static let ErrorPinkColor = #colorLiteral(red: 0.7333333333, green: 0.4078431373, blue: 0.4117647059, alpha: 1)
}
