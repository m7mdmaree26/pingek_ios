//
//  OrderAlertWatingVC.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/30/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit

class OrderAlertWatingVC: UIViewController {
    
    var vc : OrdersVC?

    @IBOutlet weak var loadingImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadingImage.rotate(duration: 1)
//        UIView.animate(withDuration: 1, delay: 0, options: .curveLinear, animations: { () -> Void in
//            self.loadingImage.transform = self.loadingImage.transform.rotated(by: .pi)
//        })
        DispatchQueue.main.asyncAfter(deadline: .now() + 3){
            self.dismiss(animated: true)
        }
    }
    
    @IBAction func cancelOrder(_ sender: Any){
        self.dismiss(animated: true)
        vc!.CancelOrder(order_id:vc!.selectedOrderId)
    }

}
