//
//  SavedPlaceModel.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/25/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import Foundation
class  SavedPlace{
    var id = ""
    var name = ""
    var address = ""
    var lat = ""
    var long = ""


    func getObject(dicc: [String: Any]) ->  SavedPlace {
        let dic = HandleJSON.getObject().handle(dicc: dicc)
        let model =  SavedPlace()
        model.id = dic["id"] as! String
        model.name = dic["name"] as! String
        model.address = dic["address"] as! String
        model.lat = dic["lat"] as! String
        model.long = dic["long"] as! String

        return model
        
    }
}
