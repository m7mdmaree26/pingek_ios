//
//  ComplaintsViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/26/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Photos
import OpalImagePicker


class ComplaintsViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    var imagesData = [Data]()
    var imagesKeys = [String]()
    var flagImage = false
    var imageStr = ""
    var imageData:Data!
    var orderId : String?
    var imagesArray:[UIImage] = []

    
    @IBOutlet weak var userImage: UIButton!
    @IBOutlet weak var imageCollection: UICollectionView!

    @IBAction func uploadImageAction(_ sender: Any) {
        
        if orderId != nil {
            let imagePicker = OpalImagePickerController()
            imagePicker.imagePickerDelegate = self
            imagePicker.maximumSelectionsAllowed = 5
            imagePicker.selectionTintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.5706335616)
            imagePicker.selectionImage = #imageLiteral(resourceName: "checked")
            present(imagePicker, animated: true, completion: nil)
        }else{
            print("😃🔍hiiiiii")
            var myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(myPickerController, animated: false, completion: nil)
        }
    }
    
    @IBOutlet weak var viewTitle: UILabel!
    @IBOutlet weak var sendBtn: RoundedButton!
    @IBOutlet weak var imageComp: UIImageView!
    @IBOutlet weak var MassgeBody: UITextView!
    @IBOutlet weak var MassgeTitle: BorderBaddedTextField!
    @IBOutlet weak var backButton: UIButton!

    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendbtnAction(_ sender: Any) {
        if(self.MassgeTitle.text?.isEmpty == true || self.MassgeBody.text?.isEmpty == true){
            self.sendBtn.shake(duration: 0.5, values: [-12.0, 12.0, -12.0, 12.0, -6.0, 6.0, -3.0, 3.0, 0.0])
            ShowErrorMassge(massge: "Please complete your Data".localized, title: "Error".localized)
        }else{
//            if(flagImage == false){
//                imagesData = []
//                imagesKeys = []
//            }else{
//                imagesData.append(self.imageData!)
//                imagesKeys.append("image")
//
//            }
            
            if orderId != nil {
                for i in imagesArray{
                    self.imagesData.append((i).pngData()! as Data)
                      print("😆\(i)")
                    imagesKeys.append("images[]")
                }
            }else{
                imagesData.append(self.imageData!)
                imagesKeys.append("image")
           
            }
            self.AddComp(subject: self.MassgeTitle.text!, message: self.MassgeBody.text!)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.hideKeyboardWhenTappedAround()
        self.navigationController?.navigationBar.topItem?.title = ""
        self.imageComp.clipsToBounds = true
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)
        self.imageComp.clipsToBounds = true
        if orderId != nil {
            self.viewTitle.text = "Sent complaint".localized
        }


    }
    override func viewWillAppear(_ animated: Bool) {
       self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name:  "JFFlat-Medium", size: 15)!]
    }
    func imagePickerController(_ picker:UIImagePickerController,didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]){
        if let imageURL = info[.imageURL] as? URL {
        let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
        let asset = result.firstObject
        
        }
        let image_data = info[.originalImage] as! UIImage
        self.imageData = (image_data).pngData()! as Data
        self.imageComp.image = image_data
        imageStr = imageData.base64EncodedString()
        userImage.backgroundColor = UIColor.clear
        self.dismiss(animated: true, completion: nil)
        self.userImage.setImage( nil, for:.normal)
        flagImage = true
        
    }
  
    func AddComp(subject:String,message:String){
        self.showProgress()
        var parmas:Parameters = [
            "user_id":getUserID(),
            "subject":subject
        ]
        var url = ""
        if orderId != nil {
            parmas.updateValue(self.orderId!, forKey: "order_id")
            parmas.updateValue(message, forKey: "text")
            url = createTicket_URL
        }else{
            parmas.updateValue(message, forKey: "message")
            url = CreateComp_URL
        }
        
        API.POSTImage(url: url , Images: imagesData, Keys: imagesKeys, header: ["Authorization":"Bearer" + getTokenId()], parameters: parmas) { (succss, value) in
            if succss{
                self.stopAnimating()
                print("🌸\(value)")
                let key = value["key"] as? String
                let msg = value["msg"] as? String
                if key == "success"{
                    ShowTrueMassge(massge: msg!, title: "Done".localized)
                    self.MassgeTitle.text = ""
                    self.MassgeBody.text = ""
                    self.imageComp.image = nil
                    self.navigationController?.popViewController(animated: true)
                }else{
                    ShowErrorMassge(massge: msg!, title: "Error".localized)
                }
                self.stopAnimating()
            }else{
                self.stopAnimating()
                ShowErrorMassge(massge: "Something went wrong and tried again".localized, title: "Error".localized)
            }
        }
    }
    

}
extension ComplaintsViewController: OpalImagePickerControllerDelegate {
    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
    }
    
    func imagePickerNumberOfExternalItems(_ picker: OpalImagePickerController) -> Int {
        return 1
    }
    
    func imagePickerTitleForExternalItems(_ picker: OpalImagePickerController) -> String {
        return NSLocalizedString("External", comment: "External (title for UISegmentedControl)")
    }
    
    func imagePicker(_ picker: OpalImagePickerController, imageURLforExternalItemAtIndex index: Int) -> URL? {
        return URL(string: "https://placeimg.com/500/500/nature")
    }
    ////////==========>>>>
   
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        self.imagesArray = images
        self.imageCollection.reloadData()
        self.userImage.setImage( nil, for:.normal)
        
        
        presentedViewController?.dismiss(animated: true, completion: nil)
        print("💙💙\(images)")

        
      flagImage = true
    }
}
extension ComplaintsViewController:UICollectionViewDelegate ,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.imagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagesCollectionViewCell", for: indexPath) as! ImagesCollectionViewCell
           cell.CurrentImage.image = self.imagesArray[indexPath.row]
           cell.deleteButton.addTarget(self, action: #selector(deleteImage), for: .touchUpInside)
           cell.deleteButton.tag = indexPath.row
        
        
           return cell
    }
    
    @objc func deleteImage(buttonTag:UIButton){
        print("🤔\(buttonTag.tag)")
        self.imagesArray.remove(at: buttonTag.tag)
        self.imageCollection.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
               return CGSize(width: 100 , height:100)
    }
}
