//
//  OrderDetilsViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/9/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import MIAlertController
import Cosmos

class OrderDetilsViewController: UIViewController ,UITableViewDataSource ,UITableViewDelegate{
    var OrdersDataSourse = OrderDetilsModel()
    var imageLink = ""
    var resivdAdd = ""
    var delverAdd = ""
    var resName = ""
    var distnsfromStore = ""
    var distansfromClint = ""
    var delverTime = ""
    var orderId = ""
    var ExpectedMsg = ""
    var Resivelat = ""
    var ResiveLong = ""
    var Delgatelat = ""
    var DelgateLng = ""
    var MaxExpixtedPrice = ""
    var MinExpixtedPrice = ""
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var ratingBar: CosmosView!
    @IBOutlet weak var readyBtn: UIButton!
    @IBOutlet weak var backButton: UIButton!
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)

        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.estimatedRowHeight =  UIScreen.main.bounds.height/2
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tabBarController?.tabBar.isHidden = true
        navigationController?.navigationBar.barTintColor = UIColor.black
        self.showProgress()
        getOrderDetils(order_id:self.orderId)
        self.readyBtn.isEnabled = false
    }
    @IBAction func goback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func readytodeliver(_ sender: Any) {
        let Reg = Storyboard.Home.viewController(ReadytoDeliverViewController.self)
        Reg.deliverAdd = self.delverAdd
        Reg.resiveAdd = self.resivdAdd
        Reg.distnsfromStore = self.distnsfromStore
        Reg.distansfromClint = self.distansfromClint
        Reg.resName = self.resName
        Reg.delverTime = self.delverTime
        Reg.Delgatelat = self.Delgatelat
        Reg.DelgateLng =  self.DelgateLng
        Reg.ResiveLong = self.ResiveLong
        Reg.Resivelat = self.Resivelat
        Reg.orderId = self.orderId
        Reg.ExpixtedMsg = self.ExpectedMsg
        Reg.MaxExpixtedPrice = self.MaxExpixtedPrice
        Reg.MinExpixtedPrice = self.MinExpixtedPrice
        Reg.app_percentage = self.OrdersDataSourse.app_percentage
        self.navigationController?.pushViewController(Reg, animated: false)
    }
    
    ////////////////==================>>>>>>> TableView((SecondView))
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.OrdersDataSourse.details.count
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let m = OrdersDataSourse.details[indexPath.section]
        if(m.key == "text"){
            return UITableView.automaticDimension
        }else{
            return 220
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetilsTableViewCell", for: indexPath) as! OrderDetilsTableViewCell
        if(self.OrdersDataSourse.details[indexPath.section].key == "text"){
            cell.massgeLbl.text = self.OrdersDataSourse.details[indexPath.section].value
        }else{
            cell.massgeImage.setImageWith(self.OrdersDataSourse.details[indexPath.section].value)
            cell.massgeLbl.isHidden = true
            cell.continerView.isHidden = true
        }
        cell.userImage.setImageWith(imageLink)
        return cell
    }
    
    @IBAction func sentComplaint(_ sender: Any) {
        let Reg = Storyboard.Home.viewController(ComplaintsViewController.self)
        Reg.orderId = orderId
        self.navigationController?.pushViewController(Reg, animated: false)
    }
    func getOrderDetils(order_id:String){
        
        let parmas :Parameters = [
            "lat": getUserLat(),
            "long":getUserLong(),
            "order_id":order_id
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        API.POST(url: OPenOrder_URL, parameters: parmas, headers: header) { (succss, value) in
            print("🤭\(parmas)")
            print("💙\(value)")
            
            if succss{
                let key = value["key"] as? String
                
                if(key == "success"){
                    self.stopAnimating()
                    self.readyBtn.isEnabled = true
                    let data = value["data"] as! [String:Any]
                    
                    
                    self.OrdersDataSourse = OrderDetilsModel().getObject(dicc: data)
                    self.resivdAdd = self.OrdersDataSourse.receive_address
                    self.delverAdd = self.OrdersDataSourse.deliver_address
                    self.ratingBar.rating = Double(self.OrdersDataSourse.rating)!
                    self.distnsfromStore = self.OrdersDataSourse.distance_to_store + " " + "Km".localized()
                    self.distansfromClint = self.OrdersDataSourse.distance_to_client  + " " + "Km".localized()
                    self.resName = self.OrdersDataSourse.store_name
                    self.delverTime = self.OrdersDataSourse.deliver_time
                    self.ResiveLong = self.OrdersDataSourse.receive_long
                    self.Resivelat =  self.OrdersDataSourse.receive_lat
                    self.DelgateLng =  self.OrdersDataSourse.deliver_long
                    self.Delgatelat =  self.OrdersDataSourse.deliver_lat
//                    if self.OrdersDataSourse.price != ""{
//                        self.delivaryPrice.text = "Delivary cost".localized + self.OrdersDataSourse.price + "SAR".localized
//                        self.delivaryIcon.isHidden = false
//                    }else{
//                        self.delivaryIcon.isHidden = true
//                    }
                    self.userName.text = self.OrdersDataSourse.username
                    self.userImage.setImageWith(self.OrdersDataSourse.avatar)
                    self.imageLink = self.OrdersDataSourse.avatar
                    self.ExpectedMsg = self.OrdersDataSourse.expected_price_msg
                    self.MaxExpixtedPrice = self.OrdersDataSourse.max_expected_price
                    self.MinExpixtedPrice = self.OrdersDataSourse.min_expected_price
                    self.tableView.animateTable()
                }
                
            }else{
                self.stopAnimating()
                ShowErrorMassge(massge: "Something went wrong and tried again".localized(), title: "Error".localized())            }
        }
    }
    
    
}
