//
//  NewOrderDelegates.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/23/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import Foundation
import OpalImagePicker



extension NewOrderViewController: OpalImagePickerControllerDelegate {
    func imagePickerDidCancel(_ picker: OpalImagePickerController) {
    }
    
    func imagePickerNumberOfExternalItems(_ picker: OpalImagePickerController) -> Int {
        return 1
    }
    
    func imagePickerTitleForExternalItems(_ picker: OpalImagePickerController) -> String {
        return NSLocalizedString("External", comment: "External (title for UISegmentedControl)")
    }
    
    func imagePicker(_ picker: OpalImagePickerController, imageURLforExternalItemAtIndex index: Int) -> URL? {
        return URL(string: "https://placeimg.com/500/500/nature")
    }
    ////////==========>>>>
   
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage]) {
        self.imagesArray = images
        
        
        presentedViewController?.dismiss(animated: true, completion: nil)
        self.imageCollection.reloadData()
        print("💙💙\(images)")

        
      flagImage = true
    }
}

extension NewOrderViewController:UICollectionViewDelegate ,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.imagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagesCollectionViewCell", for: indexPath) as! ImagesCollectionViewCell
           cell.CurrentImage.image = self.imagesArray[indexPath.row]
           cell.deleteButton.addTarget(self, action: #selector(deleteImage), for: .touchUpInside)
           cell.deleteButton.tag = indexPath.row
        
        
           return cell
    }
    
    @objc func deleteImage(buttonTag:UIButton){
        print("🤔\(buttonTag.tag)")
        self.imagesArray.remove(at: buttonTag.tag)
        self.imageCollection.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
               return CGSize(width: 100 , height:100)
    }
}
