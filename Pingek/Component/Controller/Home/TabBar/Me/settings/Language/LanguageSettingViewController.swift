//
//  LanguageSettingViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/26/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
class LanguageSettingViewController: UIViewController {
    let paragraphStyle = NSMutableParagraphStyle()
    var Language = ""
    var langArray = ["English" , "عربي"]
    @IBOutlet weak var backButton: UIButton!
    @IBAction func changLangAction(_ sender: Any) {

        
        let optionMenu = UIAlertController(title: "Change Language".localized, message: nil, preferredStyle: .actionSheet)
        
        let action1 = UIAlertAction(title: "English", style: .default, handler: {
            [unowned self] (alert: UIAlertAction!) -> Void in
            if AppLanguage.currentLanguage().contains("ar"){
                AppLanguage.setAppLanguage(lang: "en")
                Localizer.DoTheExhange()
    
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
                UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
                UITableView.appearance().semanticContentAttribute = .forceLeftToRight
                UIImageView.appearance().semanticContentAttribute = .forceLeftToRight
                UITextField.appearance().semanticContentAttribute = .forceLeftToRight
                guard let window = UIApplication.shared.keyWindow else { return }
                
                let vc =  Storyboard.Home.viewController(TabBarViewController.self)
                window.rootViewController = vc
            }
            
        })
        let action2 = UIAlertAction(title: "العربية".localized, style: .default, handler: {
            [unowned self] (alert: UIAlertAction!) -> Void in
            
            if AppLanguage.currentLanguage().contains("en"){
                AppLanguage.setAppLanguage(lang: "ar")
                Localizer.DoTheExhange()
                UIView.appearance().semanticContentAttribute = .forceRightToLeft
                UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
                UITableView.appearance().semanticContentAttribute = .forceRightToLeft
                UIImageView.appearance().semanticContentAttribute = .forceRightToLeft
                UITextField.appearance().semanticContentAttribute = .forceRightToLeft


               guard let window = UIApplication.shared.keyWindow else { return }
                
                let vc =  Storyboard.Home.viewController(TabBarViewController.self)
                window.rootViewController = vc
            }
            
            
        })
        let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil)
        
        optionMenu.addAction(action1)
        optionMenu.addAction(action2)
        optionMenu.addAction(cancelAction)
        
        present(optionMenu, animated: true, completion: nil)
        optionMenu.view.tintColor =  UIColor.BasicColor
    }
 
    @objc func actionPickerCancel(){
        
        print("Cancel")
    }
    
    
    @IBOutlet weak var languageText: BorderBaddedTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
         //EditTextDesign(text: languageText)
        self.navigationController?.navigationBar.topItem?.title = ""
        if(getServerLang() == "ar"){
            self.languageText.text = "عربي"
        }else{
            self.languageText.text = "English"
        }
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)
        self.languageText.setLeftPaddingPoints(10)
        self.languageText.setRightPaddingPoints(10)

    }
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func restartApplication () {
        let viewController = Storyboard.Home.viewController(TabBarViewController.self)
        let navCtrl = UINavigationController(rootViewController: viewController)

        guard
                let window = UIApplication.shared.keyWindow,
                let rootViewController = window.rootViewController
                else {
            return
        }

        navCtrl.view.frame = rootViewController.view.frame
        navCtrl.view.layoutIfNeeded()

        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
            window.rootViewController = navCtrl
        })

    }
}
