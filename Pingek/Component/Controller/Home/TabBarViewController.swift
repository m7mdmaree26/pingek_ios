//
//  TabBarViewController.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/16/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import SOTabBar


class TabBarViewController : UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "JFFlat-Medium", size: 12)!], for: .normal)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name:  "JFFlat-Medium", size: 15)!]
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "JFFlat-Medium", size: 15)!], for: UIControl.State.normal)
        
        guard let tabItems = self.tabBar.items else { return }
        
        tabItems[0].image = UIImage(named: "noun_homee")
        tabItems[0].selectedImage = UIImage(named: "noun_homeee")
        tabItems[0].title = "Home".localized
        tabItems[1].image = UIImage(named: "choices")
        tabItems[1].selectedImage = UIImage(named: "choices_active")
        tabItems[1].title = "My Orders".localized
        tabItems[2].image = UIImage(named: "gift")
        tabItems[2].selectedImage = UIImage(named: "gift_active")
        tabItems[2].title = "My Delegates".localized
        tabItems[3].image = UIImage(named: "bell-1")
        tabItems[3].selectedImage = UIImage(named: "bell_active")
        tabItems[3].title = "Notifications".localized
        tabItems[4].image = UIImage(named: "user")
        tabItems[4].selectedImage = UIImage(named: "user_active")
        tabItems[4].title = "Me".localized
    }



}
