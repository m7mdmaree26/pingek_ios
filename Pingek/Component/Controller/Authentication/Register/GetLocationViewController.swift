//
//  GetLocationViewController.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/27/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import MapKit

protocol sendDataBackDelegate {
    func finishPassing(location: String , lat:Double , lng:Double)
}

protocol sendDataBackDelegateTwo {
    func finishPassing2(location: String , lat:Double , lng:Double)
}

class GetLocationViewController: UIViewController , UIGestureRecognizerDelegate , GMSMapViewDelegate{
    
    @IBOutlet weak var confirmationBtn: RoundedButton!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var googleMap: GMSMapView!
    @IBOutlet weak var backButton: UIButton!
    
    
    let locationManager = CLLocationManager()
    var matchingItems: [MKMapItem] = [MKMapItem]()
    var delegate: sendDataBackDelegate?
    var delegate2: sendDataBackDelegateTwo?
    let Storeannotation = ImageAnnotation()
    let userAnnotaion = ImageAnnotation()
    var ResultArray = [SearchDataModel]()
    var lat:Double?
    var lng:Double?
    var marker = GMSMarker()

    
    
    override func viewWillAppear(_ animated: Bool) {
        requestLocationAccess()
        requestLocationAccess()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        setMapview()
    }
    
    func setMapview() {
        
        requestLocationAccess()
        googleMap.delegate = self
        locationManager.delegate = self
        locationManager.requestLocation()
        locationManager.startUpdatingLocation()
        self.locationLabel.text = "Your Location".localized
        self.confirmationBtn.setTitle("Confirmation".localized, for: .normal)
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)

    }
    
   @IBAction func goBack(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
   }

    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        self.lat = coordinate.latitude
        self.lng = coordinate.longitude
        print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
        marker.position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
        getAddress(lat:coordinate.latitude , Lng: coordinate.longitude)
                
    }
    
   func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
      returnPostionOfMapView(mapView: mapView)
   }


   //Convert the location position to address
   func returnPostionOfMapView(mapView:GMSMapView){
        self.lat = mapView.camera.target.latitude
        self.lng = mapView.camera.target.longitude
        let position = CLLocationCoordinate2DMake(self.lat!, self.lng!)
        marker.position = position
        getAddress(lat:self.lat! , Lng: self.lng!)
   }
    
    func getAddress(lat: Double , Lng: Double){
        
        var formatted_address: String!
        API.GET(url: URLs.Google_GeoCoding + "\(lat),\(Lng)&key=\(defaults.string(forKey: GOOGLE_KEY)!)&language=\(getServerLang())") { (success, result) in
            if success {
                let dict = result
                if let status = dict["status"] as? String {
                    if status == "OK" {
                        if let results = dict["results"] as? [[String:AnyObject]] {
                            let resultAddress = results[0]
                            let fullAddress = resultAddress["formatted_address"] as! String
                            formatted_address = fullAddress.replacingOccurrences(of: ",", with: " ").replacingOccurrences(of: "-", with: " ")
                            print("Address:" + formatted_address)
                            self.locationLabel.text = formatted_address
                        }
                    } else {
                        
                    }
                }
            } else {
                // Failed to Call API
                
            }
        }
    }
    
    
    @IBAction func chooseLocationBtnPressed(_ sender: Any) {
        if self.locationLabel.text?.isEmpty == false  {
            if (self.locationLabel.text == "موقعك" || self.locationLabel.text == "Your Location") {
                Alert.showAlertOnVC(target: self, title: "يجب اختيار موقع", message: "قم بتحديد الموقع علي الخريطة")
            }else{
                delegate?.finishPassing(location: locationLabel.text!, lat: self.lat! , lng: self.lng!)
                delegate2?.finishPassing2(location: locationLabel.text!, lat: self.lat!, lng: self.lng!)
                self.navigationController?.popViewController(animated: true)
            }
           
        }
        else{
            Alert.showAlertOnVC(target: self, title: "يجب اختيار موقع", message: "قم بتحديد الموقع علي الخريطة")
        }
    }
    
    func requestLocationAccess() {
        let status = CLLocationManager.authorizationStatus()
        
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            return
            
        case .denied, .restricted:
            print("location access denied")
            
        default:
            locationManager.requestWhenInUseAuthorization()
        }
    }
    let regionRadius: CLLocationDistance = 100
    func centerMapOnLocation(location: CLLocation){
        
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 13.0)
        self.googleMap.camera = camera

        marker.position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        marker.icon = #imageLiteral(resourceName: "maps_anflags-1")
        marker.map = googleMap
        
        self.lat = location.coordinate.latitude
        self.lng = location.coordinate.longitude
        getAddress(lat: location.coordinate.latitude , Lng: location.coordinate.longitude)
    }
 
    
}

extension GetLocationViewController : CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("listen")
        centerMapOnLocation(location: locationManager.location!)
        self.locationManager.stopUpdatingLocation()
        
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        Alert.showAlertOnVC(target: self, title: "Make sure GPS is open".localized, message: "")
    }
}

