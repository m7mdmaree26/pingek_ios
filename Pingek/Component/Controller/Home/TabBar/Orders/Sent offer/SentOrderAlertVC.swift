//
//  SentOrderAlertVC.swift
//  Sandkom
//
//  Created by Engy Bakr on 2/5/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit

class SentOrderAlertVC: UIViewController {
        

    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.view1.isHidden = true
        // Do any additional setup after loading the view.
    }
    

    
    @IBAction func goToHome(_ sender: Any) {
        self.dismiss(animated: true)
        guard let window = UIApplication.shared.keyWindow else { return }
        let vc = Storyboard.Home.viewController(TabBarViewController.self)
        window.rootViewController = vc
    }
    
    @IBAction func withdraw(_ sender: Any) {
        self.view1.isHidden = false
        self.view2.isHidden = true
    }
    
    
}
