//
//  MapTableViewCell.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/23/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit
import MapKit

class MapTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ResiverWidth: NSLayoutConstraint!
    @IBOutlet weak var senderwidth: NSLayoutConstraint!
    @IBOutlet weak var UserResveImage: UIImageView!
    @IBOutlet weak var UserSenderImage: UIImageView!
    @IBOutlet weak var mapImage: UIImageView!
    var artwork = Artwork(title: "", locationName: "", discipline: "", coordinate:  CLLocationCoordinate2D(latitude: 0.0, longitude:0.0))
    override func awakeFromNib() {
        super.awakeFromNib()
        self.ResiverWidth.constant = 50
        self.senderwidth.constant = 50
        self.mapImage.clipsToBounds = true
        
    }
    override func prepareForReuse() {
        self.UserSenderImage.image = nil
        self.UserResveImage.image = nil
       
       
    }
    
    func getSnapShotMap(lat:Double,Long:Double){
        
        let mapSnapshotOptions = MKMapSnapshotter.Options()
        
        // Set the region of the map that is rendered.
        let location = CLLocationCoordinate2DMake(lat, Long) // Apple HQ
        let region = MKCoordinateRegion(center: location, latitudinalMeters: 500000, longitudinalMeters: 500000)
        mapSnapshotOptions.region = region
        
        // Set the scale of the image. We'll just use the scale of the current device, which is 2x scale on Retina screens.
        mapSnapshotOptions.scale = UIScreen.main.scale
        
        // Set the size of the image output.
        mapSnapshotOptions.size = CGSize(width: 400, height: 400)
        
        // Show buildings and Points of Interest on the snapshot
        mapSnapshotOptions.showsBuildings = true
        mapSnapshotOptions.showsPointsOfInterest = true
        
        let snapShotter = MKMapSnapshotter(options: mapSnapshotOptions)
        snapShotter.start { (snapshot:MKMapSnapshotter.Snapshot?, errror) in
            self.mapImage.image = snapshot?.image
        }
    }
    
    
}
