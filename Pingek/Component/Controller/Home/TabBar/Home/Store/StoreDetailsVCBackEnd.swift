//
//  StoreDetailsVCBackEnd.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/22/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import Alamofire


extension StoreDetailsVC {
    
    func getStoreDetailst(){
        self.showProgress()
         var params:Parameters = [
             "lat":UserLat,
             "long":UserLong,
         ]
        var header:Dictionary = [String:Any]()
        if(getoIsVistor() == "yes"){
             header = [
                "lang":getServerLang()
                
            ]
        }else{
            header = [
                "Authorization":"Bearer" + getTokenId(),
                "lang":getServerLang()
            ]
        }
        if self.notificaion != nil {
            if self.notificaion?.place_ref == "" {
                params.updateValue(self.notificaion!.place_id, forKey: "store_id")
            }
            
        }else{
            params.updateValue(self.store.place_id, forKey: "store_id")
        }
        
       
         API.POST(url: storeDetails_URL, parameters: params, headers: header, withIndicator: false) { (succss, value) in
             if succss{
                 self.stopAnimating()
                 print("🤭\(value)")
                 let key = value["key"] as! String
                 if (key == "success"){
                    self.branches.removeAll()
                    let data = value["data"] as! [String:Any]
                    let store = data["store"] as! [String:Any]
                    let storeBranches = data["branches"] as! [[String:Any]]
                    let storeDetails = StoreDetailsModel().getObject(dicc: store)
                    self.storeDetails = storeDetails
                    self.storeName.text = storeDetails.name
                    self.storeImage.setImageWith(storeDetails.icon)
                    self.ratesView.rating = Double(storeDetails.rate)!
                    self.sharingCount.text = storeDetails.num_comments + "Sharing".localized
                    self.noOfRates.text = "(" + storeDetails.num_rating + ")"
                    self.storeCoverImage.setImageWith(storeDetails.cover)
                    self.storeLocation.text = storeDetails.address
                    self.storeDistance.text = "far about".localized + storeDetails.distance
                    //self.storeOpeningTimes.text = storeDetails.opening_hours
                    self.menuCollection.reloadData()
                    
                    for i in storeBranches{
                        self.branches.append(BranchesModel().getObject(dicc: i))
                    }
                 }
                 
             }else{
                 self.stopAnimating()
             }
             
         }
     }
      
    
    ////////================>>>>>>
       func getNearstProviders(RestruntRef:String,RestruntID:String,user_id:String?,tokenId:String?){
        
            self.showProgress()

            var params:Dictionary = [String:Any]()
            var header:Dictionary = [String:Any]()
            if(getoIsVistor() == "yes"){
               params = [
                   "lat": getUserLat(),
                   "long":getUserLong(),
                   "place_ref":RestruntRef,
                   "place_id":RestruntID,
               ]
               
                header = [
                   "lang":getServerLang()
                   
               ]
           }else{
                params = [
                   "lat": getUserLat(),
                   "long":getUserLong(),
                   "place_ref":RestruntRef,
                   "place_id":RestruntID,
                   "user_id": user_id!,
               ]
               header = [
                   "Authorization":"Bearer" + tokenId!,
                   "lang":getServerLang()
               ]
           }
    
           API.POST(url: nearProviders_URL, parameters: params, headers: header) { (succss, value) in
               
               if succss{
                   self.stopAnimating()
                   let key = value["key"] as? String
                   if(key == "success"){
                        let data = value["data"] as! [String:Any]
                        let place_address = data["place_address"] as! String
                        let place_name = data["place_name"] as! String
                        let place_lng = data["place_lng"] as! Double
                        let place_lat = data["place_lat"] as! Double
                        let distance = data["distance"] as! Double
                        let place_website = data["place_website"] as! String

                        let opening_hours = data["opening_hours"] as! [String]
                        self.OpnningHoue = opening_hours
                        self.storeAddress.text = place_address
                        self.storeNameLbl.text = place_name
                        self.place_website = place_website
                        self.addMarkerToMap(lat: place_lat,lng: place_lng,image : #imageLiteral(resourceName: "store"),title : String(describing:  distance))
   

//                           self.ProviderDelgate = true
//                       }else{
//                            self.ProviderDelgate = false
//                       }
//
//                       if(delegate_place == "false"){
//                           self.cheackBox.isChecked = false
//                           self.RequstInformation.isHidden = true
//                           self.RequstPadding.isHidden = true
//                           self.topSpace.constant = 0.0
//                       }else{
//                           self.cheackBox.isChecked = true
//                           self.RequstInformation.isHidden = false
//                           self.RequstPadding.isHidden = false
//                           self.topSpace.constant = 58.0
//                       }
//
                       self.daystableView.animateTable()
                   }
                  
               }else{
                    self.stopAnimating()
                   //   print("🍉")
               }
           }
       }
    
    func getWattingOrders(){
        
        self.showProgress()
        self.OrdersDataSourse.removeAll()
           var parmas :Parameters = [
               "lat": getUserLat(),
               "long":getUserLong(),
               ]
        if self.notificaion == nil{
            if store.reference == "" {
                parmas.updateValue(self.store.place_id, forKey: "store_id")
            }else{
                parmas.updateValue(self.store.place_id, forKey: "place_id")
                parmas.updateValue(self.store.reference, forKey: "place_ref")
            }
        }else{
            if self.notificaion?.place_ref == "" {
                parmas.updateValue(self.notificaion!.place_id, forKey: "store_id")

            }else{
                parmas.updateValue(self.notificaion!.place_id , forKey: "place_id")
                parmas.updateValue(self.notificaion!.place_ref , forKey: "place_ref")
            }
        }
        
        
           let header:HTTPHeaders = [
               "Authorization":"Bearer" + getTokenId(),
               "lang":getServerLang()
           ]
           API.POST(url: PlaceWatingOrders_URL, parameters: parmas, headers: header) { (succss, value) in
               print("🤭\(parmas)")
               if succss{
                   self.stopAnimating()
                   let key = value["key"] as? String
                  print("🌸\(value)")
                   if(key == "success"){
                       let data = value["data"] as! [[String:Any]]
                       self.OrdersDataSourse = []
                       if(data.count == 0){
                           self.noRequstLbl.isHidden = false
                       }else{
                           self.noRequstLbl.isHidden = true

                           for i in data{
                            self.OrdersDataSourse.append(wattingOrders().getObject(dicc: i))

                           }
                         print("💙\(self.OrdersDataSourse.count)")
                       self.tableView.animateTable()
                   }
                   }
                   
               }else{
                   self.stopAnimating()
               }
           }
       }
}
