//
//  Constants.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/20/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import Foundation
import UIKit
let defaults = UserDefaults.standard
let Lang_ar = "ar"
let Lang_en = "en"
let textFiledWidth = ((UIScreen.main.bounds.width)/1.2)
let iosMinumumLocalizationMirror  = "10"
let User_Lat  = "User_Lat"
let User_Lng  = "User_Lng"
let User_ID  = "User_ID"
let Token_ID  = "Token_ID"
let User_Avatar  = "User_Avatar"
let User_Name  = "User_Name"
let Mobile_Number  = "Mobile_Number"
let User_Email  = "User_Email"
let GOOGLE_KEY  = "GOOGLE_KEY"
let User_Address  = "User_Address"
let orderDetilsFrom  = "orderDetilsFrom"
let is_Delgate  = "is_Delgate"
let is_Vistor  = "is_Vistor"
let is_LoogedIn  = "is_LoogedIn"
let globalConvirstionId  = "globalConvirstionId"
let convirsationFrom  = "convirsationFrom"
let LunchFrom  = "LunchFrom"
let AllowkmPrice  = "AllowkmPrice"
let nearOrderfrom  = "nearOrderfrom"
let ViewOrderfrom  = "ViewOrderfrom"
let GlobalBid  = "GlobalBid"
let Time_zone = "time_zone"

func getTimeZone() -> String{
    return   String(UserDefaults.standard.string(forKey: Time_zone)!)
}
func getGlobalBid() -> String{
    return   String(UserDefaults.standard.string(forKey: GlobalBid)!)
}

func getViewOrder() -> String{
    return   String(UserDefaults.standard.string(forKey: ViewOrderfrom)!)
}

func getFromnearOrder() -> String{
    return   String(UserDefaults.standard.string(forKey: nearOrderfrom)!)
}
func getAllowkmPrice() -> Int{
    
    return  UserDefaults.standard.integer(forKey: AllowkmPrice)
}

func getLunchFrom() -> String{
    
    return  String(UserDefaults.standard.string(forKey: LunchFrom)!)
}

func getglobalConvirstionId() -> String{
    
    return  String(UserDefaults.standard.string(forKey: globalConvirstionId)!)
}

func getconvirsationFrom() -> String{
    
    return  String(UserDefaults.standard.string(forKey: convirsationFrom)!)
}

func getisLoogedIn() -> String{
    
    return  String(UserDefaults.standard.string(forKey: is_LoogedIn)!)
}

func getisDelgate() -> String{
    return  String(UserDefaults.standard.string(forKey: is_Delgate)!)
}

func getoIsVistor() -> String{
    
    return  String(UserDefaults.standard.string(forKey: is_Vistor)!)
}
func getorderDetilsFrom() -> String{
    
    return  String(UserDefaults.standard.string(forKey: orderDetilsFrom)!)
}
func getUserAddress() -> String{
    
    return  String(UserDefaults.standard.string(forKey: User_Address)!)
}
func getUserLat() -> String{
    if  UserDefaults.standard.object(forKey: User_Lat) != nil{
        return  String(UserDefaults.standard.string(forKey: User_Lat)!)

    }else{
        return ""
    }
}
func getUserLong() -> String{
   if  UserDefaults.standard.object(forKey: User_Lng) != nil{
        return  String(UserDefaults.standard.string(forKey: User_Lng)!)

    }else{
        return ""
    }
}

func getUserID() -> String{
    
    return  UserDefaults.standard.string(forKey: User_ID)!
}
func getTokenId() -> String{
    
    return  String(UserDefaults.standard.string(forKey: Token_ID)!)
}
func getUserAvatar() -> String{
    
    return  String(UserDefaults.standard.string(forKey: User_Avatar)!)
}
func getUserName() -> String{
    
    return  String(UserDefaults.standard.string(forKey: User_Name)!)
}
func getMobileNumber() -> String{
    
    return  String(UserDefaults.standard.string(forKey: Mobile_Number)!)
}
func getUSerEmail() -> String{
    
    return  String(UserDefaults.standard.string(forKey: User_Email)!)
}
