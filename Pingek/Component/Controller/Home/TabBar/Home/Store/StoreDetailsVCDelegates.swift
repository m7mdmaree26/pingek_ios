//
//  StoreDetailsVCDelegates.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/22/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import EzPopup


extension StoreDetailsVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.storeDetails.menus.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoreMenuCollectionCell", for: indexPath) as! StoreMenuCollectionCell
        cell.imageItem.setImageWith(self.storeDetails.menus[indexPath.row])
        return cell
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 70 , height:70)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         let vc = Storyboard.Home.viewController(ShowImageVC.self)
            vc.image = self.storeDetails.menus[indexPath.row]
            vc.storeWeb = self.storeDetails.website
            vc.storePho = self.storeDetails.phone
                   
            let popupVC = PopupViewController(contentController: vc, popupWidth: UIScreen.main.bounds.width, popupHeight:  UIScreen.main.bounds.height)
                               
            popupVC.backgroundAlpha = 0.5
            popupVC.backgroundColor = .black
            popupVC.canTapOutsideToDismiss = true
            popupVC.cornerRadius = 0
            popupVC.shadowEnabled = true
                                   
                                   
            self.present(popupVC, animated: true)
    }
        
}


extension StoreDetailsVC :UITableViewDataSource ,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        if(tableView == daystableView){
         return  1
        }
        return self.OrdersDataSourse.count
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == daystableView){
            return self.OpnningHoue.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(tableView == daystableView){
            return  20
        }else{
             return 100
        }
 
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == daystableView){
            let cell = daystableView.dequeueReusableCell(withIdentifier: "TimeTableViewCell", for: indexPath) as! TimeTableViewCell
            cell.textLabel?.text = self.OpnningHoue[indexPath.row]
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            return cell
    
        }else{
          
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "PaddingRequstTableViewCell", for: indexPath) as! PaddingRequstTableViewCell
            let item = self.OrdersDataSourse[indexPath.section]
            cell1.configureCell(order: item)
            return cell1
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let vc = Storyboard.Home.viewController(OrderDetilsViewController.self)
        vc.orderId = self.OrdersDataSourse[indexPath.section].id
        self.navigationController?.pushViewController(vc, animated: true)

    }
       
}

extension StoreDetailsVC : StoreBranchDelegate {
    func selectBranch(branche: BranchesModel) {
        self.storeLocation.text = branche.address
        self.storeDistance.text = branche.distance
        self.store.vicinity = branche.address
        self.store.lat = Double(branche.lat)!
        self.store.long = Double(branche.lng)!
        
    }
    
    
    
}
