//
//  NotficationsModel.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/16/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import Foundation
struct NotficationsModel{
    
    var id: Int
    var user_id:Int
    var name:String
    var image:String
    var text:String
    var order_status:String
    var seen:String
    var date:String
    var orderNextPage : String
    var orderRoute : String
    var place_ref : String
    var place_id : String
    var conversation_id : Int

}
