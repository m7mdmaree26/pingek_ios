//
//  StoreMenuCollectionCell.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/22/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit

class StoreMenuCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imageItem: UIImageView!
}
