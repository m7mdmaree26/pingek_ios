//
//  ALLHomeTableViewCell.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 11/26/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import UIKit

class ALLHomeTableViewCell: UITableViewCell, UITableViewDataSource , UITableViewDelegate  {
    @IBOutlet weak var CategoryName: UILabel!
    @IBOutlet weak var instedTableView: UITableView!
    var CountArry = ["Hii","Hello","Sara"]
    override func awakeFromNib() {
        super.awakeFromNib()
         configureMenuTableView()
         self.instedTableView.reloadData()
        instedTableView.delegate = self
        instedTableView.dataSource = self
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "insideTableViewCell", for: indexPath) as! insideTableViewCell
       // cell.addtionsName.text = self.AddtionsData[indexPath.row].addition_name!
        //cell.addtionsPrice.text = String(describing: self.AddtionsData[indexPath.row].addition_price!)
        cell.layer.cornerRadius = 15
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 1
        return cell
    }
    func configureMenuTableView(){
        instedTableView.register(UINib(nibName: "insideTableViewCell", bundle: nil), forCellReuseIdentifier: "insideTableViewCell")
        //        insideTableView.reloadData()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70
    }
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
   
    
}
