//
//  AllOfferBidesVC.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/27/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import Alamofire

class AllOfferBidesVC: UIViewController {

    var order_id = ""
    var bids = [BideDetailsModel]()
    @IBOutlet weak var allBodsTable: UITableView!
    @IBOutlet weak var orderDetails: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        getAllBids()
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        allBodsTable.tableFooterView = UIView()
        self.orderDetails.text = "Order number".localized + " " + order_id
        self.backButton.setImage((AppLanguage.getLang().contains("en") ? #imageLiteral(resourceName: "arrow_right") : #imageLiteral(resourceName: "arrow_left")  ), for: .normal)

        

    }
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getAllBids(){
        self.showProgress()
        let parmas :Parameters = [
        
            "order_id":order_id
        ]
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        API.POST(url: viewAllBids_URL, parameters: parmas, headers: header) { (succss, value) in
                   
        if succss{
            self.stopAnimating()
            let key = value["key"] as? String
                    
            if(key == "success"){
                let data = value["data"] as! [[String:Any]]
                for i in data{
                    self.bids.append(BideDetailsModel().getObject(dicc: i))
                }
                self.allBodsTable.reloadData()
            }
                       
        }else{
            self.stopAnimating()
                      
        }
    }
    
        
    }
    
    func agreeBid(bid_id:String,action:String, index: Int){
            let parmas :Parameters = [
                "bid_id":bid_id,
                "action":action
            ]
            print("🍉\(parmas)")
            let header:HTTPHeaders = [
                "Authorization":"Bearer" + getTokenId(),
                "lang":getServerLang()
            ]
            API.POST(url: agreeBid_URL, parameters: parmas, headers: header) { (succss, value) in
                if succss{
                    let key = value["key"] as! String
                    if(key == "fail"){
                        let msg = value["msg"] as! String
                        ShowErrorMassge(massge: msg, title: "Error".localized)
                    }else{
                        
                        let data = value["data"] as! [String:Any]
                        let keyData = data["key"] as! String
                        let message_ar = data["message_ar"] as! String
                        let message_en = data["message_en"] as! String
                        
                        
                        
                        
                        
                        //===============disAgree Offer
                        if(keyData == "disagreeBid"){
                            if(getServerLang() == "ar"){
                                ShowTrueMassge(massge: message_ar, title: "")
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                    self.dismiss(animated: true)
                                    
                                }
                            }else{
                                ShowTrueMassge(massge: message_en, title: "")
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                     self.dismiss(animated: true)
                                }
                            }
                            self.bids.remove(at: index)
                            self.allBodsTable.reloadData()
                        }else{
                            ShowTrueMassge(massge: "Your request has been accepted".localized, title: "Success".localized)
                            let conversation_id = data["conversation_id"] as! Int
                            let vc = Storyboard.Home.viewController(ChatViewController.self)
                            vc.ConvrstionId = String(conversation_id)
                            self.navigationController?.pushViewController(vc, animated: false)

                        }
                    }
                }else{
                    
                }
            }
        }
    
}

extension AllOfferBidesVC :UITableViewDataSource ,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bids.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BideTableCell", for: indexPath) as! BideTableCell
        cell.userName.text = self.bids[indexPath.row].provider_name
        cell.bideImage.setImageWith(self.bids[indexPath.row].avatar)
        cell.delivryTime.text = self.bids[indexPath.row].deliver_time + " " + "Hour".localized
        cell.delivryCost.text = self.bids[indexPath.row].price + " " + self.bids[indexPath.row].currency
        cell.distanceLbl.text = self.bids[indexPath.row].distance_provider_to_client + " " + "KM".localized
        cell.noOfOffers.text = self.bids[indexPath.row].num_bids
        cell.noOfRates.text = "( " + self.bids[indexPath.row].num_rating + " )"
        cell.rateView.rating = Double(self.bids[indexPath.row].rating)!
        cell.agreeOffer = {
            self.agreeBid(bid_id:self.bids[indexPath.row].id,action:"agree", index: indexPath.row)
        }
        cell.refuseOffer = {
            self.agreeBid(bid_id:self.bids[indexPath.row].id,action:"refuse", index: indexPath.row)
        }
        
        return cell
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
          return 210
    
       }
       
}


