//
//  MeVC.swift
//  Sandkom
//
//  Created by Engy Bakr on 1/16/20.
//  Copyright © 2020 Engy Bakr. All rights reserved.
//

import UIKit
import Cosmos
import Alamofire
import EzPopup

class MeVC: UIViewController {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var usersCommentsView: UIView!
    @IBOutlet weak var settingView: UIView!
    @IBOutlet weak var delegateView: UIView!
    
    @IBOutlet weak var accountStock: UILabel!
    @IBOutlet weak var totalDelegatePrice: UILabel!
    @IBOutlet weak var noOfOrders: UILabel!
    @IBOutlet weak var noOgCommentsButton: UIButton!
    
    @IBOutlet weak var socialButton1: UIButton!
    @IBOutlet weak var socialButton2: UIButton!
    @IBOutlet weak var socialButton3: UIButton!
    @IBOutlet weak var socialButton4: UIButton!

    @IBOutlet weak var socialCollection: UICollectionView!
    @IBOutlet weak var settingButton: UIButton!
    @IBOutlet weak var delegateArrpwImage: UIImageView!
    
    var socials = [SocialModel]()

    var link1 = ""
    var link2 = ""
    var link3 = ""
    var link4 = ""
    
    var icon1 = ""
    var icon2 = ""
    var icon3 = ""
    var icon4 = ""

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        
        let settingTap = UITapGestureRecognizer(target: self, action: #selector(openSetting))
        self.settingView.addGestureRecognizer(settingTap)
        
        let commentsTap = UITapGestureRecognizer(target: self, action: #selector(openComments))
        self.usersCommentsView.addGestureRecognizer(commentsTap)
        
        let delegateTap = UITapGestureRecognizer(target: self, action: #selector(openDelegate))
        self.delegateView.addGestureRecognizer(delegateTap)
        getMyProfileData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        getMyProfileData()
        getSocilsData()
    }
    
    @IBAction func addCopone(_ sender: Any) {
        let vc = Storyboard.Home.viewController(AddCoponViewController.self)
        let popupVC = PopupViewController(contentController: vc, popupWidth: UIScreen.main.bounds.width-80, popupHeight: 200)
        popupVC.backgroundAlpha = 0.5
        popupVC.backgroundColor = .black
        popupVC.canTapOutsideToDismiss = true
        popupVC.cornerRadius = 20
        popupVC.shadowEnabled = true
        self.present(popupVC, animated: true)
    }
    @IBAction func openNotificationSettings(_ sender: Any) {
        let vc = Storyboard.Home.viewController(ControlNotficationViewController.self)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    @IBAction func logout(_ sender: Any) {
         logOut()
    }
    @IBAction func openWhatsApp(_ sender: Any) {
       
    }
    @IBAction func openInstagram(_ sender: Any) {
        
    }
    @IBAction func openTwitter(_ sender: Any) {
        
    }
    @IBAction func openFaceBook(_ sender: Any) {
       
    }
    
    func setupView()  {
        self.navigationController?.isNavigationBarHidden = true
        self.settingButton.setImage((AppLanguage.getLang().contains("en") ?  #imageLiteral(resourceName: "arrow_poin_left") : #imageLiteral(resourceName: "arrow_poin_right")   ), for: .normal)
        self.noOgCommentsButton.setImage((AppLanguage.getLang().contains("en") ?  #imageLiteral(resourceName: "arrow_poin_left") : #imageLiteral(resourceName: "arrow_poin_right")   ), for: .normal)
        self.delegateArrpwImage.image = (AppLanguage.getLang().contains("en") ?  #imageLiteral(resourceName: "arrow_poin_left") : #imageLiteral(resourceName: "arrow_poin_right") )
    }
    
    @objc func openSetting(sender: UITapGestureRecognizer){
        let vc = Storyboard.Home.viewController(SettingViewController.self)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @objc func openComments(sender: UITapGestureRecognizer){
        let vc = Storyboard.Home.viewController(CustomrComentsViewController.self)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @objc func openDelegate(sender: UITapGestureRecognizer){
        guard let url = URL(string:"http://Pingek.4hoste.com/delegation") else { return }
        UIApplication.shared.open(url)
    }
    
}



extension MeVC {
    
    func getMyProfileData(){
        let params:Parameters = [
            "user_id":getUserID()
        ]
        API.POST(url: MyProfile_URL, parameters: params, headers: ["Authorization": "Bearer" + getTokenId()]) { (success, value) in
            if success{
                    self.stopAnimating()
                    let key = value["key"] as! String
                    if(key == "success"){
                    let data = value["data"] as! [String:Any]
                    let avatar = data["avatar"] as! String
                    let rate = data["rate"] as! Double
                    let balance = data["balance"] as! Double
                    let delegate = data["delegate"] as! String
                    if(delegate == "true"){
                        defaults.set("yes", forKey: is_Delgate)
                        self.delegateView.isHidden = true
                    }else{
                        defaults.set("no", forKey: is_Delgate)
                        self.delegateView.isHidden = false
                    }
                    let total_delivery_fees = data["total_delivery_fees"] as! Double
                    let num_orders = data["num_orders"] as! Int
                    let num_comments = data["num_comments"] as! Int
                    let name = data["name"] as? String
                    defaults.set(name, forKey: User_Name)
                    defaults.set(avatar, forKey: User_Avatar)
                    self.ratingView.rating = rate
                    self.userImage.setImageWith(avatar)
                    self.accountStock.text = String(balance) + " " + "SR".localized()
                    self.totalDelegatePrice.text = String(total_delivery_fees) + " " + "SR".localized()
                    self.noOfOrders.text = String(describing: num_orders)
                    self.userName.text = name
                        self.noOgCommentsButton.setTitle(" " + String(describing: num_comments) + " ", for: .normal)
                }else{
                        ShowTrueMassge(massge: "".localized(), title: "Error".localized)
                }
                
            }else{
                 self.stopAnimating()
            }
        }
    }
    func getSocilsData(){

        API.POST(url: Socials_URL, parameters: [:], headers: ["Authorization": "Bearer" + getTokenId(),"lang":getServerLang()]) { (success, vlaue) in
            if success{
                self.stopAnimating()
                self.socials.removeAll()
                print("😎\(vlaue)")
                let data = vlaue["data"] as! [[String:Any]]
                for i in data {
                    self.socials.append(SocialModel().getObject(dicc: i))
                }
                self.socialCollection.reloadData()
            }
                
        }
    }
    func logOut(){
        let header:HTTPHeaders = [
            "Authorization":"Bearer" + getTokenId(),
            "lang":getServerLang()
        ]
        API.POST(url: LogOut_URL, parameters: ["device_id":AppDelegate.FCMTOKEN], headers: header) { (succss, value) in
            if succss{
                self.stopAnimating()
                let key = value["key"] as? String
                if(key == "success"){
                let msg = value["msg"] as? String
                ShowTrueMassge(massge: msg!, title: "Done".localized())
                    SocketConnection.sharedInstance.socket.disconnect()
                    defaults.set("no", forKey: is_LoogedIn)
                    let Reg = Storyboard.Authentication.viewController(LoginViewController.self)
                    self.navigationController?.pushViewController(Reg, animated: true)
                }
                
            }else{
                self.stopAnimating()
                //   print("🍉")
            }
        }
    }
}


extension MeVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.socials.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoreMenuCollectionCell", for: indexPath) as! StoreMenuCollectionCell
        cell.imageItem.setImageWith(self.socials[indexPath.row].logo)
        cell.cornerRadius = 20
        return cell
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 40 , height:40)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         guard let url = URL(string:self.socials[indexPath.row].link) else { return }
         UIApplication.shared.open(url)
    }
        
        
}

