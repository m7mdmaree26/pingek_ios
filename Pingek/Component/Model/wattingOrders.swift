//
//  wattingOrders.swift
//  BeepBeep
//
//  Created by Sara Ashraf on 12/9/18.
//  Copyright © 2018 Sara Ashraf. All rights reserved.
//

import Foundation
class wattingOrders{
    
    var id = ""
    var client_name = ""
    var rate = ""
    var avatar = ""
    var place_name = ""
    var place_icon = ""
    var details = [wattingOrdersDetails]()
    var deliver_time = ""
    var price = ""
    var status = ""
    var distance_to_store = ""
    var distance_to_client = ""
    var date = ""

    func getObject(dicc: [String: Any]) ->  wattingOrders {
        let dic = HandleJSON.getObject().handle(dicc: dicc)
        let model =  wattingOrders()
        model.id = dic["id"] as! String
        model.client_name = dic["client_name"] as! String
        model.rate = dic["rate"] as! String
        model.avatar = dic["avatar"] as! String
        model.place_name = dic["place_name"] as! String
        model.place_icon = dic["place_icon"] as! String
        model.deliver_time = dic["deliver_time"] as! String
        model.price = dic["price"] as! String
        model.status = dic["status"] as! String
        model.distance_to_store = dic["distance_to_store"] as! String
        model.distance_to_client = dic["distance_to_client"] as! String
        model.date = dic["date"] as! String
        for item in dic["details"] as! [[String: Any]] {
            model.details.append(wattingOrdersDetails().getObject(dicc: item))
        }

        return model
           
    }
}



class wattingOrdersDetails{

    var key = ""
    var value = ""

    func getObject(dicc: [String: Any]) ->  wattingOrdersDetails {
        let dic = HandleJSON.getObject().handle(dicc: dicc)
        let model =  wattingOrdersDetails()
        model.key = dic["key"] as! String
        model.value = dic["value"] as! String
     return model
           
    }
}
